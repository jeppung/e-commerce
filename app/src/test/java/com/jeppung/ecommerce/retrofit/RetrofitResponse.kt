package com.jeppung.ecommerce.retrofit

import com.google.gson.Gson
import com.google.gson.JsonParser
import com.jeppung.ecommerce.repository.models.ErrorResponse
import com.jeppung.ecommerce.repository.models.SuccessResponse

object RetrofitResponse {

    val registerAndRefreshResponse = """
                {
                    "accessToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMzAuMTI5OjgwODAvIiwidXNlcklkIjoiMzczNTNkMzAtMWIzZC00ZGJlLThmODQtYWZjMjdjNGU5MWJhIiwidHlwZVRva2VuIjoiYWNjZXNzVG9rZW4iLCJleHAiOjE2ODUzNDE1MjB9.ldL_6Qoo-MfMmwHrhxXUv670Uz6j0CCF9t9I8uOmW_LuAUTzCWhjMcQelP8MjfnVDqKSZj2LaqHv3TY08AB7TQ",
                    "refreshToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMzAuMTI5OjgwODAvIiwidXNlcklkIjoiMzczNTNkMzAtMWIzZC00ZGJlLThmODQtYWZjMjdjNGU5MWJhIiwidHlwZVRva2VuIjoiYWNjZXNzVG9rZW4iLCJleHAiOjE2ODUzNDQ1MjB9.HeeNuQww-w2tb3pffNC43BCmMCcE3rOj-yL7-pTGOEcIcoFCv2n9IEWS0gqxNnDaNf3sXBm7JHCxFexB5FGRgQ",
                    "expiresAt": 600
                }
    """.trimIndent()

    val loginResponse = """
        {
            "userName": "test",
            "userImage": "",
            "accessToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMzAuMTI5OjgwODAvIiwidXNlcklkIjoiMzczNTNkMzAtMWIzZC00ZGJlLThmODQtYWZjMjdjNGU5MWJhIiwidHlwZVRva2VuIjoiYWNjZXNzVG9rZW4iLCJleHAiOjE2ODUzNDE4OTV9.AceVKZlMeFFvwNPAC5Opc6mSxhAXWz1CSf4E2FipZsJkPfaFt021Yi3TpG08ENUashUwJX-YLCuIolqnb7EulA",
            "refreshToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMzAuMTI5OjgwODAvIiwidXNlcklkIjoiMzczNTNkMzAtMWIzZC00ZGJlLThmODQtYWZjMjdjNGU5MWJhIiwidHlwZVRva2VuIjoicmVmcmVzaFRva2VuIiwiZXhwIjoxNjg1MzQ0ODk1fQ.tB4EeMvkfJAV_kSwakcEujsJEqNtlvKaBbz6ga58lMw6R1NKNOSi6iy3Qn-dFtHGMkzwqpokY3uOdQYcVtahCA",
            "expiresAt": 600
        }
    """.trimIndent()

    val registerProfileResponse = """
        {
            "userName": "test",
            "userImage": "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png"
        }
    """.trimIndent()

    val productsResponse = """
        {
            "itemsPerPage": 10,
            "currentItemCount": 10,
            "pageIndex": 1,
            "totalPages": 3,
            "items": [
                {
                    "productId": "601bb59a-4170-4b0a-bd96-f34538922c7c",
                    "productName": "Lenovo Legion 3",
                    "productPrice": 10000000,
                    "image": "image1",
                    "brand": "Lenovo",
                    "store": "LenovoStore",
                    "sale": 2,
                    "productRating": 4.0
                },
                {
                    "productId": "3134a179-dff6-464f-b76e-d7507b06887b",
                    "productName": "Lenovo Legion 5",
                    "productPrice": 15000000,
                    "image": "image1",
                    "brand": "Lenovo",
                    "store": "LenovoStore",
                    "sale": 4,
                    "productRating": 4.0
                }
            ]
        }
    """.trimIndent()

    val searchResponse = """
        [
            "Lenovo Legion 3",
            "Lenovo Legion 5",
            "Lenovo Legion 7",
            "Lenovo Ideapad 3",
            "Lenovo Ideapad 5",
            "Lenovo Ideapad 7"
        ]
    """.trimIndent()

    val productDetailResponse = """
        {
            "productId": "17b4714d-527a-4be2-84e2-e4c37c2b3292",
            "productName": "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
            "productPrice": 24499000,
            "image": [
                "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
            ],
            "brand": "Asus",
            "description": "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
            "store": "AsusStore",
            "sale": 12,
            "stock": 2,
            "totalRating": 7,
            "totalReview": 5,
            "totalSatisfaction": 100,
            "productRating": 5.0,
            "productVariant": [
                {
                    "variantName": "RAM 16GB",
                    "variantPrice": 0
                },
                {
                    "variantName": "RAM 32GB",
                    "variantPrice": 1000000
                }
            ]
        }
    """.trimIndent()

    val productReviewResponse = """
        [
            {
                "userName": "John",
                "userImage": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQM4VpzpVw8mR2j9_gDajEthwY3KCOWJ1tOhcv47-H9o1a-s9GRPxdb_6G9YZdGfv0HIg&usqp=CAU",
                "userRating": 4,
                "userReview": "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
            },
            {
                "userName": "Doe",
                "userImage": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR3Z6PN8QNVhH0e7rEINu_XJS0qHIFpDT3nwF5WSkcYmr3znhY7LOTkc8puJ68Bts-TMc&usqp=CAU",
                "userRating": 5,
                "userReview": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
            }
        ]
    """.trimIndent()

    val fulfillmentRequest = """
                    {
                        "invoiceId": "ba47402c-d263-49d3-a1f8-759ae59fa4a1",
                        "status": true,
                        "date": "09 Jun 2023",
                        "time": "08:53",
                        "payment": "Bank BCA",
                        "total": 48998000
                    }
    """.trimIndent()

    val transactionResponse = """
        [
            {
                "invoiceId": "8cad85b1-a28f-42d8-9479-72ce4b7f3c7d",
                "status": true,
                "date": "09 Jun 2023",
                "time": "09:05",
                "payment": "Bank BCA",
                "total": 48998000,
                "items": [
                    {
                        "productId": "bee98108-660c-4ac0-97d3-63cdc1492f53",
                        "variantName": "RAM 16GB",
                        "quantity": 2
                    }
                ],
                "rating": 4,
                "review": "LGTM",
                "image": "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                "name": "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray"
            }
        ]
    """.trimIndent()

    fun errorResponse(code: Int, message: String): String {
        val errObject = ErrorResponse(code = code, message = message)

        return Gson().toJson(errObject)
    }

    fun successResponse(data: String?): String {
        val successObject = SuccessResponse(
            code = 200,
            message = "OK",
            data = JsonParser().parse(data)
        )
        return Gson().toJson(successObject)
    }
}
