package com.jeppung.ecommerce.retrofit

import com.jeppung.ecommerce.repository.api_service.ApiService
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

abstract class RetrofitBaseTest {
    protected lateinit var mockWebServer: MockWebServer
    protected lateinit var apiService: ApiService

    @Before
    fun setup() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun shutdown() {
        mockWebServer.shutdown()
    }
}
