package com.jeppung.ecommerce.retrofit

import com.google.gson.Gson
import com.jeppung.ecommerce.repository.models.ErrorResponse
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException

class RetrofitProductTest : RetrofitBaseTest() {

    @Test
    fun getProductDetailSuccess() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(
                RetrofitResponse.successResponse(RetrofitResponse.productDetailResponse)
            )
        mockWebServer.enqueue(mockResponse)

        val response = apiService
            .productDetail("17b4714d-527a-4be2-84e2-e4c37c2b3292")

        TestCase.assertEquals(200, response.code)
        TestCase.assertEquals(
            "17b4714d-527a-4be2-84e2-e4c37c2b3292",
            response.data?.productId
        )
    }

    @Test
    fun getProductDetailError400() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(400)
            .setBody(
                RetrofitResponse.errorResponse(400, "productId must not be empty")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService
                .productDetail("")
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)
            TestCase.assertEquals(400, errResponse.code)
            TestCase.assertEquals(
                "productId must not be empty",
                errResponse.message
            )
        }
    }

    @Test
    fun getProductDetailError404() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(404)
            .setBody(
                RetrofitResponse.errorResponse(404, "Not Found")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService
                .productDetail("")
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)
            TestCase.assertEquals(404, errResponse.code)
            TestCase.assertEquals(
                "Not Found",
                errResponse.message
            )
        }
    }

    @Test
    fun getProductReviewSuccess() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(
                RetrofitResponse.successResponse(RetrofitResponse.productReviewResponse)
            )
        mockWebServer.enqueue(mockResponse)

        val response = apiService.productReview("17b4714d-527a-4be2-84e2-e4c37c2b3292")

        TestCase.assertEquals(200, response.code)
        TestCase.assertEquals(2, response.data?.size)
    }

    @Test
    fun getProductReviewError404() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(404)
            .setBody(
                RetrofitResponse.errorResponse(404, "Not Found")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.productReview("7sdc2b3292-527a-4be2-84e2-e4c3")
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(404, errResponse.code)
            TestCase.assertEquals("Not Found", errResponse.message)
        }
    }

    @Test
    fun getProductReviewError400() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(400)
            .setBody(
                RetrofitResponse.errorResponse(400, "productId must not be empty")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.productReview("7sdc2b3292-527a-4be2-84e2-e4c3")
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(400, errResponse.code)
            TestCase.assertEquals("productId must not be empty", errResponse.message)
        }
    }
}
