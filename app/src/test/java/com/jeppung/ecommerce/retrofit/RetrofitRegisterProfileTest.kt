package com.jeppung.ecommerce.retrofit

import com.google.gson.Gson
import com.jeppung.ecommerce.repository.models.ErrorResponse
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException

class RetrofitRegisterProfileTest : RetrofitBaseTest() {

    @Test
    fun registerProfileSuccess() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(
                RetrofitResponse.successResponse(RetrofitResponse.registerProfileResponse)
            )
        mockWebServer.enqueue(mockResponse)

        val userName = RequestBody.create(
            "text/plain".toMediaTypeOrNull(),
            "test"
        )

        val userImage = MultipartBody.Part.createFormData(
            "userImage",
            "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png",
            RequestBody.create("image/*".toMediaTypeOrNull(), "")
        )

        val response = apiService.registerProfile(
            userName,
            userImage
        )

        TestCase.assertEquals(200, response.code)
        TestCase.assertEquals("test", response.data?.userName)
        TestCase.assertEquals(
            "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png",
            response.data
                ?.userImage
        )
    }

    @Test
    fun registerProfileError400() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(400)
            .setBody(
                RetrofitResponse.errorResponse(400, "Username cannot be null")
            )
        mockWebServer.enqueue(mockResponse)

        val userName = RequestBody.create(
            "text/plain".toMediaTypeOrNull(),
            "test"
        )

        val userImage = MultipartBody.Part.createFormData(
            "userImage",
            "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png",
            RequestBody.create("image/*".toMediaTypeOrNull(), "")
        )

        try {
            apiService.registerProfile(
                userName,
                userImage
            )
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(400, errResponse.code)
            TestCase.assertEquals("Username cannot be null", errResponse.message)
        }
    }
}
