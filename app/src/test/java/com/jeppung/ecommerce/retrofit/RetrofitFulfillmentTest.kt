package com.jeppung.ecommerce.retrofit

import com.google.gson.Gson
import com.jeppung.ecommerce.repository.models.ErrorResponse
import com.jeppung.ecommerce.repository.models.FulfillmentRequest
import com.jeppung.ecommerce.repository.models.FullfillmentItem
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException

class RetrofitFulfillmentTest : RetrofitBaseTest() {
    @Test
    fun fulfillmentSuccess() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(
                RetrofitResponse.successResponse(RetrofitResponse.fulfillmentRequest)
            )
        mockWebServer.enqueue(mockResponse)

        val response = apiService.processPayment(
            FulfillmentRequest(
                payment = "Bank BCA",
                items = listOf(
                    FullfillmentItem(
                        productId = "5152a3dc-5c2f-4535-a06b-a9768a6c2add",
                        variantName = "RAM 16GB",
                        quantity = 2
                    )
                )
            )
        )

        TestCase.assertEquals(200, response.code)
    }

    @Test
    fun fulfillmentError403() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(403)
            .setBody(
                RetrofitResponse.errorResponse(403, "User invalid")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.processPayment(
                FulfillmentRequest(
                    payment = "Bank BCA",
                    items = listOf(
                        FullfillmentItem(
                            productId = "5152a3dc-5c2f-4535-a06b-a9768a6c2add",
                            variantName = "RAM 16GB",
                            quantity = 2
                        )
                    )
                )
            )
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(403, errResponse.code)
            TestCase.assertEquals("User invalid", errResponse.message)
        }
    }

    @Test
    fun fulfillmentError400() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(400)
            .setBody(
                RetrofitResponse.errorResponse(400, "Payment or Items must be filled correctly")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.processPayment(
                FulfillmentRequest(
                    payment = "Bank BCA",
                    items = listOf(
                        FullfillmentItem(
                            productId = "5152a3dc-5c2f-4535-a06b-a9768a6c2add",
                            variantName = "RAM 16GB",
                            quantity = 2
                        )
                    )
                )
            )
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(400, errResponse.code)
            TestCase.assertEquals("Payment or Items must be filled correctly", errResponse.message)
        }
    }
}
