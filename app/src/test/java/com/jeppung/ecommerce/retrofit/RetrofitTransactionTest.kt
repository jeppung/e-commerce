package com.jeppung.ecommerce.retrofit

import com.google.gson.Gson
import com.jeppung.ecommerce.repository.models.ErrorResponse
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException

class RetrofitTransactionTest : RetrofitBaseTest() {

    @Test
    fun transactionSuccess() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(
                RetrofitResponse.successResponse(RetrofitResponse.transactionResponse)
            )
        mockWebServer.enqueue(mockResponse)

        val response = apiService.getAllTransaction()

        TestCase.assertEquals(200, response.code)
        TestCase.assertEquals(1, response.data.size)
    }

    @Test
    fun transactionError404() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(404)
            .setBody(
                RetrofitResponse.errorResponse(404, "Not Found")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.getAllTransaction()
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(404, errResponse.code)
            TestCase.assertEquals("Not Found", errResponse.message)
        }
    }

    @Test
    fun transactionError403() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(403)
            .setBody(
                RetrofitResponse.errorResponse(403, "User invalid")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.getAllTransaction()
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(403, errResponse.code)
            TestCase.assertEquals("User invalid", errResponse.message)
        }
    }
}
