package com.jeppung.ecommerce.retrofit

import com.google.gson.Gson
import com.jeppung.ecommerce.repository.models.ErrorResponse
import com.jeppung.ecommerce.repository.models.UserModel
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException

class RetrofitRegisterTest : RetrofitBaseTest() {
    @Test
    fun registerSuccess() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(
                RetrofitResponse.successResponse(RetrofitResponse.registerAndRefreshResponse)
            )
        mockWebServer.enqueue(mockResponse)

        val response = apiService.register(
            UserModel(
                email = "test@test.com",
                password = "joshua123",
                firebaseToken = "abcd-efgh-ijkl-mnop"
            )
        )
        TestCase.assertEquals(200, response.code)
    }

    @Test
    fun registerError400() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(400)
            .setBody(
                RetrofitResponse.errorResponse(400, "Email is already taken")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.register(
                UserModel(
                    email = "test@test.com",
                    password = "joshua123",
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            )
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()?.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(400, errResponse.code)
            TestCase.assertEquals("Email is already taken", errResponse.message)
        }
    }

    @Test
    fun registerError403ApiNotValid() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(403)
            .setBody(
                RetrofitResponse.errorResponse(403, "Api key is not valid")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.register(
                UserModel(
                    email = "test@test.com",
                    password = "joshua123",
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            )
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()?.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(403, errResponse.code)
            TestCase.assertEquals("Api key is not valid", errResponse.message)
        }
    }

    @Test
    fun registerError403ApiCannotBeNull() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(403)
            .setBody(
                RetrofitResponse.errorResponse(403, "API key cannot be null")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.register(
                UserModel(
                    email = "test@test.com",
                    password = "joshua123",
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            )
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()?.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(403, errResponse.code)
            TestCase.assertEquals("API key cannot be null", errResponse.message)
        }
    }
}
