package com.jeppung.ecommerce.retrofit

import com.google.gson.Gson
import com.jeppung.ecommerce.repository.models.ErrorResponse
import junit.framework.TestCase
import okhttp3.mockwebserver.MockResponse
import org.junit.Test

class RetrofitRefreshTest : RetrofitBaseTest() {

    @Test
    fun refreshSuccess() {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(
                RetrofitResponse.successResponse(RetrofitResponse.registerAndRefreshResponse)
            )
        mockWebServer.enqueue(mockResponse)

        val response = apiService.refreshToken(
            hashMapOf(
                "token" to "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMzAuMTI5OjgwODAvIiwidXNlcklkIjoiMzczNTNkMzAtMWIzZC00ZGJlLThmODQtYWZjMjdjNGU5MWJhIiwidHlwZVRva2VuIjoiYWNjZXNzVG9rZW4iLCJleHAiOjE2ODUzNDUwMjN9.U3FQQCGsyBCWE5qUOkWjneI_igtUj9bDKvJI-25o-8a6NMekmvvdlzjJVvK2Yyed9IpAaGTMXNgeQsl9M04uDA"
            )
        ).execute()

        TestCase.assertEquals(200, response.body()?.code)
    }

    @Test
    fun refreshError403ApiKeyIsNotValid() {
        val mockResponse = MockResponse()
            .setResponseCode(403)
            .setBody(
                RetrofitResponse.errorResponse(403, "Api key is not valid")
            )
        mockWebServer.enqueue(mockResponse)

        val response = apiService.refreshToken(
            hashMapOf(
                "token" to "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMzAuMTI5OjgwODAvIiwidXNlcklkIjoiMzczNTNkMzAtMWIzZC00ZGJlLThmODQtYWZjMjdjNGU5MWJhIiwidHlwZVRva2VuIjoiYWNjZXNzVG9rZW4iLCJleHAiOjE2ODUzNDUwMjN9.U3FQQCGsyBCWE5qUOkWjneI_igtUj9bDKvJI-25o-8a6NMekmvvdlzjJVvK2Yyed9IpAaGTMXNgeQsl9M04uDA"
            )
        ).execute()

        val errResponse =
            Gson().fromJson(response.errorBody()!!.string(), ErrorResponse::class.java)

        TestCase.assertEquals(403, errResponse.code)
        TestCase.assertEquals("Api key is not valid", errResponse.message)
    }

    @Test
    fun refreshError403ApiKeyCannotBeNull() {
        val mockResponse = MockResponse()
            .setResponseCode(403)
            .setBody(
                RetrofitResponse.errorResponse(403, "API key cannot be null")
            )
        mockWebServer.enqueue(mockResponse)

        val response = apiService.refreshToken(
            hashMapOf(
                "token" to "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMzAuMTI5OjgwODAvIiwidXNlcklkIjoiMzczNTNkMzAtMWIzZC00ZGJlLThmODQtYWZjMjdjNGU5MWJhIiwidHlwZVRva2VuIjoiYWNjZXNzVG9rZW4iLCJleHAiOjE2ODUzNDUwMjN9.U3FQQCGsyBCWE5qUOkWjneI_igtUj9bDKvJI-25o-8a6NMekmvvdlzjJVvK2Yyed9IpAaGTMXNgeQsl9M04uDA"
            )
        ).execute()

        val errResponse =
            Gson().fromJson(response.errorBody()!!.string(), ErrorResponse::class.java)

        TestCase.assertEquals(403, errResponse.code)
        TestCase.assertEquals("API key cannot be null", errResponse.message)
    }
}
