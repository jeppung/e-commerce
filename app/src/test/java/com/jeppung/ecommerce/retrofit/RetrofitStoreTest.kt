package com.jeppung.ecommerce.retrofit

import com.google.gson.Gson
import com.jeppung.ecommerce.repository.models.ErrorResponse
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException

class RetrofitStoreTest : RetrofitBaseTest() {

    @Test
    fun getProducts() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(
                RetrofitResponse.successResponse(RetrofitResponse.productsResponse)
            )
        mockWebServer.enqueue(mockResponse)

        val response = apiService.getSearchProduct(
            search = "lenovo",
            brand = "lenovo",
            lowest = 10000000,
            highest = 20000000,
            sort = "sale",
            limit = 10,
            page = 1
        )

        TestCase.assertEquals(200, response.code)
        TestCase.assertEquals(
            10,
            response.data?.itemsPerPage
        )
        response.data?.items?.forEach {
            TestCase.assertEquals(true, it.productName.contains("lenovo", ignoreCase = true))
            TestCase.assertEquals(true, it.brand.contains("lenovo", ignoreCase = true))
        }
    }

    @Test
    fun getProductsErrorNotFound() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(404)
            .setBody(
                RetrofitResponse.errorResponse(404, "Not Found")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.getSearchProduct(
                search = "asdfaf",
                brand = "lenovo",
                lowest = 10000000,
                highest = 20000000,
                sort = "sale",
                limit = 10,
                page = 1
            )
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)
            TestCase.assertEquals(404, errResponse.code)
            TestCase.assertEquals("Not Found", errResponse.message)
        }
    }

    @Test
    fun searchSuccess() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(
                RetrofitResponse.successResponse(RetrofitResponse.searchResponse)
            )
        mockWebServer.enqueue(mockResponse)

        val response = apiService.search("lenovo")

        TestCase.assertEquals(200, response.code)
        TestCase.assertEquals(6, response.data.size)
    }
}
