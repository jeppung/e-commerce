package com.jeppung.ecommerce.retrofit

import com.google.gson.Gson
import com.jeppung.ecommerce.repository.models.ErrorResponse
import com.jeppung.ecommerce.repository.models.ReviewRatingRequest
import com.jeppung.ecommerce.repository.models.SuccessResponse
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException

class RetrofitRatingTest : RetrofitBaseTest() {
    @Test
    fun ratingSuccess() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(
                Gson().toJson(
                    SuccessResponse(
                        code = 200,
                        message = "Fulfillment rating and review success",
                        data = null
                    )
                )
            )
        mockWebServer.enqueue(mockResponse)

        val response = apiService.submitReviewRating(
            ReviewRatingRequest(
                invoiceId = "6d2ed50f-0c87-4a57-b873-8a4addd68949",
                rating = 0,
                review = ""
            )
        )

        TestCase.assertEquals(200, response.code)
        TestCase.assertEquals("Fulfillment rating and review success", response.message)
    }

    @Test
    fun ratingError404() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(404)
            .setBody(
                RetrofitResponse.errorResponse(404, "Not Found")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.submitReviewRating(
                ReviewRatingRequest(
                    invoiceId = "6d2ed54addd689490f-0c87-4a57-b873-8a",
                    rating = 0,
                    review = ""
                )
            )
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(404, errResponse.code)
            TestCase.assertEquals("Not Found", errResponse.message)
        }
    }

    @Test
    fun ratingError403() = runTest {
        val mockResponse = MockResponse()
            .setResponseCode(403)
            .setBody(
                RetrofitResponse.errorResponse(403, "User or Product invalid")
            )
        mockWebServer.enqueue(mockResponse)

        try {
            apiService.submitReviewRating(
                ReviewRatingRequest(
                    invoiceId = "6d2ed54addd689490f-0c87-4adsds57-b873-8a",
                    rating = 0,
                    review = ""
                )
            )
        } catch (e: HttpException) {
            val errResponse =
                Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java)

            TestCase.assertEquals(403, errResponse.code)
            TestCase.assertEquals("User or Product invalid", errResponse.message)
        }
    }
}
