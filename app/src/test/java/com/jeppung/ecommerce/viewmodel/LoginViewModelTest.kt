package com.jeppung.ecommerce.viewmodel

import com.jeppung.ecommerce.repository.models.Data
import com.jeppung.ecommerce.repository.models.LoginResponse
import com.jeppung.ecommerce.repository.models.UserModel
import com.jeppung.ecommerce.views.login.LoginUiState
import com.jeppung.ecommerce.views.login.LoginViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class LoginViewModelTest : ViewModelBaseTest() {
    @Mock
    private lateinit var mockViewModel: LoginViewModel

    @Before
    fun setup() {
        apiService = mock()
        mockViewModel = LoginViewModel(apiService)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun loginSuccess() = runTest {
        whenever(
            apiService.login(
                UserModel(
                    email = "test@test.com",
                    password = "testtest123",
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            )
        ).thenReturn(
            LoginResponse(
                200,
                Data(
                    "",
                    "test",
                    "a",
                    600,
                    "b"
                ),
                message = "OK"
            )
        )

        val values = mutableListOf<LoginUiState>()

        mockViewModel.login(
            UserModel(
                email = "test@test.com",
                password = "testtest123",
                firebaseToken = "abcd-efgh-ijkl-mnop"
            )
        )

        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.loginUiState.collect(values::add)
        }
        advanceUntilIdle()

        assertEquals(
            listOf(
                LoginUiState.Loading,
                LoginUiState.Success(
                    LoginResponse(
                        200,
                        Data(
                            "",
                            "test",
                            "a",
                            600,
                            "b"
                        ),
                        message = "OK"
                    ),
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            ),
            values
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun loginErrorInvalidCredential() = runTest {
        whenever(
            apiService.login(
                UserModel(
                    email = "test@test.com",
                    password = "testtest123",
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            )
        ).thenThrow(InvalidCredentialsException())

        val values = mutableListOf<LoginUiState>()

        mockViewModel.login(
            UserModel(
                email = "test@test.com",
                password = "testtest123",
                firebaseToken = "abcd-efgh-ijkl-mnop"
            )
        )

        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.loginUiState.collect(values::add)
        }
        advanceUntilIdle()

        assertEquals(
            listOf(
                LoginUiState.Loading,
                LoginUiState.Error("Email or password is not valid")
            ),
            values
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun loginErrorInvalidApiKey() = runTest {
        whenever(
            apiService.login(
                UserModel(
                    email = "test@test.com",
                    password = "testtest123",
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            )
        ).thenThrow(InvalidApiKey())

        val values = mutableListOf<LoginUiState>()

        mockViewModel.login(
            UserModel(
                email = "test@test.com",
                password = "testtest123",
                firebaseToken = "abcd-efgh-ijkl-mnop"
            )
        )

        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.loginUiState.collect(values::add)
        }
        advanceUntilIdle()

        assertEquals(
            listOf(
                LoginUiState.Loading,
                LoginUiState.Error("Api key is not valid")
            ),
            values
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun loginErrorNullApiKey() = runTest {
        whenever(
            apiService.login(
                UserModel(
                    email = "test@test.com",
                    password = "testtest123",
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            )
        ).thenThrow(NullApiKey())

        val values = mutableListOf<LoginUiState>()

        mockViewModel.login(
            UserModel(
                email = "test@test.com",
                password = "testtest123",
                firebaseToken = "abcd-efgh-ijkl-mnop"
            )
        )

        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.loginUiState.collect(values::add)
        }
        advanceUntilIdle()

        assertEquals(
            listOf(
                LoginUiState.Loading,
                LoginUiState.Error("API key cannot be null")
            ),
            values
        )
    }

    class InvalidCredentialsException : RuntimeException("Email or password is not valid")
    class InvalidApiKey : RuntimeException("Api key is not valid")
    class NullApiKey : RuntimeException("API key cannot be null")
}
