package com.jeppung.ecommerce.viewmodel

import androidx.lifecycle.SavedStateHandle
import com.jeppung.ecommerce.repository.models.ProductReviewData
import com.jeppung.ecommerce.repository.models.ProductReviewResponse
import com.jeppung.ecommerce.views.product_review.ProductReviewUiState
import com.jeppung.ecommerce.views.product_review.ProductReviewViewModel
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class ProductReviewViewModelTest : ViewModelBaseTest() {

    @Mock
    private lateinit var mockViewModel: ProductReviewViewModel

    @Before
    fun setup() {
        apiService = mock()
        val savedStateHandle = SavedStateHandle()
        mockViewModel = ProductReviewViewModel(apiService, savedStateHandle)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun productReviewUiStateSuccess() = runTest {
        whenever(apiService.productReview("bee98108-660c-4ac0-97d3-63cdc1492f53"))
            .thenReturn(
                ProductReviewResponse(
                    200,
                    dummyReviews,
                    "OK"
                )
            )

        mockViewModel.productReview(
            "bee98108-660c-4ac0-97d3-63cdc1492f53",
        )
        advanceUntilIdle()

        TestCase.assertTrue(mockViewModel.productReviewUiState.getOrAwaitValue() is ProductReviewUiState.Success)
    }

    @Test
    fun productReviewUiStateError() = runTest {
        whenever(apiService.productReview("63cdc1492f5-660c-4ac0-97d3-3dsda"))
            .thenThrow(
                RuntimeException("Not Found")
            )

        mockViewModel.productReview(
            "63cdc1492f5-660c-4ac0-97d3-3dsda",
        )
        advanceUntilIdle()

        TestCase.assertTrue(mockViewModel.productReviewUiState.getOrAwaitValue() is ProductReviewUiState.Error)
    }
}

val dummyReviews: List<ProductReviewData> = listOf(
    ProductReviewData(
        "",
        "test1",
        "review1",
        4
    ),
    ProductReviewData(
        "",
        "test2",
        "review2",
        5
    ),
)
