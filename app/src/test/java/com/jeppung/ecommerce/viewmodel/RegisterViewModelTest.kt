import com.jeppung.ecommerce.repository.models.RegisterData
import com.jeppung.ecommerce.repository.models.RegisterResponse
import com.jeppung.ecommerce.repository.models.UserModel
import com.jeppung.ecommerce.viewmodel.ViewModelBaseTest
import com.jeppung.ecommerce.views.register.RegisterUiState
import com.jeppung.ecommerce.views.register.RegisterViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class RegisterViewModelTest : ViewModelBaseTest() {

    @Mock
    private lateinit var mockViewModel: RegisterViewModel

    @Before
    fun setup() {
        apiService = mock()
        mockViewModel = RegisterViewModel(apiService)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun registerSuccess() = runTest {
        whenever(
            apiService.register(
                UserModel(
                    email = "test@test.com",
                    password = "testtest123",
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            )
        ).thenReturn(
            RegisterResponse(
                200,
                RegisterData(
                    "a",
                    600,
                    "b"
                ),
                message = "OK"
            )
        )

        val values = mutableListOf<RegisterUiState>()

        mockViewModel.register(
            UserModel(
                email = "test@test.com",
                password = "testtest123",
                firebaseToken = "abcd-efgh-ijkl-mnop"
            )
        )

        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.registerUiState.collect(values::add)
        }
        advanceUntilIdle()

        Assert.assertEquals(
            listOf(
                RegisterUiState.Loading,
                RegisterUiState.Success(
                    RegisterResponse(
                        200,
                        RegisterData(
                            "a",
                            600,
                            "b"
                        ),
                        message = "OK"
                    ),
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            ),
            values
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun registerErrorEmailAlreadyTaken() = runTest {
        whenever(
            apiService.register(
                UserModel(
                    email = "test@test.com",
                    password = "testtest123",
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            )
        ).thenThrow(
            CredentialAlreadyTakenException()
        )

        val values = mutableListOf<RegisterUiState>()

        mockViewModel.register(
            UserModel(
                email = "test@test.com",
                password = "testtest123",
                firebaseToken = "abcd-efgh-ijkl-mnop"
            )
        )

        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.registerUiState.collect(values::add)
        }
        advanceUntilIdle()

        Assert.assertEquals(
            listOf(
                RegisterUiState.Loading,
                RegisterUiState.Error("Email is already taken")
            ),
            values
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun registerErrorApiNotValid() = runTest {
        whenever(
            apiService.register(
                UserModel(
                    email = "test@test.com",
                    password = "testtest123",
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            )
        ).thenThrow(
            InvalidApiKeyException()
        )

        val values = mutableListOf<RegisterUiState>()

        mockViewModel.register(
            UserModel(
                email = "test@test.com",
                password = "testtest123",
                firebaseToken = "abcd-efgh-ijkl-mnop"
            )
        )

        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.registerUiState.collect(values::add)
        }
        advanceUntilIdle()

        Assert.assertEquals(
            listOf(
                RegisterUiState.Loading,
                RegisterUiState.Error("Api key is not valid")
            ),
            values
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun registerErrorNullApi() = runTest {
        whenever(
            apiService.register(
                UserModel(
                    email = "test@test.com",
                    password = "testtest123",
                    firebaseToken = "abcd-efgh-ijkl-mnop"
                )
            )
        ).thenThrow(
            NullApiKeyException()
        )

        val values = mutableListOf<RegisterUiState>()

        mockViewModel.register(
            UserModel(
                email = "test@test.com",
                password = "testtest123",
                firebaseToken = "abcd-efgh-ijkl-mnop"
            )
        )

        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.registerUiState.collect(values::add)
        }
        advanceUntilIdle()

        Assert.assertEquals(
            listOf(
                RegisterUiState.Loading,
                RegisterUiState.Error("API key cannot be null")
            ),
            values
        )
    }

    class CredentialAlreadyTakenException : RuntimeException("Email is already taken")
    class InvalidApiKeyException : RuntimeException("Api key is not valid")
    class NullApiKeyException : RuntimeException("API key cannot be null")
}
