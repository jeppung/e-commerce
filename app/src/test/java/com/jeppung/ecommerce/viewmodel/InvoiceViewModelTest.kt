package com.jeppung.ecommerce.viewmodel

import com.jeppung.ecommerce.repository.models.ProductReviewResponse
import com.jeppung.ecommerce.repository.models.ReviewRatingRequest
import com.jeppung.ecommerce.repository.models.ReviewRatingResponse
import com.jeppung.ecommerce.views.invoice.InvoiceUiState
import com.jeppung.ecommerce.views.invoice.InvoiceViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.HttpException
import retrofit2.Response
import java.lang.RuntimeException

class InvoiceViewModelTest : ViewModelBaseTest() {

    @Mock
    private lateinit var mockViewModel: InvoiceViewModel

    @Before
    fun setup() {
        apiService = mock()
        mockViewModel = InvoiceViewModel(apiService)
    }

    @Test
    fun invoiceUiStateSuccess() = runTest {
        whenever(
            apiService.submitReviewRating(
                ReviewRatingRequest(
                    "Review1",
                    4,
                    "bee98108-660c-4ac0-97d3-63cdc1492f53"
                )
            )
        ).thenReturn(
            ReviewRatingResponse(
                200,
                "Fulfillment rating and review success"
            )
        )

        mockViewModel.submitReviewRating(
            ReviewRatingRequest(
                "Review1",
                4,
                "bee98108-660c-4ac0-97d3-63cdc1492f53"
            ),
        )

        val values = mutableListOf<InvoiceUiState>()
        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.invoiceUiState.collect(values::add)
        }
        advanceUntilIdle()

        Assert.assertEquals(
            listOf(
                InvoiceUiState.Loading,
                InvoiceUiState.Success(
                    "Fulfillment rating and review success"
                )
            ),
            values
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun invoiceUiStateError() = runTest {
        val errorResponseBody = ResponseBody.create(null, "Error body")
        val errorResponse = Response.error<ProductReviewResponse>(400, errorResponseBody)
        val httpException = HttpException(errorResponse)

        whenever(
            apiService.submitReviewRating(
                ReviewRatingRequest(
                    "Review1",
                    4,
                    ""
                )
            )
        ).thenThrow(
            RuntimeException("Not Found")
        )

        mockViewModel.submitReviewRating(
            ReviewRatingRequest(
                "Review1",
                4,
                "bee98108-660c-4ac0-97d3-63cdc1492f53"
            ),
        )

        val values = mutableListOf<InvoiceUiState>()
        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.invoiceUiState.collect(values::add)
        }
        advanceUntilIdle()

        Assert.assertTrue(
            values[values.lastIndex] is InvoiceUiState.Error
        )
    }
}
