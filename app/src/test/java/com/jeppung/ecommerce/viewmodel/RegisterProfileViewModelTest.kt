package com.jeppung.ecommerce.viewmodel

import com.jeppung.ecommerce.repository.models.RegisterProfileData
import com.jeppung.ecommerce.repository.models.RegisterProfileResponse
import com.jeppung.ecommerce.views.register_profile.RegisterProfileUiState
import com.jeppung.ecommerce.views.register_profile.RegisterProfileViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class RegisterProfileViewModelTest : ViewModelBaseTest() {
    @Mock
    private lateinit var mockViewModel: RegisterProfileViewModel

    @Before
    fun setup() {
        apiService = mock()
        mockViewModel = RegisterProfileViewModel(apiService)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun registerProfileSuccess() = runTest {
        val userName = RequestBody.create(
            "text/plain".toMediaTypeOrNull(),
            "test"
        )
        val userImage = MultipartBody.Part.createFormData(
            "userImage",
            "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png",
            RequestBody.create("image/*".toMediaTypeOrNull(), "")
        )

        whenever(
            apiService.registerProfile(
                userName,
                userImage
            )
        ).thenReturn(
            RegisterProfileResponse(
                200,
                RegisterProfileData(
                    "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png",
                    "test",
                ),
                message = "OK"
            )
        )

        val values = mutableListOf<RegisterProfileUiState>()

        mockViewModel.registerProfile(
            userName,
            userImage
        )

        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.registerProfileUiState.collect(values::add)
        }
        advanceUntilIdle()

        Assert.assertEquals(
            listOf(
                RegisterProfileUiState.Loading,
                RegisterProfileUiState.Success(
                    RegisterProfileResponse(
                        200,
                        RegisterProfileData(
                            "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png",
                            "test",
                        ),
                        message = "OK"
                    )
                )
            ),
            values
        )
    }
}
