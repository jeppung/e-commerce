package com.jeppung.ecommerce.viewmodel

import com.jeppung.ecommerce.repository.models.Transaction
import com.jeppung.ecommerce.repository.models.TransactionItem
import com.jeppung.ecommerce.repository.models.TransactionsResponse
import com.jeppung.ecommerce.views.transaksi.TransactionUiState
import com.jeppung.ecommerce.views.transaksi.TransactionViewModel
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class TransactionViewModelTest : ViewModelBaseTest() {

    @Mock
    private lateinit var mockViewModel: TransactionViewModel

    @Before
    fun setup() {
        apiService = mock()
        mockViewModel = TransactionViewModel(apiService)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun getAllTransaction() = runTest {
        whenever(apiService.getAllTransaction())
            .thenReturn(
                TransactionsResponse(
                    200,
                    dummyTransaction,
                    "OK"
                )
            )

        mockViewModel.getAllTransaction()

        val values = mutableListOf<TransactionUiState>()
        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.transactionUiState.collect(values::add)
        }
        advanceUntilIdle()

        TestCase.assertTrue(values.get(values.lastIndex) is TransactionUiState.Success)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun transactionUiStateError() = runTest {
        whenever(apiService.getAllTransaction())
            .thenThrow(
                RuntimeException("User invalid")
            )

        mockViewModel.getAllTransaction()

        val values = mutableListOf<TransactionUiState>()
        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.transactionUiState.collect(values::add)
        }
        advanceUntilIdle()

        TestCase.assertTrue(values.get(values.lastIndex) is TransactionUiState.Error)
    }
}

val dummyTransaction: List<Transaction> = listOf(
    Transaction(
        "21 jun 2023",
        48998000,
        listOf(
            TransactionItem(
                1,
                "bee98108-660c-4ac0-97d3-63cdc1492f53",
                "RAM 16GB"
            )
        ),
        "LGTM",
        4,
        "8cad85b1-a28f-42d8-9479-72ce4b7f3c7d",
        "Bank BCA",
        "09:05",
        true,
        "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
        "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray"
    ),
    Transaction(
        "22 jun 2023",
        50998000,
        listOf(
            TransactionItem(
                3,
                "bee98108-660c-4ac0-97d3-63cdc1492f53",
                "RAM 32GB"
            )
        ),
        "LGTM",
        5,
        "xsdasdas-dasdasd-dasdasd-dasdasd",
        "Bank BNI",
        "09:05",
        true,
        "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
        "Lenovo Ideapad G713RM-R736H6G-O - Eclipse Gray"
    ),
)
