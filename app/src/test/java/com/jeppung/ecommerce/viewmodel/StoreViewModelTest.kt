package com.jeppung.ecommerce.viewmodel

import com.jeppung.ecommerce.repository.models.SearchResponse
import com.jeppung.ecommerce.views.store.StoreSearchUiState
import com.jeppung.ecommerce.views.store.StoreViewModel
import junit.framework.TestCase
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.lang.RuntimeException

class StoreViewModelTest : ViewModelBaseTest() {

    @Mock
    private lateinit var mockViewModel: StoreViewModel

    @Before
    fun setup() {
        apiService = mock()
        mockViewModel = StoreViewModel(apiService)
    }

    @Test
    fun storeUiStateSuccess() = runTest {
        whenever(apiService.search("lenovo")).thenReturn(
            SearchResponse(
                200,
                dummyQuerySearchData,
                "OK"
            )
        )

        mockViewModel.onQuerySearch("lenovo")

        val values = mutableListOf<StoreSearchUiState>()
        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.storeSearchUiState.collect(values::add)
        }
        advanceUntilIdle()

        TestCase.assertTrue(values.get(1) is StoreSearchUiState.Success)
    }

    @Test
    fun storeUiStateError() = runTest {
        whenever(apiService.search("lenovo")).thenThrow(
            RuntimeException("Error")
        )

        mockViewModel.onQuerySearch("lenovo")

        val values = mutableListOf<StoreSearchUiState>()
        backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
            mockViewModel.storeSearchUiState.collect(values::add)
        }
        advanceUntilIdle()

        TestCase.assertTrue(values.get(1) is StoreSearchUiState.Error)
    }
}

val dummyQuerySearchData = listOf(
    "Lenovo Legion 3",
    "Lenovo Legion 5",
    "Lenovo Legion 7",
    "Lenovo Ideapad 3",
    "Lenovo Ideapad 5",
    "Lenovo Ideapad 7"
)
