package com.jeppung.ecommerce.utils

import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.jeppung.ecommerce.repository.prefs.UserPreference
import com.jeppung.ecommerce.repository.room.AppDatabase
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale

object AppUtils {
    fun setSpannableBottomInfo(
        context: Context,
        original: Int,
        terms: String,
        condition: String
    ): SpannableString {
        val bottomInfo = context.resources.getString(
            original,
        )

        val spannableBottomInfo = SpannableString(bottomInfo)

        spannableBottomInfo.setSpan(
            ForegroundColorSpan(Color.parseColor("#6750A4")),
            bottomInfo.indexOf(terms),
            bottomInfo.lastIndexOf(terms) + terms.length,
            Spannable.SPAN_EXCLUSIVE_INCLUSIVE
        )
        spannableBottomInfo.setSpan(
            ForegroundColorSpan(Color.parseColor("#6750A4")),
            bottomInfo.indexOf(condition),
            bottomInfo.lastIndexOf(condition) + condition.length,
            Spannable.SPAN_EXCLUSIVE_INCLUSIVE
        )

        return spannableBottomInfo
    }

    fun formatCurrencyIDR(amount: Long): String {
        val currencyFormatter = DecimalFormat.getCurrencyInstance() as DecimalFormat
        val customSymbol = DecimalFormatSymbols(Locale("id", "ID"))
        currencyFormatter.decimalFormatSymbols = customSymbol
        currencyFormatter.positivePrefix = "Rp"
        currencyFormatter.negativePrefix = "-Rp"
        currencyFormatter.minimumFractionDigits = 0

        return currencyFormatter.format(amount)
    }

    fun parseCurrencyIdr(currencyString: String): Long {
        val cleanString = currencyString.replace("\\D".toRegex(), "")

        return try {
            cleanString.toLong()
        } catch (e: NumberFormatException) {
            0
        }
    }

    fun hideSoftKeyboard(context: Context, view: View) {
        val keyboard =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        keyboard.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun authTimeoutHandler(userPreference: UserPreference, db: AppDatabase) {
        userPreference.setAccessToken(null)
        userPreference.setRefreshToken(null)
        userPreference.setUserName(null)
        db.clearAllTables()
    }
}
