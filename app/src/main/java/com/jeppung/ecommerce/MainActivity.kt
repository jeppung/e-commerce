package com.jeppung.ecommerce

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import androidx.navigation.ui.setupWithNavController
import androidx.window.layout.WindowMetricsCalculator
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.jeppung.ecommerce.databinding.ActivityMainBinding
import com.jeppung.ecommerce.repository.prefs.UserPreference
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()

    private val isOnboarding by lazy { userPreference.getIsOnboard() }
    private val cartBadge by lazy { BadgeDrawable.create(this) }
    private val notifBadge by lazy { BadgeDrawable.create(this) }
    private val darkMode by lazy { userPreference.getDarkMode() }

    @Inject
    lateinit var userPreference: UserPreference

    @Inject
    lateinit var remoteConfig: FirebaseRemoteConfig

    @Inject
    lateinit var firebaseMessaging: FirebaseMessaging

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /**
         * Request notification permission (for SDK >= 33)
         */
        if (Build.VERSION.SDK_INT >= 33) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.POST_NOTIFICATIONS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.POST_NOTIFICATIONS),
                    123
                )
            }
        }

        /**
         *  Subscribe to cloud messaging topic
         */
        firebaseMessaging.subscribeToTopic("promo")
            .addOnCompleteListener {}

        /**
         *  Fetching Firebase Remote Config data
         */
        remoteConfig.fetchAndActivate().addOnCompleteListener {}

        /**
         Setting up app theme
         */
        if (darkMode) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }

        /**
         Setup navigation Helper
         */
        val navHostNavFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_container) as NavHostFragment
        val navController = navHostNavFragment.findNavController()

        lifecycleScope.launch {
            navController.currentBackStack.collectLatest { data ->
                Log.d("TAGGG", data.map { it.destination.displayName }.toString())
            }
        }

        /**
         * Setting up app layout based on device width
         */
        computeWindowSizeClasses(navController)

        /**
         *  Navigation validation
         */
        if (isOnboarding) {
            if (userPreference.getAccessToken() != null) {
                if (userPreference.getUserName().isNullOrEmpty()) {
                    /*
                        This shouldnt be the code, but to avoid navigation crash when running in real device
                        the real code is only to setGraph to pre_login_navigation then navigate to registerProfileFragment
                     */
                    navController.popBackStack()
                    navController.navigate(
                        R.id.pre_login,
                        null,
                        navOptions {
                            popUpTo(R.id.main) {
                                inclusive = true
                            }
                        }
                    )
                    navController.navigate(
                        R.id.registerProfileFragment,
                        null,
                        navOptions {
                            popUpTo(R.id.loginFragment) {
                                inclusive = true
                            }
                        }
                    )
                } else {
                    if (navController.currentDestination?.parent?.displayName?.contains("main") == false) {
                        settingNavigationBarVisibility(View.VISIBLE)
                        navController.navigate(R.id.action_global_main)
                    }
                }
            } else {
                navController.popBackStack()
                navController.navigate(R.id.action_global_pre_login)
            }
        } else {
            /*
                This shouldnt be the code, but to avoid navigation crash when running in real device
                the real code is only to setGraph to pre_login_navigation then navigate to onboardingFragment
             */
            navController.popBackStack()
            navController.navigate(
                R.id.pre_login,
                null,
                navOptions {
                    popUpTo(R.id.main) {
                        inclusive = true
                    }
                }
            )
            navController.navigate(
                R.id.onboardingFragment,
                null,
                navOptions {
                    popUpTo(R.id.loginFragment) {
                        inclusive = true
                    }
                }
            )
        }

        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (
                destination.id == R.id.onboardingFragment ||
                destination.id == R.id.loginFragment ||
                destination.id == R.id.registerFragment ||
                destination.id == R.id.registerProfileFragment ||
                destination.id == R.id.invoiceFragment
            ) {
                settingNavigationBarVisibility(View.GONE)
                binding.mainnToolbar.mainToolbar.apply {
                    visibility = View.VISIBLE
                    isTitleCentered = true
                    navigationIcon = null
                    menu.setGroupVisible(R.id.toolbar_menu_group, false)
                }
                binding.mainnToolbar.mainToolbarBottomStroke.visibility = View.VISIBLE

                when (destination.id) {
                    R.id.onboardingFragment -> {
                        binding.mainnToolbar.mainToolbar.visibility = View.GONE
                        binding.mainnToolbar.mainToolbarBottomStroke.visibility = View.GONE
                    }
                    R.id.loginFragment -> {
                        binding.mainnToolbar.mainToolbar.title =
                            resources.getString(R.string.text_login)
                    }

                    R.id.registerFragment -> {
                        binding.mainnToolbar.mainToolbar.title =
                            resources.getString(R.string.text_register)
                    }

                    R.id.registerProfileFragment -> {
                        binding.mainnToolbar.mainToolbar.title =
                            resources.getString(R.string.text_profile)
                    }

                    R.id.invoiceFragment -> {
                        binding.mainnToolbar.mainToolbar.title = "Status"
                    }
                }
            } else {
                binding.mainnToolbar.mainToolbar.apply {
                    title = userPreference.getUserName()
                    isTitleCentered = false
                }

                if (destination.id == R.id.homeFragment ||
                    destination.id == R.id.storeFragment ||
                    destination.id == R.id.wishlistFragment ||
                    destination.id == R.id.transaksiFragment ||
                    destination.id == R.id.storeBottomSheetFragment
                ) {
                    settingNavigationBarVisibility(View.VISIBLE)
                    binding.mainnToolbar.mainToolbar.apply {
                        visibility = View.VISIBLE
                        setNavigationIcon(R.drawable.ic_profile)
                        setNavigationOnClickListener { }
                        menu.setGroupVisible(R.id.toolbar_menu_group, true)
                    }
                    binding.mainnToolbar.mainToolbarBottomStroke.visibility = View.VISIBLE
                } else {
                    settingNavigationBarVisibility(View.GONE)
                    binding.mainBottomNavContainer?.let {
                        it.visibility = View.GONE
                        binding.view.visibility = View.GONE
                    }

                    binding.mainnToolbar.mainToolbar.apply {
                        setNavigationIcon(R.drawable.ic_arrow_back)
                        setNavigationOnClickListener { navController.navigateUp() }
                        menu.setGroupVisible(R.id.toolbar_menu_group, false)
                    }
                    when (destination.id) {
                        R.id.productDetailComposeFragment -> {
                            binding.mainnToolbar.mainToolbar.menu.setGroupVisible(
                                R.id.toolbar_menu_group,
                                true
                            )
                            binding.mainnToolbar.mainToolbar.title =
                                resources.getString(R.string.product_detail)
                        }

                        R.id.productReviewComposeFragment -> {
                            binding.mainnToolbar.mainToolbar.title =
                                resources.getString(R.string.buyer_review)
                        }

                        R.id.storeSearchFragment -> {
                            binding.mainnToolbar.mainToolbar.visibility = View.GONE
                            binding.mainnToolbar.mainToolbarBottomStroke.visibility = View.GONE
                        }

                        R.id.notificationFragment -> {
                            binding.mainnToolbar.mainToolbar.title =
                                getString(R.string.notification_text)
                        }

                        R.id.cartFragment -> {
                            binding.mainnToolbar.mainToolbar.title = getString(R.string.cart_text)
                        }

                        R.id.directBuyFragment -> {
                            binding.mainnToolbar.mainToolbar.title =
                                getString(R.string.checkout_text)
                        }

                        R.id.paymentFragment -> {
                            binding.mainnToolbar.mainToolbar.title =
                                getString(R.string.choose_payment)
                        }
                        R.id.chatFragment -> {
                            binding.mainnToolbar.mainToolbar.title = getString(R.string.chat_text)
                        }
                    }
                }
            }
        }

        binding.mainnToolbar.mainToolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.toolbar_cart -> {
                    navController.navigate(
                        R.id.cartFragment,
                        null,
                        NavOptions.Builder()
                            .setEnterAnim(androidx.navigation.ui.R.anim.nav_default_enter_anim)
                            .setExitAnim(androidx.navigation.ui.R.anim.nav_default_exit_anim)
                            .setPopEnterAnim(androidx.navigation.ui.R.anim.nav_default_pop_enter_anim)
                            .setPopExitAnim(androidx.navigation.ui.R.anim.nav_default_exit_anim)
                            .build()
                    )
                    true
                }

                R.id.toolbar_notifications -> {
                    navController.navigate(
                        R.id.notificationFragment,
                        null,
                        NavOptions.Builder()
                            .setEnterAnim(androidx.navigation.ui.R.anim.nav_default_enter_anim)
                            .setExitAnim(androidx.navigation.ui.R.anim.nav_default_exit_anim)
                            .setPopEnterAnim(androidx.navigation.ui.R.anim.nav_default_pop_enter_anim)
                            .setPopExitAnim(androidx.navigation.ui.R.anim.nav_default_exit_anim)
                            .build()
                    )
                    true
                }

                R.id.toolbar_menu -> {
                    navController.navigate(R.id.chatFragment, null, NavOptions.Builder()
                        .setEnterAnim(androidx.navigation.ui.R.anim.nav_default_enter_anim)
                        .setExitAnim(androidx.navigation.ui.R.anim.nav_default_exit_anim)
                        .setPopEnterAnim(androidx.navigation.ui.R.anim.nav_default_pop_enter_anim)
                        .setPopExitAnim(androidx.navigation.ui.R.anim.nav_default_exit_anim)
                        .build()
                    )
                    true
                }
                else -> false
            }
        }

        /**
         * Setup toolbar cart badge listener
         */
        binding.mainnToolbar.mainToolbar.viewTreeObserver.addOnGlobalLayoutListener {
            viewModel.getCartSize().observe(this) { value ->
                cartBadge.apply {
                    isVisible = value.isNotEmpty()
                    number = value.size
                }
                BadgeUtils.attachBadgeDrawable(
                    cartBadge,
                    binding.mainnToolbar.mainToolbar,
                    R.id.toolbar_cart
                )
            }
        }

        /**
         * Setup toolbar notification badge listener
         */
        binding.mainnToolbar.mainToolbar.viewTreeObserver.addOnGlobalLayoutListener {
            viewModel.getNotificationSize().observe(this) { value ->
                val data = value.filter { !it.isRead }
                notifBadge.apply {
                    isVisible = data.isNotEmpty()
                    number = data.size
                }
                BadgeUtils.attachBadgeDrawable(
                    notifBadge,
                    binding.mainnToolbar.mainToolbar,
                    R.id.toolbar_notifications
                )
            }
        }
    }

    private fun settingNavigationBarVisibility(visibility: Int) {
        binding.mainBottomNavContainer?.visibility = visibility
        binding.mainRailNavContainer?.visibility = visibility
        binding.mainNavContainer?.visibility = visibility
        binding.view.visibility = visibility
    }

    private fun computeWindowSizeClasses(navController: NavController) {
        val metrics = WindowMetricsCalculator.getOrCreate()
            .computeCurrentWindowMetrics(this)

        val widthDp = metrics.bounds.width() /
            resources.displayMetrics.density

        when {
            widthDp < 600f -> {
                viewModel.getWishlistSize().observe(this) { value ->
                    val wishlistBadge =
                        binding.mainBottomNavContainer?.getOrCreateBadge(R.id.wishlistFragment)
                    wishlistBadge?.apply {
                        isVisible = value.isNotEmpty()
                        number = value.size
                    }
                }
                binding.mainBottomNavContainer?.setupWithNavController(navController)
                binding.mainBottomNavContainer?.setOnItemReselectedListener {
                    // Do nothing
                }
            }

            widthDp < 840f -> {
                viewModel.getWishlistSize().observe(this) { value ->
                    val wishlistBadge =
                        binding.mainRailNavContainer?.getOrCreateBadge(R.id.wishlistFragment)
                    wishlistBadge?.apply {
                        isVisible = value.isNotEmpty()
                        number = value.size
                    }
                }
                binding.mainRailNavContainer?.setupWithNavController(navController)
                binding.mainRailNavContainer?.setOnItemReselectedListener {
                    // Do nothing
                }
            }

            else -> {
                binding.mainNavContainer?.setupWithNavController(navController)
            }
        }
    }
}
