package com.jeppung.ecommerce.views.chat

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.jeppung.ecommerce.databinding.ItemMessageHeaderBinding
import com.jeppung.ecommerce.databinding.ItemMessageOtherBinding
import com.jeppung.ecommerce.databinding.ItemMessageUserBinding
import com.jeppung.ecommerce.repository.models.UserMessage
import com.jeppung.ecommerce.repository.prefs.UserPreference
import java.sql.Timestamp
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.Date
import java.util.Locale

class ChatAdapter(
    private val userPreference: UserPreference
) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {
    private var chats = emptyList<UserMessage>()

    inner class ViewHolder(private val binding: ViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: UserMessage, viewType: Int) {
            val messageTime = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(item.timestamp!!)
            when (viewType) {
                MSG_RIGHT -> {
                    val userBinding = binding as ItemMessageUserBinding
                    setChatHolder(userBinding, item, messageTime)
                }
                MSG_LEFT -> {
                    val otherBinding = binding as ItemMessageOtherBinding
                    setChatHolder(otherBinding, item, messageTime)
                }
                else -> {
                    val headerBinding = binding as ItemMessageHeaderBinding
                    headerBinding.itemChatHeader.text = SimpleDateFormat("dd MMM yyyy").format(messageTime)
                    if(item.name == userPreference.getUserName()) {
                        headerBinding.itemChatOther.itemChatOther.visibility = View.GONE
                        headerBinding.itemChatUser.itemChatUser.visibility = View.VISIBLE
                        ViewHolder(headerBinding.itemChatUser).bind(item, MSG_RIGHT)
                    }else{
                        headerBinding.itemChatOther.itemChatOther.visibility = View.VISIBLE
                        headerBinding.itemChatUser.itemChatUser.visibility = View.GONE
                        ViewHolder(headerBinding.itemChatOther).bind(item, MSG_LEFT)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatAdapter.ViewHolder {
        val binding = when (viewType) {
            MSG_HEADER -> {
                ItemMessageHeaderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            }
            MSG_LEFT -> {
                ItemMessageOtherBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            }
            else -> {
                ItemMessageUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            }
        }

        return ViewHolder(binding)
    }

    override fun getItemViewType(position: Int): Int {
        val prevMessageDate = if(position == 0) 0 else SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(chats[position-1].timestamp).date
        val currMessageDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(chats[position].timestamp).date

        return if(currMessageDate > prevMessageDate){
            MSG_HEADER
        }else if (chats[position].name == userPreference.getUserName()) {
            MSG_RIGHT
        }
        else {
            MSG_LEFT
        }
    }

    override fun onBindViewHolder(holder: ChatAdapter.ViewHolder, position: Int) {
        holder.bind(chats[position], holder.itemViewType)
    }

    override fun getItemCount(): Int = chats.size

    fun setData(newData: List<UserMessage>) {
        chats = newData
        notifyDataSetChanged()
    }

    private fun setChatHolder(binding: ViewBinding, item: UserMessage, messageTime: Date) {
        when(binding) {
            is ItemMessageUserBinding -> {
                binding.itemChatUsername.text = item.name
                binding.itemChatText.text = item.message
                binding.itemChatTime.text = SimpleDateFormat("HH:mm").format(messageTime)
            }
            is ItemMessageOtherBinding -> {
                binding.itemChatUsername.text = item.name
                binding.itemChatText.text = item.message
                binding.itemChatTime.text = SimpleDateFormat("HH:mm").format(messageTime)
            }
        }
    }

    companion object {
        const val MSG_LEFT = 0
        const val MSG_RIGHT = 1
        const val MSG_HEADER = 2
    }
}

//class ChatDiffUtil(
//    private val oldChat: List<UserMessage>,
//    private val newChat: List<UserMessage>
//): DiffUtil.Callback() {
//    override fun getOldListSize(): Int = oldChat.size
//
//    override fun getNewListSize(): Int = newChat.size
//
//    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
//        return oldChat[oldItemPosition] === newChat[newItemPosition]
//    }
//
//    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
//        return oldChat[oldItemPosition] == newChat[newItemPosition]
//    }
//
//}