package com.jeppung.ecommerce.views.cart

import androidx.lifecycle.ViewModel
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.entity.Cart
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val appDatabase: AppDatabase
) : ViewModel() {

//    private val _allCartProduct = MutableLiveData<List<Cart>>()
//    val allCartProduct: LiveData<List<Cart>> = _allCartProduct

    fun getAllCartProduct() = appDatabase.cartDao().getAllCartProduct()

    fun updateCartProduct(product: Cart) {
        appDatabase.cartDao().updateCartProduct(product)
    }

    fun setAllCartProductCheckedValue(value: Int) {
        appDatabase.cartDao().setAllCartProductCheckedValue(value)
    }

    fun getCheckedProduct(): List<Cart> {
        return appDatabase.cartDao().getCheckedCartProduct()
    }

    fun deleteCartProduct(product: Cart) {
        appDatabase.cartDao().deleteCartProductWithId(product.productId)
    }

    init {
        getAllCartProduct()
    }
}
