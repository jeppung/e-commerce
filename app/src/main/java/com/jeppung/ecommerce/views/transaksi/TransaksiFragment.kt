package com.jeppung.ecommerce.views.transaksi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentTransaksiBinding
import com.jeppung.ecommerce.repository.models.InvoiceData
import com.jeppung.ecommerce.repository.models.Transaction
import com.jeppung.ecommerce.repository.prefs.UserPreference
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class TransaksiFragment : Fragment() {

    private lateinit var binding: FragmentTransaksiBinding
    private val viewModel: TransactionViewModel by activityViewModels()
    private lateinit var adapter: TransactionItemAdapter

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    @Inject
    lateinit var userPreference: UserPreference

    @Inject
    lateinit var appDatabase: AppDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTransaksiBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.transactionRecycleView.layoutManager = LinearLayoutManager(requireContext())

        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.transactionUiState.collect {
                    when (it) {
                        is TransactionUiState.Loading -> binding.transactionProgressBar.visibility = View.VISIBLE
                        is TransactionUiState.Error -> {
                            binding.transactionProgressBar.visibility = View.GONE
                            if (it.message == "Not Found") {
                                binding.transactionRecycleView.visibility = View.GONE
                                binding.transactionErrorView.storeErrorView.visibility = View.VISIBLE
                                binding.transactionErrorView.storeErrorButton.visibility = View.VISIBLE
                                binding.transactionErrorView.storeErrorButton.text = resources.getString(
                                    R.string.button_refresh
                                )
                                binding.transactionErrorView.storeErrorButton.setOnClickListener {
                                    viewModel.getAllTransaction()
                                }
                            } else {
                                AppUtils.authTimeoutHandler(userPreference, appDatabase)
                                view!!.findNavController().navigate(R.id.action_global_pre_login)
                            }
                        }
                        is TransactionUiState.Success -> {
                            binding.transactionProgressBar.visibility = View.GONE
                            setupTransactionRecycleView(it.data)
                        }
                    }
                }
            }
        }

        viewModel.getAllTransaction()
    }

    private fun setupTransactionRecycleView(data: List<Transaction>) {
        if (data.isEmpty()) {
            binding.transactionRecycleView.visibility = View.GONE
            binding.transactionErrorView.storeErrorView.visibility = View.VISIBLE
            binding.transactionErrorView.storeErrorButton.visibility = View.VISIBLE
            binding.transactionErrorView.storeErrorButton.text = resources.getString(R.string.button_refresh)
            binding.transactionErrorView.storeErrorButton.setOnClickListener {
                viewModel.getAllTransaction()
            }
        } else {
            binding.transactionRecycleView.visibility = View.VISIBLE
            binding.transactionErrorView.apply {
                storeErrorView.visibility = View.GONE
                storeErrorButton.visibility = View.GONE
            }
            adapter = TransactionItemAdapter(data.reversed()) { transaction ->
                firebaseAnalytics.logEvent("BUTTON CLICKED") {
                    param("transaction", "beri ulasan")
                }
                val bundle = Bundle().apply {
                    putParcelable(
                        "invoiceData",
                        InvoiceData(
                            invoiceId = transaction.invoiceId,
                            payment = transaction.payment,
                            date = transaction.date,
                            status = transaction.status,
                            time = transaction.time,
                            total = transaction.total
                        )
                    )
                }
                view!!.findNavController()
                    .navigate(R.id.action_transaksiFragment_to_invoiceFragment, bundle)
            }
            binding.transactionRecycleView.adapter = adapter
        }
    }
}
