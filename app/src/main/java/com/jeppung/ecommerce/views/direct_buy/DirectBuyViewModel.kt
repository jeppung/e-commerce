package com.jeppung.ecommerce.views.direct_buy

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.jeppung.ecommerce.repository.api_service.ApiService
import com.jeppung.ecommerce.repository.models.FulfillmentRequest
import com.jeppung.ecommerce.repository.models.FullfillmentItem
import com.jeppung.ecommerce.repository.models.InvoiceData
import com.jeppung.ecommerce.repository.models.InvoiceResponse
import com.jeppung.ecommerce.repository.room.AppDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class DirectBuyViewModel @Inject constructor(
    private var appDatabase: AppDatabase,
    private var apiService: ApiService
) : ViewModel() {

    private val _setLoadingProcessPayment = MutableLiveData<Boolean>()
    val setLoadingProcessPayment: LiveData<Boolean> = _setLoadingProcessPayment

    fun processPayment(
        data: FulfillmentRequest,
        callback: PaymentProcessListener,
        onSuccess: (InvoiceData, List<FullfillmentItem>) -> Unit
    ) {
        viewModelScope.launch {
            _setLoadingProcessPayment.value = true
            try {
                val response = apiService.processPayment(data)
                _setLoadingProcessPayment.value = false
                onSuccess(response.data, data.items)
            } catch (e: Exception) {
                _setLoadingProcessPayment.value = false
                if (e is HttpException) {
                    val errResponse = Gson().fromJson(
                        e.response()!!.errorBody()!!.string(),
                        InvoiceResponse::class.java
                    )
                    callback.onFailure(Throwable(errResponse.message))
                } else {
                    callback.onFailure(Throwable(e.message))
                }
            }
        }
    }

    fun deleteRelatedProductFromCart(productId: String) {
        appDatabase.cartDao().deleteCartProductWithId(productId)
    }
}

interface PaymentProcessListener {
    fun onFailure(err: Throwable)
}
