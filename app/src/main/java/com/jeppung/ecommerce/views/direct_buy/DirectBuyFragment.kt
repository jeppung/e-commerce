package com.jeppung.ecommerce.views.direct_buy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentDirectBuyBinding
import com.jeppung.ecommerce.repository.models.FulfillmentRequest
import com.jeppung.ecommerce.repository.models.FullfillmentItem
import com.jeppung.ecommerce.repository.models.PaymentMethod
import com.jeppung.ecommerce.repository.room.entity.Cart
import com.jeppung.ecommerce.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DirectBuyFragment : Fragment() {

    private lateinit var binding: FragmentDirectBuyBinding
    private lateinit var viewModel: DirectBuyViewModel
    private lateinit var products: List<Cart>
    private var totalPrice: Long = 0L
    private lateinit var source: String
    private var paymentImage: String = ""
    private var paymentLabel: String = ""

    private lateinit var adapter: DirectBuyItemAdapter
    private var callback: OnDirectBuyProductListener = object : OnDirectBuyProductListener {
        override fun onCartProductQtyChanged(product: Cart, productList: List<Cart>) {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("direct_buy", "item qty change")
            }
            binding.directBuyTotalPrice.text =
                AppUtils.formatCurrencyIDR(productList.sumOf { it.productPrice * it.quantity })
        }
    }

    private var paymentProcessCallback: PaymentProcessListener = object : PaymentProcessListener {
        override fun onFailure(err: Throwable) {
            Snackbar.make(view!!, err.message.toString(), Snackbar.LENGTH_SHORT).show()
        }
    }

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics
    private val currency = "IDR"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        parentFragmentManager.setFragmentResultListener(
            "requestKey",
            requireActivity()
        ) { _, bundle ->
            val paymentMethod =
                bundle.getParcelable<PaymentMethod>("paymentMethod")!!

            val temp: MutableList<Bundle> = mutableListOf()
            products.forEach {
                temp.add(
                    Bundle().apply {
                        putString(FirebaseAnalytics.Param.ITEM_ID, it.productId)
                        putString(FirebaseAnalytics.Param.ITEM_NAME, it.productTitle)
                        putString(FirebaseAnalytics.Param.ITEM_VARIANT, it.productVariant)
                        putInt(
                            FirebaseAnalytics.Param.PRICE,
                            it.productPrice.toInt()
                        )
                    }
                )
            }

            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO) {
                param(FirebaseAnalytics.Param.CURRENCY, currency)
                param(FirebaseAnalytics.Param.VALUE, totalPrice)
                param(FirebaseAnalytics.Param.PAYMENT_TYPE, paymentMethod.label)
                param(FirebaseAnalytics.Param.ITEMS, temp.toTypedArray())
            }

            paymentLabel = paymentMethod.label
            paymentImage = paymentMethod.image
            binding.directBuyScrollView.scrollTo(
                0,
                binding.directBuyScrollView.getChildAt(0).height
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDirectBuyBinding.inflate(inflater)
        viewModel = ViewModelProvider(requireActivity())[DirectBuyViewModel::class.java]

        products = arguments?.getParcelableArrayList<Cart>("products")!!
        totalPrice = arguments?.getLong("totalPrice")!!
        source = arguments?.getString("source")!!

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val temp: MutableList<Bundle> = mutableListOf()
        products.forEach {
            temp.add(
                Bundle().apply {
                    putString(FirebaseAnalytics.Param.ITEM_ID, it.productId)
                    putString(FirebaseAnalytics.Param.ITEM_NAME, it.productTitle)
                    putString(FirebaseAnalytics.Param.ITEM_VARIANT, it.productVariant)
                    putInt(
                        FirebaseAnalytics.Param.PRICE,
                        it.productPrice.toInt()
                    )
                }
            )
        }

        binding.directBuyListItem.layoutManager = LinearLayoutManager(requireContext())
        adapter = DirectBuyItemAdapter(products, callback)
        binding.directBuyListItem.adapter = adapter

        binding.directBuyTotalPrice.text =
            AppUtils.formatCurrencyIDR(products.sumOf { it.productPrice * it.quantity })

        if (paymentImage.isNotEmpty() && paymentLabel.isNotEmpty()) {
            binding.directBuyPaymentMethodLabel.text = paymentLabel
            Glide.with(requireContext()).load(paymentImage)
                .into(binding.directBuyPaymentMethodImage)
            binding.directBuyPayBtn.isEnabled = true
        }

        binding.directBuyPaymentMethodBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("direct_buy", "select payment method")
            }
            view.findNavController().navigate(R.id.action_directBuyFragment_to_paymentFragment)
        }

        viewModel.setLoadingProcessPayment.observe(viewLifecycleOwner) { value ->
            if (value) {
                binding.directBuyPaymentProcessProgressBar.visibility = View.VISIBLE
            } else {
                binding.directBuyPaymentProcessProgressBar.visibility = View.GONE
            }
        }

        binding.directBuyPayBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("direct_buy", "pay")
            }
            val listItem: MutableList<FullfillmentItem> = mutableListOf()

            products.forEach { product ->
                listItem.add(
                    FullfillmentItem(
                        productId = product.productId,
                        variantName = product.productVariant,
                        quantity = product.quantity
                    )
                )
            }
            val data = FulfillmentRequest(
                payment = paymentLabel,
                items = listItem
            )

            viewModel.processPayment(data, paymentProcessCallback) { data, fulfillmentItem ->
                if (source == "cart") {
                    fulfillmentItem.forEach {
                        try {
                            viewModel.deleteRelatedProductFromCart(it.productId)
                        } catch (e: Exception) {
                            // ignore
                        }
                    }
                }

                val bundle = Bundle().apply {
                    putParcelable("invoiceData", data)
                }

                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.PURCHASE) {
                    param(FirebaseAnalytics.Param.TRANSACTION_ID, data.invoiceId)
                    param(FirebaseAnalytics.Param.CURRENCY, currency)
                    param(FirebaseAnalytics.Param.VALUE, totalPrice)
                    param(FirebaseAnalytics.Param.ITEMS, temp.toTypedArray())
                }

                view.findNavController()
                    .navigate(R.id.action_directBuyFragment_to_invoiceFragment, bundle)
            }
        }

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT) {
            param(FirebaseAnalytics.Param.CURRENCY, currency)
            param(FirebaseAnalytics.Param.VALUE, totalPrice)
            param(FirebaseAnalytics.Param.ITEMS, temp.toTypedArray())
        }
    }
}
