package com.jeppung.ecommerce.views.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.databinding.FragmentNotificationBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class NotificationFragment : Fragment() {

    private lateinit var binding: FragmentNotificationBinding
    private lateinit var viewModel: NotificationViewModel

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    private val adapter by lazy {
        NotificationItemAdapter {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("notification", "notification item")
            }
            try {
                lifecycleScope.launch {
                    delay(500) // to not instanly update the data (show the ripple first)
                    viewModel.updateIsReadStatus(it)
                }
            } catch (e: Exception) {
                // Ignore
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNotificationBinding.inflate(inflater)
        viewModel = ViewModelProvider(requireActivity())[NotificationViewModel::class.java]

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.notificationRecycleView.adapter = adapter

        binding.notificationRecycleView.layoutManager = LinearLayoutManager(requireContext())
        viewModel.getNotifications().observe(viewLifecycleOwner) { data ->
            if (data.isNullOrEmpty()) {
                binding.notificationErrorView.storeErrorView.visibility = View.VISIBLE
                binding.notificationRecycleView.visibility = View.GONE
                binding.notificationErrorView.storeErrorButton.visibility = View.GONE
            } else {
                binding.notificationErrorView.storeErrorView.visibility = View.GONE
                binding.notificationRecycleView.visibility = View.VISIBLE
                adapter.updateNotificationList(data)
            }
        }
    }
}
