package com.jeppung.ecommerce.views.payment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.remoteconfig.ConfigUpdate
import com.google.firebase.remoteconfig.ConfigUpdateListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigException
import com.google.gson.Gson
import com.jeppung.ecommerce.repository.models.PaymentList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PaymentViewModel @Inject constructor(
    private val remoteConfig: FirebaseRemoteConfig,
) : ViewModel() {

    private val _paymentListUiState = MutableSharedFlow<PaymentListUiState>()
    val paymentListUiState: SharedFlow<PaymentListUiState> = _paymentListUiState

    private fun getRealTimePaymentUpdate() {
        remoteConfig.addOnConfigUpdateListener(object : ConfigUpdateListener {
            override fun onUpdate(configUpdate: ConfigUpdate) {
                if (configUpdate.updatedKeys.contains("payment")) {
                    remoteConfig.activate().addOnCompleteListener {
                        viewModelScope.launch {
                            _paymentListUiState.emit(PaymentListUiState.Loading)
                            val paymentList =
                                Gson().fromJson(remoteConfig.getString("payment"), Array<PaymentList>::class.java)
                                    .toList()
                            _paymentListUiState.emit(PaymentListUiState.Success(paymentList))
                        }
                    }
                }
            }
            override fun onError(error: FirebaseRemoteConfigException) {
                viewModelScope.launch {
                    _paymentListUiState.emit(PaymentListUiState.Error)
                }
            }
        })
    }

    private fun getAllPaymentMethod() {
        viewModelScope.launch {
            _paymentListUiState.emit(PaymentListUiState.Loading)
            val paymentList =
                Gson().fromJson(remoteConfig.getString("payment"), Array<PaymentList>::class.java)
                    .toList()
            _paymentListUiState.emit(PaymentListUiState.Success(paymentList))
        }
    }

    init {
        getRealTimePaymentUpdate()
        getAllPaymentMethod()
    }
}

sealed class PaymentListUiState {
    object Error : PaymentListUiState()
    data class Success(val data: List<PaymentList>) : PaymentListUiState()
    object Loading : PaymentListUiState()
}
