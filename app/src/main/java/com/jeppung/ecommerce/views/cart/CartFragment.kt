package com.jeppung.ecommerce.views.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentCartBinding
import com.jeppung.ecommerce.repository.room.entity.Cart
import com.jeppung.ecommerce.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CartFragment : Fragment() {

    private lateinit var binding: FragmentCartBinding
    private lateinit var viewModel: CartViewModel
    private lateinit var adapter: CartItemAdapter
    private val currency = "IDR"

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics
    private var callback: OnCartProductListener = object : OnCartProductListener {
        override fun onCartProductChecked(product: Cart, productList: List<Cart>) {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("cart", "cart item check")
            }
            viewModel.updateCartProduct(product)
            viewModel.getAllCartProduct()
        }

        override fun onRemoveCartProductClicked(product: Cart) {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("cart", "remove item cart")
            }
            viewModel.deleteCartProduct(product)

            val productBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, product.productId)
                putString(FirebaseAnalytics.Param.ITEM_NAME, product.productTitle)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, product.productVariant)
                putInt(FirebaseAnalytics.Param.QUANTITY, product.quantity)
                putInt(FirebaseAnalytics.Param.PRICE, product.productPrice.toInt())
            }
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.REMOVE_FROM_CART) {
                param(FirebaseAnalytics.Param.CURRENCY, currency)
                param(FirebaseAnalytics.Param.VALUE, product.quantity * product.productPrice)
                param(FirebaseAnalytics.Param.ITEMS, arrayOf(productBundle))
            }

            viewModel.getAllCartProduct()
            Snackbar.make(view!!, "Product deleted from cart!", Snackbar.LENGTH_SHORT).show()
        }

        override fun onCartProductQtyChanged(product: Cart) {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("cart", "update item cart qty")
            }
            viewModel.updateCartProduct(product)
            viewModel.getAllCartProduct()
        }

        override fun onTotalPriceChanged(totalPrice: Long) {
            binding.cartTotalPrice.text = AppUtils.formatCurrencyIDR(totalPrice)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCartBinding.inflate(inflater)
        viewModel = ViewModelProvider(requireActivity())[CartViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cartRecycleView.layoutManager = LinearLayoutManager(requireContext())
        adapter = CartItemAdapter(callback)
        binding.cartRecycleView.adapter = adapter

        viewModel.getAllCartProduct().observe(viewLifecycleOwner) { data ->
            if (data.isNullOrEmpty()) {
                binding.cartSuccessView.visibility = View.INVISIBLE
                binding.cartErrorView.storeErrorView.visibility = View.VISIBLE
                binding.cartErrorView.storeErrorButton.visibility = View.GONE

                cartBtnValidation(data)
                adapter.updateData(data)
            } else {
                val temp: MutableList<Bundle> = mutableListOf()
                data.forEach { product ->
                    temp.add(
                        Bundle().apply {
                            putString(FirebaseAnalytics.Param.ITEM_ID, product.productId)
                            putString(FirebaseAnalytics.Param.ITEM_NAME, product.productTitle)
                            putString(FirebaseAnalytics.Param.ITEM_VARIANT, product.productVariant)
                            putInt(FirebaseAnalytics.Param.QUANTITY, product.quantity)
                            putInt(FirebaseAnalytics.Param.PRICE, product.productPrice.toInt())
                        }
                    )
                }
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_CART) {
                    param(FirebaseAnalytics.Param.ITEMS, temp.toTypedArray())
                }

                cartBtnValidation(data)
                adapter.updateData(data)
            }
        }

        binding.cartCheckboxAll.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("cart", "check all cart item")
            }
            if (binding.cartCheckboxAll.isChecked) {
                viewModel.setAllCartProductCheckedValue(1)
                viewModel.getAllCartProduct()
            } else {
                viewModel.setAllCartProductCheckedValue(0)
                viewModel.getAllCartProduct()
            }
        }

        binding.cartDeleteAllCheckedProductBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("cart", "delete all cart item")
            }
            val products = viewModel.getCheckedProduct()

            val temp: MutableList<Bundle> = mutableListOf()
            products.forEach { product ->
                viewModel.deleteCartProduct(product)
                temp.add(
                    Bundle().apply {
                        putString(FirebaseAnalytics.Param.ITEM_ID, product.productId)
                        putString(FirebaseAnalytics.Param.ITEM_NAME, product.productTitle)
                        putString(FirebaseAnalytics.Param.ITEM_VARIANT, product.productVariant)
                        putInt(FirebaseAnalytics.Param.PRICE, product.productPrice.toInt())
                    }
                )
            }
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.REMOVE_FROM_CART) {
                param(FirebaseAnalytics.Param.CURRENCY, currency)
                param(
                    FirebaseAnalytics.Param.ITEMS,
                    temp.toTypedArray()
                )
            }
            viewModel.getAllCartProduct()
            binding.cartCheckboxAll.isChecked =
                false // if cart was set to true, it will remove the checked value
        }

        binding.cartBuyBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("cart", "buy")
            }
            val products = viewModel.getCheckedProduct()

            val bundle = Bundle().apply {
                putParcelableArrayList("products", ArrayList(products))
                putLong(
                    "totalPrice",
                    AppUtils.parseCurrencyIdr(binding.cartTotalPrice.text.toString())
                )
                putString("source", "cart")
            }

            view.findNavController().navigate(R.id.action_cartFragment_to_directBuyFragment, bundle)
        }
    }

    private fun cartBtnValidation(data: List<Cart>) {
        binding.cartCheckboxAll.isChecked = if (data.isEmpty()) {
            false
        } else {
            data.filter { it.isChecked }.size == data.size
        }

        if (data.any { it.isChecked }) {
            binding.cartBuyBtn.isEnabled = true
            binding.cartDeleteAllCheckedProductBtn.visibility = View.VISIBLE
        } else {
            binding.cartBuyBtn.isEnabled = false
            binding.cartDeleteAllCheckedProductBtn.visibility = View.INVISIBLE
        }
    }
}
