package com.jeppung.ecommerce.views.product_review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Star
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.Typography
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.repository.models.ProductReviewData

class ProductReviewComposeFragment : Fragment() {

    private lateinit var productId: String
    private var productReview: List<ProductReviewData>? = null
    private lateinit var viewModel: ProductReviewViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(requireActivity())[ProductReviewViewModel::class.java]
        productId = arguments?.getString("product_id")!!

        return ComposeView(requireContext()).apply {
            setContent {
                val uiState by viewModel.productReviewUiState.observeAsState()

                LaunchedEffect(this) {
                    if (productId == viewModel.getSavedProductId()) {
                        productReview = viewModel.getSavedProductReview()!!
                    } else {
                        viewModel.productReview(productId)
                    }
                }

                ProductReviewComposeTheme {
                    Surface(
                        color = MaterialTheme.colorScheme.background
                    ) {
                        uiState?.let {
                            when (it) {
                                is ProductReviewUiState.Loading -> {
                                    Box(
                                        contentAlignment = Alignment.Center
                                    ) {
                                        CircularProgressIndicator()
                                    }
                                }

                                is ProductReviewUiState.Error -> {
                                    ProductReviewErrorScreen(
                                        viewModel = viewModel,
                                        productId = productId
                                    )
                                }

                                is ProductReviewUiState.Success -> {
                                    ProductReviewScreen(
                                        reviews = productReview ?: (uiState as ProductReviewUiState.Success).data
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun ProductReviewErrorScreen(viewModel: ProductReviewViewModel, productId: String) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_error),
            contentDescription = null,
            modifier = Modifier.size(128.dp)
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(text = stringResource(id = R.string.text_empty_title), style = MaterialTheme.typography.headlineMedium)
        Spacer(modifier = Modifier.height(4.dp))
        Text(
            text = stringResource(id = R.string.text_empty_description),
            style = MaterialTheme.typography.bodyLarge
        )
        Spacer(modifier = Modifier.height(8.dp))
        Button(onClick = {
            viewModel.productReview(productId)
        }) {
            Text(stringResource(id = R.string.button_refresh))
        }
    }
}

@Composable
fun ProductReviewScreen(reviews: List<ProductReviewData>) {
    LazyColumn {
        items(reviews.size) { index ->
            ReviewItem(review = reviews[index])
            Divider(thickness = 1.dp, color = Color(0xFFCAC4D0))
        }
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun ReviewItem(review: ProductReviewData) {
    Column(modifier = Modifier.padding(16.dp)) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            GlideImage(
                model = review.userImage,
                contentDescription = null,
                modifier = Modifier
                    .size(36.dp)
                    .clip(
                        CircleShape
                    )
            ) {
                it.load(review.userImage).error(R.drawable.thumbnail)
            }
            Spacer(modifier = Modifier.width(8.dp))
            Column {
                Text(
                    review.userName,
                    style = MaterialTheme.typography.titleLarge.copy(fontSize = 12.sp)
                )
                RatingBar(rating = review.userRating)
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            review.userReview,
            modifier = Modifier.fillMaxWidth(),
            style = MaterialTheme.typography.bodySmall
        )
    }
}

@Composable
fun RatingBar(rating: Int) {
    Row {
        repeat(5) { i ->
            Icon(
                imageVector = Icons.Outlined.Star,
                tint = if (i < rating) MaterialTheme.colorScheme.onBackground else MaterialTheme.colorScheme.outlineVariant,
                contentDescription = null,
                modifier = Modifier.size(12.dp)
            )
        }
    }
}

@Composable
fun ProductReviewComposeTheme(
    content: @Composable () -> Unit
) {
    val fonts = FontFamily(
        Font(R.font.poppins_regular_400),
        Font(R.font.poppins_medium_500, weight = FontWeight.Medium),
        Font(R.font.poppins_semibold_600, weight = FontWeight.SemiBold),
        Font(R.font.poppins_bold_700, weight = FontWeight.Bold)
    )

    val typography = Typography(
        labelLarge = TextStyle(
            fontFamily = fonts,
            fontSize = 14.sp,
            fontWeight = FontWeight.Medium,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false,
            )
        ),
        bodySmall = TextStyle(
            fontFamily = fonts,
            fontSize = 12.sp,
            color = Color(0xFF49454F),
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        bodyMedium = TextStyle(
            fontFamily = fonts,
            fontSize = 14.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        bodyLarge = TextStyle(
            fontFamily = fonts,
            fontSize = 16.sp,
        ),
        titleMedium = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.Medium,
            color = Color(0xFF49454F),
            fontSize = 16.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        titleLarge = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.SemiBold,
            fontSize = 20.sp,
            color = Color(0xFF49454F),
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        headlineMedium = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.Medium,
            fontSize = 32.sp
        )
    )

    val darkTypography = Typography(
        labelLarge = TextStyle(
            fontFamily = fonts,
            fontSize = 14.sp,
            fontWeight = FontWeight.Medium,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false,
            )
        ),
        bodySmall = TextStyle(
            fontFamily = fonts,
            fontSize = 12.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        bodyMedium = TextStyle(
            fontFamily = fonts,
            fontSize = 14.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        bodyLarge = TextStyle(
            fontFamily = fonts,
            fontSize = 16.sp,
        ),
        titleMedium = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.Medium,
            fontSize = 16.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        titleLarge = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.SemiBold,
            fontSize = 20.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        headlineMedium = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.Medium,
            fontSize = 32.sp
        )
    )

    val DarkColorScheme = darkColorScheme(
        background = colorResource(id = R.color.blackSurfaceContainerLowest)
    )

    val LightColorScheme = lightColorScheme(
        background = colorResource(id = R.color.white)
    )

    MaterialTheme(
        typography = if (isSystemInDarkTheme()) darkTypography else typography,
        content = content,
        colorScheme = if (isSystemInDarkTheme()) DarkColorScheme else LightColorScheme
    )
}
