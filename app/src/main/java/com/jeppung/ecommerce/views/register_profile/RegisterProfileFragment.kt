package com.jeppung.ecommerce.views.register_profile

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentRegisterProfileBinding
import com.jeppung.ecommerce.repository.prefs.UserPreference
import com.jeppung.ecommerce.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

@AndroidEntryPoint
class RegisterProfileFragment : Fragment() {

    private lateinit var binding: FragmentRegisterProfileBinding
    private val viewModel: RegisterProfileViewModel by viewModels()

    @Inject
    lateinit var userPreference: UserPreference

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    private var multiPartImage: MultipartBody.Part? = null
    private var imageUri: Uri? = null
    private val startCameraActivity = registerForActivityResult(
        ActivityResultContracts.TakePicture()
    ) { isPictureTaken ->
        if (isPictureTaken) {
            IMAGE_SOURCE = CAMERA
            processingImage(imageUri!!)
            binding.registerProfilePlaceholder.visibility = View.GONE
            Glide.with(requireContext())
                .asBitmap()
                .load(imageUri)
                .into(binding.registerProfileImage)
        } else {
            // Ignore if user dont take picture and exit camera
        }
    }
    private val startGalleryActivity = registerForActivityResult(
        ActivityResultContracts.GetContent()
    ) { uri ->
        if (uri != null) {
            IMAGE_SOURCE = GALLERY
            processingImage(uri)
            binding.registerProfilePlaceholder.visibility = View.GONE
            Glide.with(requireContext()).load(imageUri)
                .placeholder(R.drawable.ic_profile)
                .centerCrop()
                .into(binding.registerProfileImage)
        } else {
            // Ignore if user dont select image
        }
    }

    private val startRequestCameraPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                launchCamera()
            } else {
                Toast.makeText(requireContext(), "Camera permission denied", Toast.LENGTH_SHORT).show()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterProfileBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val cameraOption = arrayOf(
            resources.getString(R.string.text_camera),
            resources.getString(R.string.text_gallery)
        )

        /**
         * RegisterProfileUiState validation
         */
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.registerProfileUiState.collect {
                    when (it) {
                        is RegisterProfileUiState.Loading -> {
                            binding.registerProfileProgressBar.visibility = View.VISIBLE
                        }

                        is RegisterProfileUiState.Success -> {
                            binding.registerProfileProgressBar.visibility = View.GONE

                            val response = it.data
                            Toast.makeText(
                                requireContext(),
                                response.message,
                                Toast.LENGTH_SHORT
                            ).show()

                            userPreference.setUserName(
                                response.data!!.userName
                            )
                            userPreference.setUserImage(
                                response.data.userImage
                            )

                            view.findNavController()
                                .navigate(R.id.action_global_main)
                        }

                        is RegisterProfileUiState.Error -> {
                            binding.registerProfileProgressBar.visibility = View.GONE
                            Toast.makeText(
                                requireContext(),
                                it.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
        }

        /**
         * Handling image chooser
         */
        binding.registerProfileImageContainer.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("register_profile", "image_container")
            }
            val builder = AlertDialog.Builder(requireContext())
            builder.apply {
                setTitle(resources.getString(R.string.text_choose_image))
                setItems(cameraOption) { _, i ->
                    when (i) {
                        0 -> {
                            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                                param("register_profile", "camera")
                            }
                            imageFromCameraHandler()
                        }

                        1 -> {
                            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                                param("register_profile", "camera")
                            }
                            imageFromGalleryHandler()
                        }
                    }
                }
                show()
            }
        }

        /**
         * Handling name validation
         */
        binding.registerProfileNameInput.addTextChangedListener { input ->
            binding.registerProfileFinishBtn.isEnabled = input!!.isNotEmpty()
        }

        /**
         * Handling registerProfile submission
         */
        binding.registerProfileFinishBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("register_profile", "finish")
            }
            val username = RequestBody.create(
                "text/plain".toMediaTypeOrNull(),
                binding.registerProfileNameInput.text.toString().trim()
            )

            if (imageUri != null) {
                when (IMAGE_SOURCE) {
                    CAMERA -> {
                        val file = File(imageUri!!.path!!)
                        val requestFile =
                            RequestBody.create("image/*".toMediaTypeOrNull(), file)
                        multiPartImage =
                            MultipartBody.Part.createFormData(
                                "userImage",
                                file.name,
                                requestFile
                            )
                    }

                    GALLERY -> {
                        val file = File(imageUri!!.path!!)
                        val requestFile =
                            RequestBody.create("image/*".toMediaTypeOrNull(), file)
                        multiPartImage =
                            MultipartBody.Part.createFormData(
                                "userImage",
                                file.name,
                                requestFile
                            )
                    }

                    else -> {
                        multiPartImage = null
                    }
                }
            }

            viewModel.registerProfile(
                username = username,
                image = multiPartImage,
            )
        }

        /** Configuring spanned bottom text */
        val spannedBottomInfo = AppUtils.setSpannableBottomInfo(
            requireContext(),
            R.string.terms_and_conditions_login,
            resources.getString(R.string.terms),
            resources.getString(R.string.conditions)
        )
        binding.registerProfileBottomInfo.text = spannedBottomInfo
    }

    private fun launchCamera() {
        try {
            imageUri = FileProvider.getUriForFile(requireContext(), requireContext().packageName, createTempFile())
            startCameraActivity.launch(imageUri)
        } catch (e: Exception) {
            Toast
                .makeText(requireContext(), e.message, Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun imageFromCameraHandler() {
        val initialCameraPermission = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA,
        )
        if (initialCameraPermission != PackageManager.PERMISSION_GRANTED) {
            startRequestCameraPermission.launch(Manifest.permission.CAMERA)
        } else {
            launchCamera()
        }
    }

    private fun imageFromGalleryHandler() {
        startGalleryActivity.launch(
            "image/*"
        )
    }

    private fun getRotationAngle(imageUri: Uri): Int {
        val inputStream = requireContext().contentResolver.openInputStream(imageUri)
        inputStream?.let {
            val exifInterface = ExifInterface(it)
            val rotation = exifInterface.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )
            return when (rotation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> 90
                ExifInterface.ORIENTATION_ROTATE_180 -> 180
                ExifInterface.ORIENTATION_ROTATE_270 -> 270
                else -> 0
            }
        }
        return 0
    }

    private fun processingImage(imageUri: Uri) {
        val tempFile = createTempFile()

        requireContext().contentResolver.openInputStream(imageUri).use { input ->
            val rotationDegree = getRotationAngle(imageUri)
            val outputStream = ByteArrayOutputStream()
            val bitmap = BitmapFactory.decodeStream(input)
            val processedBitmapImage = Bitmap.createBitmap(
                bitmap,
                0,
                0,
                bitmap.width,
                bitmap.height,
                Matrix().apply {
                    postRotate(rotationDegree.toFloat())
                },
                true
            )
            val cropped = ThumbnailUtils.extractThumbnail(processedBitmapImage, 512, 512)
            cropped.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)

            if (IMAGE_SOURCE == CAMERA) {
                requireContext().contentResolver.openOutputStream(imageUri).use {
                    cropped.compress(Bitmap.CompressFormat.JPEG, 100, it)
                }
            } else {
                FileOutputStream(tempFile).use { output ->
                    outputStream.toByteArray().inputStream()
                        .copyTo(output)
                }
                this.imageUri = tempFile.toUri()
            }
        }
    }

    private fun createTempFile(): File {
        val tempDir = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile("image_${System.currentTimeMillis()}", ".jpg", tempDir)
    }

    companion object {
        private var IMAGE_SOURCE = ""
        private const val GALLERY = "GALLERY"
        private const val CAMERA = "CAMERA"
    }
}
