package com.jeppung.ecommerce.views.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jeppung.ecommerce.databinding.ItemOnboardingBinding

class OnboardingViewAdapter(private val items: List<Int>) :
    RecyclerView.Adapter<OnboardingViewAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemOnboardingBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemOnboardingBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.onboardingScreenImage2.setImageDrawable(
            ContextCompat.getDrawable(holder.itemView.context, items[position])
        )
    }

    override fun getItemCount(): Int = items.size
}
