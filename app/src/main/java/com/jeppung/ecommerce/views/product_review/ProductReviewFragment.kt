// package com.jeppung.ecommerce.views.product_review
//
// import android.os.Bundle
// import android.view.LayoutInflater
// import android.view.View
// import android.view.ViewGroup
// import android.widget.Toast
// import androidx.fragment.app.Fragment
// import androidx.lifecycle.ViewModelProvider
// import androidx.recyclerview.widget.DividerItemDecoration
// import androidx.recyclerview.widget.LinearLayoutManager
// import com.jeppung.ecommerce.databinding.FragmentProductReviewBinding
//
//
// class ProductReviewFragment : Fragment() {
//
//    private lateinit var binding: FragmentProductReviewBinding
//    private lateinit var viewModel: ProductReviewViewModel
//    private lateinit var productId: String
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View {
//        binding = FragmentProductReviewBinding.inflate(inflater)
//        viewModel = ViewModelProvider(requireActivity())[ProductReviewViewModel::class.java]
//
//        productId = arguments?.getString("product_id")!!
//
//        return binding.root
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        binding.productReviewRecycleView.layoutManager = LinearLayoutManager(requireContext())
//        binding.productReviewRecycleView.addItemDecoration(
//            DividerItemDecoration(
//                binding.productReviewRecycleView.context,
//                LinearLayoutManager.VERTICAL,
//            )
//        )
//
//        viewModel.productReviewData.observe(viewLifecycleOwner) { productReviewData ->
//            binding.productReviewRecycleView.adapter = ProductReviewItemAdapter(productReviewData)
//        }
//
//        viewModel.productReview(productId, object : ProductReviewCallback {
//            override fun onFailure(err: Throwable) {
//                Toast.makeText(requireContext(), err.message, Toast.LENGTH_SHORT).show()
//            }
//        })
//    }
//
// }
