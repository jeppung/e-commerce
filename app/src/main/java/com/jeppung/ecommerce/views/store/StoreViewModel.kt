package com.jeppung.ecommerce.views.store

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.google.gson.Gson
import com.jeppung.ecommerce.repository.api_service.ApiService
import com.jeppung.ecommerce.repository.models.ProductItem
import com.jeppung.ecommerce.repository.models.QueryModel
import com.jeppung.ecommerce.repository.models.SearchResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(
    private var apiService: ApiService,
) : ViewModel() {

    private var _storeSearchUiState = MutableSharedFlow<StoreSearchUiState>()
    val storeSearchUiState: SharedFlow<StoreSearchUiState> = _storeSearchUiState

    private val _query = MutableLiveData<QueryModel>()
    val query: LiveData<QueryModel> = _query

    val flowText = MutableStateFlow("")

    @OptIn(ExperimentalCoroutinesApi::class)
    var pagingData: Flow<PagingData<ProductItem>> = _query.asFlow().flatMapLatest { query ->
        Pager(PagingConfig(pageSize = 10, initialLoadSize = 10, prefetchDistance = 1)) {
            StoreProductPagingSource(apiService, query)
        }.flow
    }.cachedIn(viewModelScope)

    init {
        viewModelScope.launch {
            flowText.debounce(1000).collectLatest {
                onQuerySearch(it)
            }
        }

        searchProduct(
            QueryModel(
                search = "",
                brand = null,
                lowest = null,
                highest = null,
                sort = null,
                page = null
            )
        )
    }

    fun searchProduct(queryModel: QueryModel) {
        _query.value = queryModel
    }

    fun onQuerySearch(query: String) {
        viewModelScope.launch {
            _storeSearchUiState.emit(StoreSearchUiState.Loading)
            try {
                val response = apiService.search(query)
                _storeSearchUiState.emit(StoreSearchUiState.Success(response))
            } catch (e: Exception) {
                if (e is HttpException) {
                    val errResponse = Gson().fromJson(
                        e.response()!!.errorBody()!!.string(),
                        SearchResponse::class.java
                    )
                    _storeSearchUiState.emit(StoreSearchUiState.Error(errResponse.message))
                } else {
                    _storeSearchUiState.emit(StoreSearchUiState.Error(e.message!!))
                }
            }
        }
    }

    fun resetAllStoreData() {
        _query.value = QueryModel(
            search = "",
            brand = null,
            lowest = null,
            highest = null,
            sort = null,
            page = null
        )
    }
}

sealed class StoreSearchUiState {
    object Loading : StoreSearchUiState()
    data class Success(val data: SearchResponse) : StoreSearchUiState()
    data class Error(val message: String) : StoreSearchUiState()
}
