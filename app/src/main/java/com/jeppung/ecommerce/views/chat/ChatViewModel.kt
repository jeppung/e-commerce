package com.jeppung.ecommerce.views.chat

import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport.Session.User
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.getValue
import com.jeppung.ecommerce.repository.models.UserMessage
import dagger.hilt.android.lifecycle.HiltViewModel
import java.sql.Timestamp
import java.util.UUID
import javax.inject.Inject

@HiltViewModel
class ChatViewModel @Inject constructor(
    private val firebaseDb: DatabaseReference
): ViewModel() {
    private val temp = mutableListOf<UserMessage>()

    private val _chats = MutableLiveData<List<UserMessage>>()
    val chats: LiveData<List<UserMessage>> = _chats

    fun sendChatToDb(name: String, message: String) {
        val data = UserMessage(name, message, Timestamp(System.currentTimeMillis()).toString())
        firebaseDb.child("group").child(UUID.randomUUID().toString()).setValue(data)
    }

    private fun getChats() {
        firebaseDb.child("group").addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                temp.clear()
                snapshot.children.map {
                    temp.add(it.getValue<UserMessage>()!!)
                }
                _chats.value = temp
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })
    }
    init {
        getChats()
    }
}
