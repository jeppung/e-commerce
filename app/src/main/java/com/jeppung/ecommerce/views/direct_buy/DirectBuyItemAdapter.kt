package com.jeppung.ecommerce.views.direct_buy

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.ItemDirectBuyBinding
import com.jeppung.ecommerce.repository.room.entity.Cart
import com.jeppung.ecommerce.utils.AppUtils

class DirectBuyItemAdapter(
    private val items: List<Cart>,
    private val callback: OnDirectBuyProductListener
) :
    RecyclerView.Adapter<DirectBuyItemAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ItemDirectBuyBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemDirectBuyBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.apply {
            directBuyProductTitle.text = items[position].productTitle
            Glide.with(holder.itemView.context).load(items[position].productImage)
                .into(directBuyProductImage)
            directBuyProductVariant.text = items[position].productVariant
            directBuyProductPrice.text = AppUtils.formatCurrencyIDR(items[position].productPrice)
            directBuyProductStock.text = holder.itemView.context.resources.getString(
                R.string.item_stock,
                items[position].productStock
            )
            directBuyProductQty.text = items[position].quantity.toString()

            directBuyAddQtyBtn.setOnClickListener {
                if (items[position].quantity < items[position].productStock) {
                    items[position].quantity = items[position].quantity + 1
                    directBuyProductQty.text = items[position].quantity.toString()
                    notifyDataSetChanged()
                    callback.onCartProductQtyChanged(items[position], items)
                }
            }

            directBuyReduceQtyBtn.setOnClickListener {
                if (items[position].quantity > 1) {
                    items[position].quantity = items[position].quantity - 1
                    directBuyProductQty.text = items[position].quantity.toString()
                    notifyDataSetChanged()
                    callback.onCartProductQtyChanged(items[position], items)
                }
            }

            directBuyToggleBtnGroup.addOnButtonCheckedListener { group, _, _ ->
                group.clearChecked()
            }
        }
    }

    override fun getItemCount(): Int = items.size
}

interface OnDirectBuyProductListener {
    fun onCartProductQtyChanged(product: Cart, productList: List<Cart>)
}
