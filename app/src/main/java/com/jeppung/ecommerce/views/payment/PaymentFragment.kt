package com.jeppung.ecommerce.views.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jeppung.ecommerce.databinding.FragmentPaymentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PaymentFragment : Fragment() {

    private lateinit var binding: FragmentPaymentBinding
    private lateinit var adapter: PaymentListAdapter
    private val viewModel: PaymentViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPaymentBinding.inflate(inflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.paymentRecycleView.layoutManager = LinearLayoutManager(requireContext())
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.paymentListUiState.collect {
                    when (it) {
                        is PaymentListUiState.Loading ->
                            binding.paymentProgressBar.visibility =
                                View.VISIBLE

                        is PaymentListUiState.Error ->
                            binding.paymentProgressBar.visibility =
                                View.GONE

                        is PaymentListUiState.Success -> {
                            binding.paymentProgressBar.visibility =
                                View.GONE
                            adapter = PaymentListAdapter(it.data) {
                                val bundle = Bundle().apply {
                                    putParcelable("paymentMethod", it)
                                }
                                parentFragmentManager.setFragmentResult("requestKey", bundle)
                                view.findNavController().navigateUp()
                            }
                            binding.paymentRecycleView.adapter = adapter
                        }
                    }
                }
            }
        }
    }
}
