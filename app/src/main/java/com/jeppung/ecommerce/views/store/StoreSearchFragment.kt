package com.jeppung.ecommerce.views.store

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentStoreSearchBinding
import com.jeppung.ecommerce.repository.models.QueryModel
import com.jeppung.ecommerce.repository.prefs.UserPreference
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class StoreSearchFragment : Fragment() {

    private lateinit var binding: FragmentStoreSearchBinding
    private lateinit var viewModel: StoreViewModel
    private lateinit var queryData: QueryModel

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    @Inject
    lateinit var userPreference: UserPreference

    @Inject
    lateinit var appDatabase: AppDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStoreSearchBinding.inflate(inflater)
        viewModel = ViewModelProvider(this)[StoreViewModel::class.java]

        queryData = arguments?.getParcelable<QueryModel>("QUERY_MODEL")!!

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.storeSearchUiState.collect {
                    when (it) {
                        is StoreSearchUiState.Loading -> binding.storeSearchProgressBar.visibility = View.VISIBLE
                        is StoreSearchUiState.Error -> {
                            binding.storeSearchProgressBar.visibility = View.GONE
                            if (it.message == "closed") {
                                AppUtils.authTimeoutHandler(userPreference, appDatabase)
                                Toast.makeText(
                                    requireContext(),
                                    requireContext().getString(R.string.text_kick_out),
                                    Toast.LENGTH_SHORT
                                ).show()
                                val nav = Navigation.findNavController(
                                    requireActivity(),
                                    R.id.nav_host_fragment_container
                                )
                                nav.navigate(R.id.action_global_pre_login)
                            } else {
                                Toast.makeText(
                                    requireContext(),
                                    it.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                        is StoreSearchUiState.Success -> {
                            binding.storeSearchProgressBar.visibility = View.GONE
                            binding.storeSearchRecycleView.adapter = StoreSearchViewAdapter(it.data.data) { itemSearchView, searchString ->
                                itemSearchView.setOnClickListener {
                                    firebaseAnalytics.logEvent("BUTTON CLICKED") {
                                        param("store_search", "search item")
                                    }

                                    val bundle = Bundle().apply {
                                        putParcelable(
                                            "QUERY_MODEL",
                                            QueryModel(
                                                search = searchString,
                                                brand = queryData.brand,
                                                sort = queryData.sort,
                                                highest = queryData.highest,
                                                lowest = queryData.lowest,
                                                page = queryData.page,
                                            )
                                        )
                                    }

                                    parentFragmentManager.setFragmentResult("searchKey", bundle)

                                    AppUtils.hideSoftKeyboard(requireContext(), itemSearchView)
                                    itemSearchView.findNavController()
                                        .navigateUp()
                                }
                            }
                        }
                    }
                }
            }
        }

        binding.storeSearchRecycleView.layoutManager = LinearLayoutManager(requireContext())

        /**
         * Immediately focus on search text when navigate to this fragment
         * and fill the search text with existing search data (if available)
         */
        binding.storeSearchInput.apply {
            requestFocus()
            setText(queryData.search)
        }

        /**
         * Search text with debounce using flow
         */
        binding.storeSearchInput.addTextChangedListener { text ->
            lifecycleScope.launch {
                viewModel.flowText.emit(text.toString())
            }
        }

        /**
         * Immediately search data if search string is not empty (coming from store fragment)
         */
        if (!queryData.search.isNullOrEmpty()) {
            lifecycleScope.launch {
                viewModel.flowText.emit(queryData.search!!)
            }
        }

        /**
         * Handling search data using keyboard search button
         */
        binding.storeSearchInput.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                firebaseAnalytics.logEvent("BUTTON CLICKED") {
                    param("store_search", "search button")
                }

                queryData.search = v.text.toString().trim()

                val bundle = Bundle().apply {
                    putParcelable("QUERY_MODEL", queryData)
                }

                parentFragmentManager.setFragmentResult("searchKey", bundle)

                AppUtils.hideSoftKeyboard(requireContext(), view)
                view.findNavController().navigateUp()
                return@setOnEditorActionListener true
            }
            false
        }

        /**
         * If user not changing the search text and press back handler
         */
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    binding.storeSearchInput.clearFocus()

                    val bundle = Bundle().apply {
                        putParcelable("QUERY_MODEL", queryData)
                    }

                    parentFragmentManager.setFragmentResult("searchKey", bundle)

                    view.findNavController().navigateUp()
                }
            }
        )
    }
}
