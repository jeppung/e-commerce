package com.jeppung.ecommerce.views.notification

import androidx.lifecycle.ViewModel
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.entity.Notification
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(
    private val appDatabase: AppDatabase
) : ViewModel() {
    fun getNotifications() = appDatabase.notificationDao().getAllNotifications()

    fun updateIsReadStatus(data: Notification) =
        appDatabase.notificationDao().updateIsReadStatus(data)

    init {
        getNotifications()
    }
}
