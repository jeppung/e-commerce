package com.jeppung.ecommerce.views.transaksi

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.jeppung.ecommerce.repository.api_service.ApiService
import com.jeppung.ecommerce.repository.models.Transaction
import com.jeppung.ecommerce.repository.models.TransactionsResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(
    private val apiService: ApiService,
) : ViewModel() {

    private val _transactionUiState = MutableSharedFlow<TransactionUiState>()
    val transactionUiState: SharedFlow<TransactionUiState> = _transactionUiState

    fun getAllTransaction() {
        viewModelScope.launch {
            _transactionUiState.emit(TransactionUiState.Loading)
            try {
                val response = apiService.getAllTransaction()
                _transactionUiState.emit(TransactionUiState.Success(response.data))
            } catch (e: Exception) {
                if (e is HttpException) {
                    val errResponse = Gson().fromJson(
                        e.response()!!.errorBody()!!.string(),
                        TransactionsResponse::class.java
                    )
                    _transactionUiState.emit(TransactionUiState.Error(errResponse.message))
                } else {
                    _transactionUiState.emit(TransactionUiState.Error(e.message.toString()))
                }
            }
        }
    }
}

sealed class TransactionUiState {
    object Loading : TransactionUiState()
    data class Success(val data: List<Transaction>) : TransactionUiState()
    data class Error(val message: String) : TransactionUiState()
}
