package com.jeppung.ecommerce.views.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.jeppung.ecommerce.repository.api_service.ApiService
import com.jeppung.ecommerce.repository.models.LoginResponse
import com.jeppung.ecommerce.repository.models.UserModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val apiService: ApiService
) : ViewModel() {

    private val _loginUiState = MutableSharedFlow<LoginUiState>()
    val loginUiState: SharedFlow<LoginUiState> = _loginUiState

    fun login(
        data: UserModel
    ) {
        viewModelScope.launch {
            _loginUiState.emit(LoginUiState.Loading)
            try {
                val response = apiService.login(data)
                _loginUiState.emit(LoginUiState.Success(response, data.firebaseToken))
            } catch (e: Exception) {
                if (e is HttpException) {
                    val errResponse = Gson().fromJson(
                        e.response()?.errorBody()?.string(),
                        LoginResponse::class.java
                    )
                    _loginUiState.emit(LoginUiState.Error(errResponse.message))
                } else {
                    _loginUiState.emit(LoginUiState.Error(e.message.toString()))
                }
            }
        }
    }
}

sealed class LoginUiState {
    data class Success(val data: LoginResponse, val firebaseToken: String) : LoginUiState()
    data class Error(val message: String) : LoginUiState()
    object Loading : LoginUiState()
}
