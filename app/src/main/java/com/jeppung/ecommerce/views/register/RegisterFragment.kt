package com.jeppung.ecommerce.views.register

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.messaging.FirebaseMessaging
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentRegisterBinding
import com.jeppung.ecommerce.repository.models.UserModel
import com.jeppung.ecommerce.repository.prefs.UserPreference
import com.jeppung.ecommerce.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class RegisterFragment : Fragment() {

    private lateinit var binding: FragmentRegisterBinding
    private val viewModel: RegisterViewModel by viewModels()

    @Inject
    lateinit var userPreference: UserPreference

    @Inject
    lateinit var firebaseMessaging: FirebaseMessaging

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    val method = "EMAIL & PASSWORD - Joshua"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         *  RegisterUiState validation
         */
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.registerUiState.collect {
                    when (it) {
                        is RegisterUiState.Loading -> {
                            binding.registerProgressBar.visibility = View.VISIBLE
                        }
                        is RegisterUiState.Success -> {
                            binding.registerProgressBar.visibility = View.GONE
                            val response = it.data

                            userPreference.apply {
                                setAccessToken(response.data!!.accessToken)
                                setRefreshToken(response.data!!.refreshToken)
                                setFirebaseToken(it.firebaseToken)
                            }

                            Toast.makeText(requireContext(), response.message, Toast.LENGTH_SHORT)
                                .show()

                            view.findNavController()
                                .navigate(
                                    R.id.action_registerFragment_to_registerProfileFragment,
                                )
                        }
                        is RegisterUiState.Error -> {
                            binding.registerProgressBar.visibility = View.GONE
                            Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
        }

        /**
         *  Login button onClick function
         */
        binding.registerLoginBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("register", "login")
            }
            view.findNavController()
                .navigate(R.id.action_registerFragment_to_loginFragment)
        }

        /**
         *  Register button onClick function
         */
        binding.registerDaftarBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("register", "register")
            }
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP) {
                param("Method", method)
            }
            firebaseMessaging.token.addOnCompleteListener { task ->
                try {
                    val token = task.result
                    viewModel.register(
                        UserModel(
                            email = binding.registerEmailInput.text.toString(),
                            password = binding.registerPasswordInput.text.toString(),
                            firebaseToken = token
                        )
                    )
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), e.message, Toast.LENGTH_SHORT).show()
                }
            }
        }

        /**
         *  Email input validation
         */
        binding.registerEmailInput.addTextChangedListener { input ->
            checkInput(input!!, EMAIL_TYPE)
        }

        /**
         *  Password input validation
         */
        binding.registerPasswordInput.addTextChangedListener { input ->
            checkInput(input!!, PASSWORD_TYPE)
        }

        /**
         * Configuring spanned text
         */
        val spannedBottomInfo = AppUtils.setSpannableBottomInfo(
            requireContext(),
            R.string.terms_and_conditions_login,
            resources.getString(R.string.terms),
            resources.getString(R.string.conditions)
        )
        binding.registerBottomInfo.text = spannedBottomInfo
    }

    private fun checkInput(input: Editable, type: String) {
        when (type) {
            EMAIL_TYPE -> {
                if (input.isNotEmpty()) {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(input).matches()) {
                        EMAIL_CHECK = false
                        binding.registerEmailInputLayout.error = resources.getString(R.string.email_error_validation)
                        binding.registerEmailInputLayout.isErrorEnabled = true
                        binding.registerEmailInputLayout.errorIconDrawable = null
                    } else {
                        EMAIL_CHECK = true
                        binding.registerEmailInputLayout.error = null
                        binding.registerEmailInputLayout.isErrorEnabled = false
                    }

                    binding.registerDaftarBtn.isEnabled = PASSWORD_CHECK && EMAIL_CHECK
                } else {
                    EMAIL_CHECK = false
                    binding.registerEmailInputLayout.error = null
                    binding.registerEmailInputLayout.isErrorEnabled = false
                    binding.registerDaftarBtn.isEnabled = false
                }
            }

            PASSWORD_TYPE -> {
                if (input.isNotEmpty()) {
                    if (input.length < 8) {
                        PASSWORD_CHECK = false
                        binding.registerPasswordInputLayout.error =
                            resources.getString(R.string.password_error_validation)
                        binding.registerPasswordInputLayout.isErrorEnabled = true
                        binding.registerPasswordInputLayout.errorIconDrawable = null
                    } else {
                        PASSWORD_CHECK = true
                        binding.registerPasswordInputLayout.error = null
                        binding.registerPasswordInputLayout.isErrorEnabled = false
                    }

                    binding.registerDaftarBtn.isEnabled = EMAIL_CHECK == true && PASSWORD_CHECK
                } else {
                    PASSWORD_CHECK = false
                    binding.registerPasswordInputLayout.error = null
                    binding.registerPasswordInputLayout.isErrorEnabled = false
                    binding.registerDaftarBtn.isEnabled = false
                }
            }
        }
    }

    companion object {
        private const val EMAIL_TYPE = "EMAIL"
        private const val PASSWORD_TYPE = "PASSWORD"
        private var EMAIL_CHECK = false
        private var PASSWORD_CHECK = false
    }
}
