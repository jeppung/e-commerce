package com.jeppung.ecommerce.views.store

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeppung.ecommerce.databinding.ItemSearchBinding

class StoreSearchViewAdapter(private val items: List<String>?, private val callback: (View, String) -> Unit) :
    RecyclerView.Adapter<StoreSearchViewAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemSearchBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (items != null) {
            callback(holder.itemView, items[position])
            holder.binding.storeSearchResult.text = items[position]
        }
    }

    override fun getItemCount(): Int = items?.size ?: 0
}
