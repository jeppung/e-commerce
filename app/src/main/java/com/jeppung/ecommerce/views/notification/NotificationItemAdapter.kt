package com.jeppung.ecommerce.views.notification

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.ItemNotificationBinding
import com.jeppung.ecommerce.repository.room.entity.Notification

class NotificationItemAdapter(
    private val onClick: (Notification) -> Unit
) :
    RecyclerView.Adapter<NotificationItemAdapter.ViewHolder>() {

    private var notificationList: List<Notification> = mutableListOf()

    inner class ViewHolder(val binding: ItemNotificationBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Notification) {
            if (item.isRead) {
                itemView.setBackgroundColor(itemView.context.getColor(R.color.transparent))
            }
            Glide.with(itemView.context).load(item.image).into(binding.notificationImage)
            binding.notificationBody.text = item.body
            binding.notificationInfo.text = item.type
            binding.notificationStatus.text = item.title
            binding.notificationDateTime.text = "${item.date}, ${item.time}"
            itemView.setOnClickListener {
                if (!item.isRead) {
                    item.isRead = true
                    onClick(item)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemNotificationBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(notificationList[position])
    }

    override fun getItemCount(): Int = notificationList.size

    fun updateNotificationList(data: List<Notification>) {
        notificationList = data.reversed()
        notifyDataSetChanged()
    }
}
