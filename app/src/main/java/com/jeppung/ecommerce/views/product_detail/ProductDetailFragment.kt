// package com.jeppung.ecommerce.views.product_detail
//
// import android.database.sqlite.SQLiteConstraintException
// import android.os.Bundle
// import android.util.Log
// import android.view.LayoutInflater
// import android.view.View
// import android.view.ViewGroup
// import androidx.fragment.app.Fragment
// import androidx.lifecycle.ViewModelProvider
// import androidx.navigation.findNavController
// import com.google.android.material.chip.Chip
// import com.google.android.material.snackbar.Snackbar
// import com.google.android.material.tabs.TabLayoutMediator
// import com.google.firebase.analytics.FirebaseAnalytics
// import com.google.firebase.analytics.ktx.logEvent
// import com.jeppung.ecommerce.R
// import com.jeppung.ecommerce.databinding.FragmentProductDetailBinding
// import com.jeppung.ecommerce.repository.models.ProductDetailData
// import com.jeppung.ecommerce.repository.room.entity.Cart
// import com.jeppung.ecommerce.repository.room.entity.Wishlist
// import com.jeppung.ecommerce.utils.AppUtils
// import com.jeppung.ecommerce.views.main.MainnViewModel
// import dagger.hilt.android.AndroidEntryPoint
// import javax.inject.Inject
//
// @AndroidEntryPoint
// class ProductDetailFragment : Fragment() {
//
//    private lateinit var binding: FragmentProductDetailBinding
//    private lateinit var viewModel: ProductDetailViewModel
//    private lateinit var mainViewModel: MainnViewModel
//    private lateinit var productId: String
//    private lateinit var productDetail: ProductDetailData
//    private val currency = "IDR"
//    private var callback: ProductDetailCallback = object : ProductDetailCallback {
//        override fun onFailure(err: Throwable) {
//            binding.productDetailScrollView.visibility = View.GONE
//            binding.productDetailBottomBtnContainer.visibility = View.GONE
//            binding.productDetailDivider4.visibility = View.GONE
//            binding.productDetailErrorView.storeErrorView.visibility = View.VISIBLE
//            binding.productDetailErrorView.storeErrorButton.text = "Refresh"
//            binding.productDetailErrorView.storeErrorButton.setOnClickListener {
//                viewModel.getProductDetail(productId, this)
//            }
//        }
//    }
//
//    @Inject
//    lateinit var firebaseAnalytics: FirebaseAnalytics
//
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View {
//        binding = FragmentProductDetailBinding.inflate(inflater)
//        viewModel = ViewModelProvider(requireActivity())[ProductDetailViewModel::class.java]
//        mainViewModel = ViewModelProvider(requireActivity())[MainnViewModel::class.java]
//
//        productId = arguments?.getString("product_id")!!
//
//        return binding.root
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        viewModel.checkProductWishlist(productId)
//        viewModel.getProductDetail(productId, callback)
//
//        viewModel.isWishlist.observe(viewLifecycleOwner) { value ->
//            binding.productDetailWishlistBtn.isChecked = value
//        }
//
//        viewModel.setLoadingProductDetail.observe(viewLifecycleOwner) { value ->
//            if (value) {
//                binding.productDetailProgressBar.visibility = View.VISIBLE
//                binding.productDetailScrollView.visibility = View.GONE
//                binding.productDetailBottomBtnContainer.visibility = View.GONE
//            } else {
//                binding.productDetailProgressBar.visibility = View.GONE
//                binding.productDetailScrollView.visibility = View.VISIBLE
//                binding.productDetailBottomBtnContainer.visibility = View.VISIBLE
//            }
//        }
//
//        viewModel.productDetail.observe(viewLifecycleOwner) { data ->
//            productDetail = data
//
//            val productDetailBundle = Bundle().apply {
//                putString(FirebaseAnalytics.Param.ITEM_ID, productDetail.productId)
//                putString(FirebaseAnalytics.Param.ITEM_NAME, productDetail.productName)
//                putString(
//                    FirebaseAnalytics.Param.ITEM_VARIANT,
//                    productDetail.productVariant[0].variantName
//                )
//                putString(FirebaseAnalytics.Param.ITEM_BRAND, productDetail.brand)
//                putInt(
//                    FirebaseAnalytics.Param.PRICE,
//                    productDetail.productPrice
//                )
//            }
//
//            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM) {
//                param(FirebaseAnalytics.Param.CURRENCY, currency)
//                param(FirebaseAnalytics.Param.VALUE, productDetail.productPrice.toString())
//                param(FirebaseAnalytics.Param.ITEMS, arrayOf(productDetailBundle))
//            }
//
//            binding.productDetailScrollView.visibility = View.VISIBLE
//            binding.productDetailBottomBtnContainer.visibility = View.VISIBLE
//            binding.productDetailDivider4.visibility = View.VISIBLE
//            binding.productDetailErrorView.storeErrorView.visibility = View.GONE
//            binding.productDetailViewPager.adapter = ProductDetailImageAdapter(productDetail.image)
//            TabLayoutMediator(
//                binding.productDetailDotsTabLayout,
//                binding.productDetailViewPager
//            ) { _, _ ->
//            }.attach()
//
//            binding.productDetailName.text = productDetail.productName
//            binding.productDetailPrice.text =
//                AppUtils.formatCurrencyIDR(productDetail.productPrice.toLong())
//            binding.productDetailSale.text =
//                resources.getString(R.string.item_sale, productDetail.sale)
//            binding.productDetailTotalRating.text =
//                "${productDetail.productRating} (${productDetail.sale})"
//
//            binding.productDetailVariantChipGroup.removeAllViews()
//            productDetail.productVariant.forEachIndexed { index, variant ->
//                val chip = Chip(requireActivity())
//                chip.text = variant.variantName
//                binding.productDetailVariantChipGroup.addView(chip)
//                chip.setOnClickListener {
//                    firebaseAnalytics.logEvent("BUTTON CLICKED") {
//                        param("product_detail", "variant chip")
//                    }
//                    val newPrice = productDetail.productPrice + variant.variantPrice
//                    binding.productDetailPrice.text = AppUtils.formatCurrencyIDR(newPrice.toLong())
//                }
//
//                if (index == 0) {
//                    chip.isChecked = true
//                }
//            }
//
//            binding.productDetailDeskripsi.text = productDetail.description
//            binding.productDetailUlasanRating1.text = productDetail.productRating.toString()
//            binding.productDetailUlasanTotalSatisfaction.text =
//                resources.getString(R.string.total_satisfaction, productDetail.totalSatisfaction)
//            binding.productDetailUlasanTotalRatingAndUlasan.text = resources.getString(
//                R.string.total_rating_ulasan,
//                productDetail.totalRating,
//                productDetail.totalReview
//            )
//        }
//
//        binding.productDetailAllUlasanBtn.setOnClickListener {
//            firebaseAnalytics.logEvent("BUTTON CLICKED") {
//                param("product_detail", "lihat ulasan")
//            }
//            val bundle = Bundle().apply {
//                putString("product_id", productId)
//            }
//
//            view.findNavController()
//                .navigate(R.id.action_productDetailFragment_to_productReviewFragment, bundle)
//        }
//
//        binding.productDetailWishlistBtn.setOnClickListener {
//            firebaseAnalytics.logEvent("BUTTON CLICKED") {
//                param("product_detail", "wishlist")
//            }
//            try {
//                if (binding.productDetailWishlistBtn.isChecked) {
//                    val selectedVariant =
//                        view.findViewById<Chip>(binding.productDetailVariantChipGroup.checkedChipId)
//                    viewModel.addProductToWishlist(
//                        Wishlist(
//                            productId = productDetail.productId,
//                            productImage = productDetail.image[0],
//                            productPrice = AppUtils.parseCurrencyIdr(binding.productDetailPrice.text.toString()),
//                            productStore = productDetail.store,
//                            productSale = productDetail.sale,
//                            productTitle = productDetail.productName,
//                            productRating = productDetail.productRating.toString().toFloat(),
//                            productStock = productDetail.stock,
//                            productVariant = selectedVariant.text.toString()
//                        )
//                    )
//
//                    val productBundle = Bundle().apply {
//                        putString(FirebaseAnalytics.Param.ITEM_ID, productDetail.productId)
//                        putString(FirebaseAnalytics.Param.ITEM_NAME, productDetail.productName)
//                        putInt(
//                            FirebaseAnalytics.Param.PRICE,
//                            AppUtils.parseCurrencyIdr(binding.productDetailPrice.text.toString())
//                                .toInt()
//                        )
//                        putString(
//                            FirebaseAnalytics.Param.ITEM_VARIANT,
//                            selectedVariant.text.toString()
//                        )
//                        putString(FirebaseAnalytics.Param.ITEM_BRAND, productDetail.brand)
//                    }
//
//                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_WISHLIST) {
//                        param(FirebaseAnalytics.Param.CURRENCY, currency)
//                        param(
//                            FirebaseAnalytics.Param.VALUE,
//                            AppUtils.parseCurrencyIdr(binding.productDetailPrice.text.toString())
//                        )
//                        param(FirebaseAnalytics.Param.ITEMS, arrayOf(productBundle))
//                    }
//
//                    binding.productDetailWishlistBtn.setButtonDrawable(R.drawable.ic_favorite)
//                    mainViewModel.getWishlistSize()
//                    Snackbar.make(view, "Product added to wishlist!", Snackbar.LENGTH_SHORT).show()
//                } else {
//                    viewModel.removeProductFromWishlist(productDetail.productId)
//                    binding.productDetailWishlistBtn.setButtonDrawable(R.drawable.ic_favorite_border)
//                    mainViewModel.getWishlistSize()
//                    Snackbar.make(view, "Product removed from wishlist!", Snackbar.LENGTH_SHORT)
//                        .show()
//                }
//            } catch (e: Exception) {
//                Snackbar.make(view, e.message.toString(), Snackbar.LENGTH_SHORT)
//                    .show()
//            }
//        }
//
//        binding.productDetailBeliLangsungBtn.setOnClickListener {
//            firebaseAnalytics.logEvent("BUTTON CLICKED") {
//                param("product_detail", "beli langsung")
//            }
//            val selectedVariant =
//                view.findViewById<Chip>(binding.productDetailVariantChipGroup.checkedChipId)
//
//            val product = Cart(
//                productId = productDetail.productId,
//                productTitle = productDetail.productName,
//                productVariant = selectedVariant.text.toString(),
//                productPrice = AppUtils.parseCurrencyIdr(binding.productDetailPrice.text.toString()),
//                productStock = productDetail.stock,
//                productImage = productDetail.image[0],
//                quantity = 1,
//                isChecked = true
//            )
//
//            val arrayList = ArrayList<Cart>()
//            arrayList.add(product)
//
//            val bundle = Bundle().apply {
//                putParcelableArrayList("products", arrayList)
//                putLong(
//                    "totalPrice",
//                    AppUtils.parseCurrencyIdr(binding.productDetailPrice.text.toString())
//                )
//                putString("source", "productDetail")
//            }
//            view.findNavController()
//                .navigate(R.id.action_productDetailFragment_to_directBuyFragment, bundle)
//        }
//
//        binding.productDetailPlusKeranjangBtn.setOnClickListener {
//            firebaseAnalytics.logEvent("BUTTON CLICKED") {
//                param("product_detail", "tambah keranjang")
//            }
//            val selectedVariant =
//                view.findViewById<Chip>(binding.productDetailVariantChipGroup.checkedChipId)
//
//            val productDetailBundle = Bundle().apply {
//                putString(FirebaseAnalytics.Param.ITEM_ID, productDetail.productId)
//                putString(FirebaseAnalytics.Param.ITEM_NAME, productDetail.productName)
//                putString(FirebaseAnalytics.Param.ITEM_VARIANT, selectedVariant.text.toString())
//                putString(FirebaseAnalytics.Param.ITEM_BRAND, productDetail.brand)
//                putInt(
//                    FirebaseAnalytics.Param.PRICE,
//                    AppUtils.parseCurrencyIdr(binding.productDetailPrice.text.toString()).toInt()
//                )
//            }
//
//            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
//                param(FirebaseAnalytics.Param.CURRENCY, currency)
//                param(FirebaseAnalytics.Param.ITEMS, arrayOf(productDetailBundle))
//            }
//
//            try {
//                viewModel.addProductToCart(
//                    Cart(
//                        productId = productDetail.productId,
//                        productTitle = productDetail.productName,
//                        productImage = productDetail.image[0],
//                        productPrice = AppUtils.parseCurrencyIdr(binding.productDetailPrice.text.toString()),
//                        productStock = productDetail.stock,
//                        productVariant = selectedVariant.text.toString(),
//                        quantity = 1,
//                        isChecked = false
//                    ),
//                    callback = {
//                        Log.d("TESTT", "Testt")
//                    }
//                )
//                Snackbar.make(view, "Product added to cart", Snackbar.LENGTH_SHORT)
//                    .show()
//            } catch (e: SQLiteConstraintException) {
//                if (e.message!!.contains("SQLITE_CONSTRAINT_PRIMARYKEY")) {
//                    val product = viewModel.getSingleCartProduct(productId)
//                    if (product.quantity < product.productStock) {
//                        viewModel.updateCartProductQuantity(productId, product.productVariant)
//                        Snackbar.make(view, "Product added to cart!", Snackbar.LENGTH_SHORT)
//                            .show()
//                    } else {
//                        Snackbar.make(
//                            view,
//                            "Product qty cart has reach limit stock!",
//                            Snackbar.LENGTH_SHORT
//                        ).show()
//                    }
//                }
//            }
//        }
//    }
// }
