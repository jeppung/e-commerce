package com.jeppung.ecommerce.views.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentHomeBinding
import com.jeppung.ecommerce.repository.prefs.UserPreference
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
@AndroidEntryPoint
class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private val viewModel: HomeViewModel by viewModels()

    @Inject
    lateinit var userPreference: UserPreference

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * Observing language value used for toogle switch validation
         */
        viewModel.locale.observe(viewLifecycleOwner) {
            binding.homeLanguageSwitch.isChecked = it == "id"
        }

        /**
         * Observing darkMode value used for toogle switch validation
         */
        viewModel.darkMode.observe(viewLifecycleOwner) {
            binding.homeUimodeSwitch.isChecked = it
        }

        /**
         * Handling logout button
         */
        binding.homeLogoutBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("home", "logout")
            }

            viewModel.clearAllUserData()
            viewModel.destroyDatabase()

            view.findNavController().navigate(R.id.action_global_pre_login)
        }

        /**
         * Setup language switch listener
         */
        binding.homeLanguageSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.setLocale("in")
            } else {
                viewModel.setLocale("en")
            }
        }

        /**
         * Setup darkMode switch listener
         */
        binding.homeUimodeSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.setUiMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                viewModel.setUiMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }

        /**
         * Button for testing Firebase Crashlytics
         */
        binding.homeTestBtn.setOnClickListener {
            throw RuntimeException("Test Crash")
        }
    }
}
