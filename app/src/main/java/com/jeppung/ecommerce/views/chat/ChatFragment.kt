package com.jeppung.ecommerce.views.chat

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.getValue
import com.jeppung.ecommerce.databinding.FragmentChatBinding
import com.jeppung.ecommerce.repository.models.UserMessage
import com.jeppung.ecommerce.repository.prefs.UserPreference
import com.jeppung.ecommerce.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ChatFragment : Fragment() {

    private lateinit var binding: FragmentChatBinding
    private val viewModel: ChatViewModel by viewModels()
    @Inject
    lateinit var userPreference: UserPreference
    @Inject
    lateinit var firebaseDb: DatabaseReference
    private val adapter by lazy { ChatAdapter(userPreference) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChatBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.chatRv.layoutManager = LinearLayoutManager(requireContext()).apply {
            stackFromEnd = true
        }
        binding.chatRv.adapter = adapter

        binding.chatSendBtn.setOnClickListener {
            if(binding.chatInput.text?.trim().isNullOrEmpty()) return@setOnClickListener
            viewModel.sendChatToDb(
                name = userPreference.getUserName()!!,
                binding.chatInput.text?.trim().toString()
            )
            binding.chatInput.setText("")
            AppUtils.hideSoftKeyboard(requireContext(), view)
            SEND_CHAT = true
        }

        viewModel.chats.observe(viewLifecycleOwner) { chats ->
            adapter.setData(chats.sortedByDescending { it.timestamp }.reversed())
        }

        firebaseDb.child("group").addChildEventListener(object: ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                if(SEND_CHAT) {
                    val newMessage = snapshot.getValue<UserMessage>()
                    if(newMessage!!.name == userPreference.getUserName()) {
                        binding.chatRv.scrollToPosition(adapter.itemCount)
                    }
                }
            }
            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildRemoved(snapshot: DataSnapshot) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onCancelled(error: DatabaseError) {}
        })
    }

    companion object {
        var SEND_CHAT = false
    }
}