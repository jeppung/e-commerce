package com.jeppung.ecommerce.views.product_review

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jeppung.ecommerce.repository.api_service.ApiService
import com.jeppung.ecommerce.repository.models.ProductReviewData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class ProductReviewViewModel @Inject constructor(
    private val apiService: ApiService,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private var _productReviewUiState = MutableLiveData<ProductReviewUiState>()
    val productReviewUiState: LiveData<ProductReviewUiState> = _productReviewUiState

    fun getSavedProductReview(): List<ProductReviewData>? {
        return savedStateHandle["productReview"]
    }

    fun getSavedProductId(): String? {
        return savedStateHandle["productId"]
    }

    fun productReview(productId: String) {
        viewModelScope.launch {
            _productReviewUiState.value = ProductReviewUiState.Loading
            try {
                val response = apiService.productReview(productId)
                savedStateHandle["productReview"] = response.data!!
                savedStateHandle["productId"] = productId
                _productReviewUiState.value = ProductReviewUiState.Success(response.data)
            } catch (e: Exception) {
                if (e is HttpException) {
                    _productReviewUiState.value = ProductReviewUiState.Error
                } else {
                    _productReviewUiState.value = ProductReviewUiState.Error
                }
            }
        }
    }
}

sealed class ProductReviewUiState {
    object Loading : ProductReviewUiState()
    object Error : ProductReviewUiState()
    data class Success(val data: List<ProductReviewData>) : ProductReviewUiState()
}
