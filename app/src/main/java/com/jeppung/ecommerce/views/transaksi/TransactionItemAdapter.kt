package com.jeppung.ecommerce.views.transaksi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.ItemTransactionBinding
import com.jeppung.ecommerce.repository.models.Transaction
import com.jeppung.ecommerce.utils.AppUtils

class TransactionItemAdapter(
    private val items: List<Transaction>,
    private val onReviewClick: (Transaction) -> Unit
) :
    RecyclerView.Adapter<TransactionItemAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ItemTransactionBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemTransactionBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.apply {
            transactionDate.text = items[position].date
            Glide.with(holder.itemView.context).load(items[position].image).into(transactionImage)
            transactionTotalPrice.text = AppUtils.formatCurrencyIDR(items[position].total.toLong())
            transactionQty.text = holder.itemView.context.getString(
                R.string.transaction_item_total,
                items[position].items.size
            )
            transactionTitle.text = items[position].name

            if (items[position].rating == null || items[position].review == null
            ) {
                transactionReviewBtn.visibility = View.VISIBLE
            } else {
                transactionReviewBtn.visibility = View.INVISIBLE
            }

            transactionReviewBtn.setOnClickListener {
                onReviewClick(items[position])
            }
        }
    }

    override fun getItemCount(): Int = items.size
}
