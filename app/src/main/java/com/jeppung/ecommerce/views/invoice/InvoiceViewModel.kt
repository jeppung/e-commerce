package com.jeppung.ecommerce.views.invoice

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.jeppung.ecommerce.repository.api_service.ApiService
import com.jeppung.ecommerce.repository.models.ReviewRatingRequest
import com.jeppung.ecommerce.repository.models.ReviewRatingResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class InvoiceViewModel @Inject constructor(
    private val apiService: ApiService
) : ViewModel() {

    private val _invoiceUiState = MutableSharedFlow<InvoiceUiState>()
    val invoiceUiState: SharedFlow<InvoiceUiState> = _invoiceUiState

    fun submitReviewRating(data: ReviewRatingRequest) {
        viewModelScope.launch {
            _invoiceUiState.emit(InvoiceUiState.Loading)
            try {
                val response = apiService.submitReviewRating(data)
                _invoiceUiState.emit(InvoiceUiState.Success(response.message))
            } catch (e: Exception) {
                if (e is HttpException) {
                    val errResponse = Gson().fromJson(
                        e.response()!!.errorBody()!!.string(),
                        ReviewRatingResponse::class.java
                    )
                    _invoiceUiState.emit(InvoiceUiState.Error(errResponse.message))
                } else {
                    _invoiceUiState.emit(InvoiceUiState.Error(e.message.toString()))
                }
            }
        }
    }
}

sealed class InvoiceUiState {
    object Loading : InvoiceUiState()
    data class Success(val message: String) : InvoiceUiState()
    data class Error(val message: String) : InvoiceUiState()
}
