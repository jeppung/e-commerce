package com.jeppung.ecommerce.views.store

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.ItemStoreGridBinding
import com.jeppung.ecommerce.databinding.ItemStoreLinearBinding
import com.jeppung.ecommerce.repository.models.ProductItem
import com.jeppung.ecommerce.utils.AppUtils

class StoreViewPagingAdapter(
    private val isGrid: Boolean,
    private val onProductClick: (ProductItem) -> Unit
) :
    PagingDataAdapter<ProductItem, StoreViewPagingAdapter.ViewHolder>(ProductComparator) {

    inner class ViewHolder(val binding: ViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ProductItem) {
            if (isGrid) {
                val gridBinding = binding as ItemStoreGridBinding
                gridBinding.apply {
                    Glide.with(itemView.context).load(item.image).into(storeItemProductImage)
                    storeItemProductName.text = item.productName
                    storeItemProductPrice.text =
                        AppUtils.formatCurrencyIDR(item.productPrice.toLong())
                    storeItemProductStore.text = item.store
                    storeItemProductRatingTerjual.text = itemView.context.resources.getString(
                        R.string.rating_format,
                        item.productRating.toString(),
                        item.sale.toString()
                    )
                    itemView.setOnClickListener {
                        onProductClick(item)
                    }
                }
            } else {
                val linearBinding = binding as ItemStoreLinearBinding
                linearBinding.apply {
                    Glide.with(itemView.context).load(item.image).into(storeItemProductImage)
                    storeItemProductName.text = item.productName
                    storeItemProductPrice.text =
                        AppUtils.formatCurrencyIDR(item.productPrice.toLong())
                    storeItemProductStore.text = item.store
                    storeItemProductRatingTerjual.text = itemView.context.resources.getString(
                        R.string.rating_format,
                        item.productRating.toString(),
                        item.sale.toString()
                    )
                    itemView.setOnClickListener {
                        onProductClick(item)
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isGrid) {
            if (position == itemCount) {
                2
            } else {
                1
            }
        } else {
            0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = if (isGrid) {
            ItemStoreGridBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        } else {
            ItemStoreLinearBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        }

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)

        holder.bind(item!!)
    }

    object ProductComparator : DiffUtil.ItemCallback<ProductItem>() {
        override fun areItemsTheSame(oldItem: ProductItem, newItem: ProductItem): Boolean {
            // Id is unique.
            return oldItem.productId == newItem.productId
        }

        override fun areContentsTheSame(oldItem: ProductItem, newItem: ProductItem): Boolean {
            return oldItem == newItem
        }
    }
}
