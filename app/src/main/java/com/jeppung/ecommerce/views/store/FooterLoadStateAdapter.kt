package com.jeppung.ecommerce.views.store

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jeppung.ecommerce.databinding.ItemPagingLoadingBinding

class FooterLoadStateAdapter(private val context: Context) :
    LoadStateAdapter<FooterLoadStateAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ItemPagingLoadingBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): ViewHolder {
        val binding =
            ItemPagingLoadingBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        loadState: LoadState
    ) {
        holder.binding.loadStateProgressBar.isVisible = loadState is LoadState.Loading
    }
}
