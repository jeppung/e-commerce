package com.jeppung.ecommerce.views.cart

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.ItemCartBinding
import com.jeppung.ecommerce.repository.room.entity.Cart
import com.jeppung.ecommerce.utils.AppUtils

class CartItemAdapter(
    private val callback: OnCartProductListener
) : RecyclerView.Adapter<CartItemAdapter.ViewHolder>() {

    private var dataList: List<Cart> = emptyList()

    inner class ViewHolder(val binding: ItemCartBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemCartBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.apply {
            Glide.with(holder.itemView.context).load(dataList[position].productImage)
                .into(itemCartImage)
            itemCartTitle.text = dataList[position].productTitle
            itemCartPrice.text = AppUtils.formatCurrencyIDR(dataList[position].productPrice)
            itemCartVariant.text = dataList[position].productVariant
            itemCartStock.text =
                holder.itemView.context.resources.getString(
                    R.string.item_stock,
                    dataList[position].productStock
                )
            if (dataList[position].productStock < 10) {
                itemCartStock.setTextColor(holder.itemView.context.getColor(R.color.red))
            }
            itemCartQuantity.text = dataList[position].quantity.toString()
            itemCartCheckbox.isChecked = dataList[position].isChecked!!

            itemCartCheckbox.setOnClickListener {
                dataList[position].isChecked = itemCartCheckbox.isChecked
                callback.onCartProductChecked(dataList[position], dataList)
            }

            itemCartRemoveBtn.setOnClickListener {
                callback.onRemoveCartProductClicked(dataList[position])
            }

            itemCartReduceQtyBtn.setOnClickListener {
                if (dataList[position].quantity > 1) {
                    dataList[position].quantity = dataList[position].quantity - 1
                    itemCartQuantity.text = dataList[position].quantity.toString()
                    callback.onCartProductQtyChanged(dataList[position])
                }
            }

            itemCartAddQtyBtn.setOnClickListener {
                if (dataList[position].quantity < dataList[position].productStock) {
                    dataList[position].quantity = dataList[position].quantity + 1
                    itemCartQuantity.text = dataList[position].quantity.toString()
                    callback.onCartProductQtyChanged(dataList[position])
                }
            }

            buttonToggleGroup.addOnButtonCheckedListener { group, _, _ ->
                group.clearChecked()
            }
        }
    }

    override fun getItemCount(): Int = dataList.size

    fun updateData(data: List<Cart>) {
        dataList = data
        notifyDataSetChanged()

        val filteredData = dataList.filter { it.isChecked == true }
        val totalPriceFromChecked = filteredData.sumOf { it.productPrice * it.quantity }
        callback.onTotalPriceChanged(totalPriceFromChecked)
    }
}

interface OnCartProductListener {
    fun onCartProductChecked(product: Cart, productList: List<Cart>)
    fun onRemoveCartProductClicked(product: Cart)
    fun onCartProductQtyChanged(product: Cart)
    fun onTotalPriceChanged(totalPrice: Long)
}
