package com.jeppung.ecommerce.views.wishlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.ItemWishlistGridBinding
import com.jeppung.ecommerce.databinding.ItemWishlistLinearBinding
import com.jeppung.ecommerce.repository.room.entity.Wishlist
import com.jeppung.ecommerce.utils.AppUtils

class WishlistItemAdapter(
    private val items: List<Wishlist>,
    private val isGrid: Boolean,
    private val callback: OnWishlistClickListener
) :
    RecyclerView.Adapter<WishlistItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = if (isGrid) {
            ItemWishlistGridBinding.inflate(inflater, parent, false)
        } else {
            ItemWishlistLinearBinding.inflate(inflater, parent, false)
        }
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(private val binding: ViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Wishlist) {
            if (isGrid) {
                val gridBinding = binding as ItemWishlistGridBinding
                gridBinding.wishlistItemProductName.text = item.productTitle
                gridBinding.wishlistItemProductPrice.text =
                    AppUtils.formatCurrencyIDR(item.productPrice)
                gridBinding.wishlistItemProductStore.text = item.productStore
                gridBinding.wishlistItemProductRatingTerjual.text =
                    itemView.context.resources.getString(
                        R.string.rating_format,
                        item.productRating.toString(),
                        item.productSale.toString()
                    )
                Glide.with(itemView.context).load(item.productImage)
                    .into(gridBinding.wishlistItemProductImage)

                itemView.setOnClickListener {
                    callback.onWishlistClick(item.productId)
                }
                gridBinding.wishlistItemRemoveBtn.setOnClickListener {
                    callback.onRemoveClick(item.productId)
                }
                gridBinding.wishlistItemAddToCartBtn.setOnClickListener {
                    callback.onAddToCartClicked(item)
                }
            } else {
                val linearBinding = binding as ItemWishlistLinearBinding
                linearBinding.wishlistItemProductName.text = item.productTitle
                linearBinding.wishlistItemProductPrice.text =
                    AppUtils.formatCurrencyIDR(item.productPrice)
                linearBinding.wishlistItemProductStore.text = item.productStore
                linearBinding.wishlistItemProductRatingTerjual.text =
                    itemView.context.resources.getString(
                        R.string.rating_format,
                        item.productRating.toString(),
                        item.productSale.toString()
                    )
                Glide.with(itemView.context).load(item.productImage)
                    .into(linearBinding.wishlistItemProductImage)
                itemView.setOnClickListener {
                    callback.onWishlistClick(item.productId)
                }
                linearBinding.wishlistItemRemoveBtn.setOnClickListener {
                    callback.onRemoveClick(item.productId)
                }
                linearBinding.wishlistItemAddToCartBtn.setOnClickListener {
                    callback.onAddToCartClicked(item)
                }
            }
        }
    }
}

interface OnWishlistClickListener {
    fun onWishlistClick(productId: String)
    fun onRemoveClick(productId: String)
    fun onAddToCartClicked(product: Wishlist)
}
