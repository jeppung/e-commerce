package com.jeppung.ecommerce.views.register_profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.jeppung.ecommerce.repository.api_service.ApiService
import com.jeppung.ecommerce.repository.models.LoginResponse
import com.jeppung.ecommerce.repository.models.RegisterProfileResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class RegisterProfileViewModel @Inject constructor(
    private val apiService: ApiService
) : ViewModel() {

    private val _registerProfileUiState =
        MutableSharedFlow<RegisterProfileUiState>()
    val registerProfileUiState: SharedFlow<RegisterProfileUiState> = _registerProfileUiState

    fun registerProfile(
        username: RequestBody,
        image: MultipartBody.Part?,
    ) {
        viewModelScope.launch {
            _registerProfileUiState.emit(RegisterProfileUiState.Loading)
            try {
                val response = apiService.registerProfile(
                    userName = username,
                    userImage = image
                )
                _registerProfileUiState.emit(RegisterProfileUiState.Success(response))
            } catch (e: Exception) {
                if (e is HttpException) {
                    val errResponse = Gson().fromJson(
                        e.response()?.errorBody()?.string(),
                        LoginResponse::class.java
                    )
                    _registerProfileUiState.emit(RegisterProfileUiState.Error(errResponse.message))
                } else {
                    _registerProfileUiState.emit(RegisterProfileUiState.Error(e.message.toString()))
                }
            }
        }
    }
}

sealed class RegisterProfileUiState() {
    object Loading : RegisterProfileUiState()
    data class Success(val data: RegisterProfileResponse) : RegisterProfileUiState()
    data class Error(val message: String) : RegisterProfileUiState()
}
