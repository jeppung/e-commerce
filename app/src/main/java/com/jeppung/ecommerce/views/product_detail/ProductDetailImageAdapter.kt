// package com.jeppung.ecommerce.views.product_detail
//
// import android.view.LayoutInflater
// import android.view.ViewGroup
// import androidx.recyclerview.widget.RecyclerView
// import com.bumptech.glide.Glide
// import com.jeppung.ecommerce.R
// import com.jeppung.ecommerce.databinding.ProductDetailItemImageBinding
//
// class ProductDetailImageAdapter(private val items: List<String>) :
//    RecyclerView.Adapter<ProductDetailImageAdapter.ViewHolder>() {
//
//    inner class ViewHolder(val binding: ProductDetailItemImageBinding) :
//        RecyclerView.ViewHolder(binding.root)
//
//    override fun onCreateViewHolder(
//        parent: ViewGroup,
//        viewType: Int
//    ): ProductDetailImageAdapter.ViewHolder {
//        val binding = ProductDetailItemImageBinding.inflate(
//            LayoutInflater.from(parent.context),
//            parent,
//            false
//        )
//
//        return ViewHolder(binding)
//    }
//
//    override fun onBindViewHolder(holder: ProductDetailImageAdapter.ViewHolder, position: Int) {
//        Glide.with(holder.itemView.context).load(items[position])
//            .into(holder.binding.produtDetailImage)
//    }
//
//    override fun getItemCount(): Int = items.size
//
// }
