package com.jeppung.ecommerce.views.invoice

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentInvoiceBinding
import com.jeppung.ecommerce.repository.models.InvoiceData
import com.jeppung.ecommerce.repository.models.ReviewRatingRequest
import com.jeppung.ecommerce.repository.prefs.UserPreference
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class InvoiceFragment : Fragment() {

    private lateinit var binding: FragmentInvoiceBinding
    private lateinit var invoiceData: InvoiceData

    private val viewModel: InvoiceViewModel by viewModels()

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    @Inject
    lateinit var userPreference: UserPreference

    @Inject
    lateinit var appDatabase: AppDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentInvoiceBinding.inflate(inflater)
        invoiceData = arguments?.getParcelable("invoiceData")!!

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * Binding invoice data
         */
        binding.invoiceIdTransaksi.text = invoiceData.invoiceId
        binding.invoiceStatus.text = if (invoiceData.status) getString(R.string.success_text) else "Null"
        binding.invoiceTanggal.text = invoiceData.date
        binding.invoiceWaktu.text = invoiceData.time
        binding.invoicePaymentMethod.text = invoiceData.payment
        binding.invoiceTotalPrice.text = AppUtils.formatCurrencyIDR(invoiceData.total.toLong())

        /**
         * InvoiceFragment UI State validation
         */
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.invoiceUiState.collect {
                    when (it) {
                        is InvoiceUiState.Loading -> binding.invoiceProgressBar.visibility = View.VISIBLE
                        is InvoiceUiState.Error -> {
                            if (it.message == "closed") {
                                AppUtils.authTimeoutHandler(userPreference, appDatabase)
                                view!!.findNavController().navigate(R.id.action_global_pre_login)
                            } else {
                                Snackbar.make(view!!, it.message, Snackbar.LENGTH_SHORT).show()
                            }
                        }
                        is InvoiceUiState.Success -> {
                            Snackbar.make(view!!, it.message, Snackbar.LENGTH_SHORT).show()
                            view!!.findNavController().navigateUp()
                        }
                    }
                }
            }
        }

        /**
         * Submit review btn handler
         */
        binding.invoiceSelesaiBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("invoice", "submit review")
            }

            viewModel.submitReviewRating(
                ReviewRatingRequest(
                    invoiceId = invoiceData.invoiceId,
                    rating = if (binding.invoiceRatingBar.rating.toInt() == 0) null else binding.invoiceRatingBar.rating.toInt(),
                    review = if (binding.invoiceUlasan.text.isNullOrEmpty()) null else binding.invoiceUlasan.text.toString(),
                ),
            )
        }
    }
}
