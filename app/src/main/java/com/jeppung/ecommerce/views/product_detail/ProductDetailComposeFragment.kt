@file:OptIn(ExperimentalFoundationApi::class)

package com.jeppung.ecommerce.views.product_detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material.icons.outlined.Share
import androidx.compose.material.icons.outlined.Star
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconToggleButton
import androidx.compose.material3.InputChip
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.Typography
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.repository.models.ProductDetailData
import com.jeppung.ecommerce.repository.room.entity.Cart
import com.jeppung.ecommerce.repository.room.entity.Wishlist
import com.jeppung.ecommerce.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ProductDetailComposeFragment : Fragment() {

    private lateinit var productId: String
    private var productDetail: ProductDetailData? = null
    private lateinit var viewModel: ProductDetailViewModel
    private val args by navArgs<ProductDetailComposeFragmentArgs>()

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        productId = arguments?.getString("product_id") ?: args.id
        viewModel = ViewModelProvider(requireActivity())[ProductDetailViewModel::class.java]

        return ComposeView(requireContext()).apply {
            setContent {
                val wishlistState by viewModel.isWishlist.observeAsState()
                val uiState by viewModel.productDetailUiState.observeAsState()

                LaunchedEffect(key1 = this) {
                    if (viewModel.getSavedProductDetail()?.productId == productId) {
                        productDetail = viewModel.getSavedProductDetail()!!
                    } else {
                        viewModel.checkProductWishlist(productId)
                        viewModel.getProductDetail(productId)
                    }
                }

                ProductDetailComposeTheme {
                    Surface(
                        color = MaterialTheme.colorScheme.background
                    ) {
                        uiState?.let {
                            when (it) {
                                is ProductDetailUiState.Loading -> {
                                    Box(
                                        contentAlignment = Alignment.Center
                                    ) {
                                        CircularProgressIndicator()
                                    }
                                }

                                is ProductDetailUiState.Error -> {
                                    ProductDetailErrorScreen(
                                        viewModel = viewModel,
                                        productId = productId
                                    )
                                }

                                is ProductDetailUiState.Success -> {
                                    ProductDetailScreen(
                                        data = productDetail ?: (uiState as ProductDetailUiState.Success).product,
                                        isWishlist = wishlistState!!,
                                        view = requireView(),
                                        viewModel = viewModel,
                                        requireContext(),
                                        firebaseAnalytics
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun BottomBar(
    view: View,
    product: Cart,
    viewModel: ProductDetailViewModel,
    snackbarHostState: SnackbarHostState,
    firebaseAnalytics: FirebaseAnalytics
) {
    val scope = rememberCoroutineScope()

    Column {
        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .width(1.dp),
            color = Color(0xFFCAC4D0)
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 8.dp),
            horizontalArrangement = Arrangement.SpaceAround
        ) {
            OutlinedButton(
                onClick = {
                    firebaseAnalytics.logEvent("BUTTON CLICKED") {
                        param("productDetail", "directBuy btn")
                    }
                    val arrayList = ArrayList<Cart>()
                    arrayList.add(product)

                    val bundle = Bundle().apply {
                        putParcelableArrayList("products", arrayList)
                        putLong(
                            "totalPrice",
                            product.productPrice,
                        )
                        putString("source", "productDetail")
                    }

                    view.findNavController().navigate(
                        R.id.action_productDetailComposeFragment_to_directBuyFragment,
                        bundle
                    )
                },
                modifier = Modifier.weight(1f)
            ) {
                Text(
                    stringResource(id = R.string.buy_now),
                )
            }
            Spacer(modifier = Modifier.width(16.dp))
            Button(onClick = {
                firebaseAnalytics.logEvent("BUTTON CLICKED") {
                    param("productDetail", "add to cart btn")
                }
                viewModel.addProductToCart(product) {
                    scope.launch {
                        snackbarHostState.showSnackbar(it, duration = SnackbarDuration.Short)
                    }
                }
            }, modifier = Modifier.weight(1f)) {
                Text(
                    stringResource(id = R.string.add_to_cart),
                )
            }
        }
    }
}

@OptIn(
    ExperimentalFoundationApi::class,
    ExperimentalGlideComposeApi::class,
    ExperimentalMaterial3Api::class
)
@Composable
fun ProductDetailScreen(
    data: ProductDetailData,
    isWishlist: Boolean,
    view: View,
    viewModel: ProductDetailViewModel,
    context: Context,
    firebaseAnalytics: FirebaseAnalytics
) {
    val selectedChip = remember { mutableStateOf(0) }
    val pagerState = rememberPagerState()
    val snackbarHostState = remember { SnackbarHostState() }
    val wishlistState = remember { mutableStateOf(isWishlist) }
    val scope = rememberCoroutineScope()
    val productCartData = remember {
        mutableStateOf(
            Cart(
                productId = data.productId,
                productTitle = data.productName,
                productVariant = data.productVariant[0].variantName,
                productPrice = data.productPrice.toLong() + data.productVariant[0].variantPrice.toLong(),
                productStock = data.stock,
                productImage = data.image[0],
                quantity = 1,
                isChecked = true
            )
        )
    }

    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
        bottomBar = { BottomBar(view, productCartData.value, viewModel, snackbarHostState, firebaseAnalytics) }
    ) { paddingValues ->
        Column(
            modifier = Modifier
                .padding(paddingValues)
                .verticalScroll(rememberScrollState()),
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(309.dp)
            ) {
                HorizontalPager(pageCount = data.image.size, state = pagerState) { index ->
                    GlideImage(
                        model = "",
                        contentDescription = null,
                        modifier = Modifier.fillMaxHeight()
                    ) {
                        it.load(data.image[index]).fitCenter()
                    }
                }
                if (data.image.size > 1) {
                    Row(modifier = Modifier.align(Alignment.BottomCenter)) {
                        repeat(data.image.size) { index ->
                            val color =
                                if (pagerState.currentPage == index) {
                                    Color(0xFF6750A4)
                                } else {
                                    Color(
                                        0xFFCAC4D0
                                    )
                                }
                            Box(
                                modifier = Modifier
                                    .clip(CircleShape)
                                    .background(color)
                                    .size(8.dp)
                            )
                            if (data.image.size != index) {
                                Spacer(modifier = Modifier.width(16.dp))
                            }
                        }
                    }
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Spacer(modifier = Modifier.width(16.dp))
                Text(
                    text = AppUtils.formatCurrencyIDR(
                        data.productPrice.toLong() + data.productVariant[selectedChip.value].variantPrice.toLong()
                    ),
                    style = MaterialTheme.typography.titleLarge,
                    modifier = Modifier.weight(1f)
                )
                IconButton(onClick = {
                    firebaseAnalytics.logEvent("BUTTON CLICKED") {
                        param("productDetail", "share btn")
                    }
                    val shareIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(
                            Intent.EXTRA_TEXT,
                            """
                                Product: ${data.productName}
                                Price: ${AppUtils.formatCurrencyIDR(data.productPrice.toLong())}
                                Link: http://com.jeppung.ecommerce/product/${data.productId}
                            """.trimIndent()
                        )
                        type = "text/plain"
                    }
                    startActivity(context, Intent.createChooser(shareIntent, null), null)
                }) {
                    Icon(imageVector = Icons.Outlined.Share, contentDescription = null)
                }
                IconToggleButton(checked = wishlistState.value, onCheckedChange = {
                    firebaseAnalytics.logEvent("BUTTON CLICKED") {
                        param("productDetail", "wishlist btn")
                    }
                    if (it) {
                        viewModel.addProductToWishlist(
                            Wishlist(
                                productId = data.productId,
                                productImage = data.image[0],
                                productPrice = data.productPrice.toLong() + data.productVariant[selectedChip.value].variantPrice.toLong(),
                                productStore = data.store,
                                productSale = data.sale,
                                productTitle = data.productName,
                                productRating = data.productRating.toString().toFloat(),
                                productStock = data.stock,
                                productVariant = data.productVariant[selectedChip.value].variantName
                            )
                        )
                        wishlistState.value = it
                        scope.launch {
                            snackbarHostState.showSnackbar(
                                "Product added to wishlist!",
                                duration = SnackbarDuration.Short
                            )
                        }
                    } else {
                        viewModel.removeProductFromWishlist(data.productId)
                        wishlistState.value = it
                        scope.launch {
                            snackbarHostState.showSnackbar(
                                "Product removed from wishlist!",
                                duration = SnackbarDuration.Short
                            )
                        }
                    }
                }) {
                    Icon(
                        imageVector = if (wishlistState.value) Icons.Outlined.Favorite else Icons.Outlined.FavoriteBorder,
                        contentDescription = null
                    )
                }
            }
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                maxLines = 2,
                style = MaterialTheme.typography.bodyMedium,
                text = data.productName
            )
            Spacer(modifier = Modifier.height(8.dp))
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    stringResource(id = R.string.item_sale, data.sale),
                    style = MaterialTheme.typography.bodySmall
                )
                Spacer(modifier = Modifier.width(8.dp))
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .border(
                            border = BorderStroke(1.dp, MaterialTheme.colorScheme.outline),
                            shape = RoundedCornerShape(4.dp)
                        )
                        .padding(vertical = 2.dp)
                ) {
                    Spacer(modifier = Modifier.width(5.dp))
                    Icon(
                        modifier = Modifier
                            .width(15.dp)
                            .height(15.dp),
                        imageVector = Icons.Outlined.Star,
                        contentDescription = null
                    )
                    Spacer(modifier = Modifier.width(4.dp))
                    Text(
                        "${data.productRating} (${data.totalRating})",
                        style = MaterialTheme.typography.bodySmall
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                }
            }
            Spacer(modifier = Modifier.height(12.dp))
            Divider(
                modifier = Modifier
                    .fillMaxWidth()
                    .width(1.dp),
                color = Color(0xFFCAC4D0)
            )
            ProductDetailSection(
                title = stringResource(id = R.string.choose_variant),
                divider = true
            ) {
                LazyRow(
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    itemsIndexed(data.productVariant) { index, item ->
                        InputChip(
                            selected = selectedChip.value == index,
                            onClick = {
                                selectedChip.value = index
                                productCartData.value.apply {
                                    productVariant = item.variantName
                                    productPrice =
                                        data.productPrice.toLong() + item.variantPrice.toLong()
                                }
                            },
                            label = { Text(item.variantName) }
                        )
                    }
                }
            }
            ProductDetailSection(
                title = stringResource(id = R.string.product_description),
                divider = true
            ) {
                Text(data.description, style = MaterialTheme.typography.bodyMedium)
            }
            ProductDetailSection(
                title = stringResource(id = R.string.buyer_review),
                divider = false,
                topRightBtn = {
                    Text(
                        stringResource(id = R.string.see_all),
                        style = MaterialTheme.typography.labelLarge.copy(
                            fontSize = 12.sp
                        ),
                        color = Color(0xFF6750A4),
                        modifier = Modifier.clickable {
                            val bundle = Bundle().apply {
                                putString("product_id", data.productId)
                            }
                            view.findNavController()
                                .navigate(
                                    R.id.action_productDetailComposeFragment_to_productReviewComposeFragment,
                                    bundle
                                )
                        }
                    )
                }
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Icon(
                            imageVector = Icons.Outlined.Star,
                            contentDescription = null,
                            modifier = Modifier
                                .width(24.dp)
                                .height(24.dp)
                        )
                        Spacer(modifier = Modifier.width(4.dp))
                        Text(
                            text = "${data.productRating}",
                            style = MaterialTheme.typography.titleLarge,
                            modifier = Modifier.alignByBaseline()
                        )
                        Text(
                            text = "/5.0",
                            style = MaterialTheme.typography.bodySmall.copy(fontSize = 10.sp),
                            modifier = Modifier.alignByBaseline()
                        )
                    }
                    Spacer(modifier = Modifier.width(32.dp))
                    Column {
                        Text(
                            stringResource(
                                id = R.string.total_satisfaction,
                                data.totalSatisfaction
                            ),
                            style = MaterialTheme.typography.bodySmall.copy(
                                fontWeight = FontWeight.SemiBold
                            )
                        )
                        Text(
                            stringResource(
                                id = R.string.total_rating_ulasan,
                                data.totalRating,
                                data.totalSatisfaction
                            ),
                            style = MaterialTheme.typography.bodySmall
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun ProductDetailErrorScreen(viewModel: ProductDetailViewModel, productId: String) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_error),
            contentDescription = null,
            modifier = Modifier.size(128.dp)
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(text = stringResource(id = R.string.text_empty_title), style = MaterialTheme.typography.headlineMedium)
        Spacer(modifier = Modifier.height(4.dp))
        Text(
            text = stringResource(id = R.string.text_empty_description),
            style = MaterialTheme.typography.bodyLarge
        )
        Spacer(modifier = Modifier.height(8.dp))
        Button(onClick = {
            viewModel.getProductDetail(productId)
        }) {
            Text(stringResource(id = R.string.button_refresh))
        }
    }
}

@Composable
fun ProductDetailSection(
    title: String,
    divider: Boolean,
    topRightBtn: @Composable () -> Unit = {},
    content: @Composable () -> Unit,
) {
    Column(
        modifier = Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
    ) {
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Text(
                title,
                modifier = Modifier.weight(1f),
                style = MaterialTheme.typography.titleMedium,
            )
            topRightBtn()
        }
        Spacer(modifier = Modifier.height(8.dp))
        content()
    }
    if (divider) {
        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .width(1.dp),
            color = Color(0xFFCAC4D0)
        )
    }
}

@Composable
fun ProductDetailComposeTheme(content: @Composable () -> Unit) {
    val fonts = FontFamily(
        Font(R.font.poppins_regular_400),
        Font(R.font.poppins_medium_500, weight = FontWeight.Medium),
        Font(R.font.poppins_semibold_600, weight = FontWeight.SemiBold),
        Font(R.font.poppins_bold_700, weight = FontWeight.Bold)
    )

    val typography = Typography(
        labelLarge = TextStyle(
            fontFamily = fonts,
            fontSize = 14.sp,
            fontWeight = FontWeight.Medium,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false,
            )
        ),
        bodySmall = TextStyle(
            fontFamily = fonts,
            fontSize = 12.sp,
            color = Color(0xFF49454F),
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        bodyMedium = TextStyle(
            fontFamily = fonts,
            fontSize = 14.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        bodyLarge = TextStyle(
            fontFamily = fonts,
            fontSize = 16.sp,
        ),
        titleMedium = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.Medium,
            color = Color(0xFF49454F),
            fontSize = 16.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        titleLarge = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.SemiBold,
            fontSize = 20.sp,
            color = Color(0xFF49454F),
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        headlineMedium = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.Medium,
            fontSize = 32.sp
        )
    )

    val darkTypography = Typography(
        labelLarge = TextStyle(
            fontFamily = fonts,
            fontSize = 14.sp,
            fontWeight = FontWeight.Medium,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false,
            )
        ),
        bodySmall = TextStyle(
            fontFamily = fonts,
            fontSize = 12.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        bodyMedium = TextStyle(
            fontFamily = fonts,
            fontSize = 14.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        bodyLarge = TextStyle(
            fontFamily = fonts,
            fontSize = 16.sp,
        ),
        titleMedium = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.Medium,
            fontSize = 16.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        titleLarge = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.SemiBold,
            fontSize = 20.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        ),
        headlineMedium = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.Medium,
            fontSize = 32.sp
        )
    )

    val darkColorScheme = darkColorScheme(
        background = colorResource(id = R.color.blackSurfaceContainerLowest),
    )

    val lightColorScheme = lightColorScheme(
        background = colorResource(id = R.color.white),
    )

    MaterialTheme(
        typography = if (isSystemInDarkTheme()) darkTypography else typography,
        content = content,
        colorScheme = if (isSystemInDarkTheme()) darkColorScheme else lightColorScheme
    )
}
