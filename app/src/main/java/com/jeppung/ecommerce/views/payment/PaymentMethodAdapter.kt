package com.jeppung.ecommerce.views.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jeppung.ecommerce.databinding.ItemPaymentMethodBinding
import com.jeppung.ecommerce.repository.models.PaymentMethod

class PaymentMethodAdapter(
    private val items: List<PaymentMethod>,
    private val onClick: (PaymentMethod) -> Unit
) :
    RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ItemPaymentMethodBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: PaymentMethod) {
            Glide.with(itemView.context).load(item.image).into(binding.paymentMethodImage)
            binding.paymentMethodTitle.text = item.label

            if (!item.status) {
                binding.paymentMethodContainer.isClickable = false
                binding.paymentMethodContainer.alpha = 0.3f
            } else {
                binding.paymentMethodContainer.setOnClickListener {
                    onClick(item)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemPaymentMethodBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size
}
