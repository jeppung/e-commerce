package com.jeppung.ecommerce.views.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentOnboardingBinding
import com.jeppung.ecommerce.repository.prefs.UserPreference
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class OnboardingFragment : Fragment() {

    private lateinit var binding: FragmentOnboardingBinding

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics
    private val viewPagerOnChangeCallback: ViewPager2.OnPageChangeCallback = object :
        ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            TAB_INDICATOR = position

            if (TAB_INDICATOR == 2) {
                binding.onboardingSelanjutnyaBtn.visibility = View.INVISIBLE
            } else {
                binding.onboardingSelanjutnyaBtn.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOnboardingBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * Setup onboarding ViewPager adapter
         */
        binding.onboardingFragmentContainer.adapter = OnboardingViewAdapter(onboardingImageList)

        /**
         * Connecting ViewPager with Indicator dots
         */
        TabLayoutMediator(
            binding.onboardingDotsTabLayout,
            binding.onboardingFragmentContainer
        ) { _, _ ->
        }.attach()

        /**
         * Register callback for "Selanjutnya" btn visibility validation
         */
        binding.onboardingFragmentContainer.registerOnPageChangeCallback(viewPagerOnChangeCallback)

        /**
         * Setting up "Selanjutnya" btn event listener
         */
        binding.onboardingSelanjutnyaBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("onboarding", "Selanjutnya")
            }
            binding.onboardingFragmentContainer.currentItem++
        }

        /**
         * Setting up "Gabung Sekarang" btn event listener
         */
        binding.onboardingGabungBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("onboarding", "Gabung")
            }
            UserPreference(requireContext()).setIsOnboard(true)
            view.findNavController()
                .navigate(R.id.action_onboardingFragment_to_registerFragment)
        }

        /**
         * Setting up "Lewati" btn event listener
         */
        binding.onboardingLewatiBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("onboarding", "Lewati")
            }
            UserPreference(requireContext()).setIsOnboard(true)
            view.findNavController()
                .navigate(R.id.action_onboardingFragment_to_loginFragment)
        }
    }

    companion object {
        private var TAB_INDICATOR: Int = 0
        private var onboardingImageList = arrayListOf(
            R.drawable.onboard1,
            R.drawable.onboard2,
            R.drawable.onboard3
        )
    }
}
