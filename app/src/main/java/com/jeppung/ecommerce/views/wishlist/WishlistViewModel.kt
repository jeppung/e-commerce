package com.jeppung.ecommerce.views.wishlist

import androidx.lifecycle.ViewModel
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.entity.Cart
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(
    private val appDatabase: AppDatabase
) : ViewModel() {

    fun getAllProductWishlist() = appDatabase.wishlistDao().getAllWishlistProduct()

    fun addProductToCart(product: Cart) {
        appDatabase.cartDao().insertProductToCart(product)
    }

    fun updateCartProductQuantity(productId: String, productVariant: String) {
        appDatabase.cartDao().updateCartProductQuantity(1, productId, productVariant)
    }

    fun getSingleCartProduct(productId: String): Cart {
        return appDatabase.cartDao().getSingleCartProduct(productId)
    }

    init {
        getAllProductWishlist()
    }

    fun removeProductFromWishlist(productId: String) {
        appDatabase.wishlistDao().removeProductFromWishlist(productId)
    }
}
