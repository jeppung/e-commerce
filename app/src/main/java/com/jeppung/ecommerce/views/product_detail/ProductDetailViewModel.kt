package com.jeppung.ecommerce.views.product_detail

import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jeppung.ecommerce.repository.api_service.ApiService
import com.jeppung.ecommerce.repository.models.ProductDetailData
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.entity.Cart
import com.jeppung.ecommerce.repository.room.entity.Wishlist
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(
    private var apiService: ApiService,
    private val appDatabase: AppDatabase,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val _isWishlist = MutableLiveData<Boolean>()
    val isWishlist: LiveData<Boolean> = _isWishlist

    private val _productDetailUiState = MutableLiveData<ProductDetailUiState>()
    val productDetailUiState: LiveData<ProductDetailUiState> = _productDetailUiState

    fun checkProductWishlist(productId: String) {
        val data = appDatabase.wishlistDao().getWishlistProduct(productId)
        _isWishlist.value = data != null
    }

    fun addProductToWishlist(data: Wishlist) {
        appDatabase.wishlistDao().addWishlistProduct(data)
        checkProductWishlist(data.productId)
    }

    fun removeProductFromWishlist(productId: String) {
        appDatabase.wishlistDao().removeProductFromWishlist(
            productId
        )
        checkProductWishlist(productId)
    }

    fun addProductToCart(product: Cart, callback: (String) -> Unit) {
        try {
            appDatabase.cartDao().insertProductToCart(product)
            callback("Product added to cart!")
        } catch (e: SQLiteConstraintException) {
            if (e.message!!.contains("SQLITE_CONSTRAINT_PRIMARYKEY")) {
                val updatedProduct = getSingleCartProduct(product.productId)
                Log.d("TAGGG", product.quantity.toString())
                if (updatedProduct.quantity < updatedProduct.productStock) {
                    updateCartProductQuantity(product.productId, product.productVariant)
                    callback("Product added to cart!")
                } else {
                    callback("Product qty cart has reach limit stock!")
                }
            }
        }
    }

    private fun updateCartProductQuantity(productId: String, productVariant: String) {
        appDatabase.cartDao().updateCartProductQuantity(1, productId, productVariant)
    }

    private fun getSingleCartProduct(productId: String): Cart {
        return appDatabase.cartDao().getSingleCartProduct(productId)
    }

    fun getSavedProductDetail(): ProductDetailData? {
        return savedStateHandle.get<ProductDetailData>("productDetail")
    }

    init {
        Log.d("LOGGG", "vm Initialized")
    }

    fun getProductDetail(productId: String) {
        viewModelScope.launch {
            _productDetailUiState.value = ProductDetailUiState.Loading
            try {
                val response = apiService.productDetail(productId)
                savedStateHandle["productDetail"] = response.data!!
                _productDetailUiState.value = ProductDetailUiState.Success(response.data!!)
            } catch (e: Exception) {
                if (e is HttpException) {
                    _productDetailUiState.value = ProductDetailUiState.Error
                } else {
                    _productDetailUiState.value = ProductDetailUiState.Error
                }
            }
        }
    }
}

sealed class ProductDetailUiState {
    object Loading : ProductDetailUiState()
    data class Success(val product: ProductDetailData) : ProductDetailUiState()
    object Error : ProductDetailUiState()
}
