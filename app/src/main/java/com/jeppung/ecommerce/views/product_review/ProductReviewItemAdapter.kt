package com.jeppung.ecommerce.views.product_review

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.ItemProductReviewBinding
import com.jeppung.ecommerce.repository.models.ProductReviewData

class ProductReviewItemAdapter(private val items: List<ProductReviewData>) :
    RecyclerView.Adapter<ProductReviewItemAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ItemProductReviewBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemProductReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.apply {
            Glide.with(holder.itemView.context).load(items[position].userImage)
                .error(R.drawable.thumbnail).circleCrop()
                .into(productReviewUserImage)
            productReviewUserName.text = items[position].userName
            productReviewUserReview.text = items[position].userReview
            productReviewUserRating.rating = items[position].userRating.toFloat()
        }
    }

    override fun getItemCount(): Int = items.size
}
