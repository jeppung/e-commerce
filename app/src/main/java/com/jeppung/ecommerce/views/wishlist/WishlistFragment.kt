package com.jeppung.ecommerce.views.wishlist

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentWishlistBinding
import com.jeppung.ecommerce.repository.room.entity.Cart
import com.jeppung.ecommerce.repository.room.entity.Wishlist
import com.jeppung.ecommerce.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class WishlistFragment : Fragment() {

    private lateinit var binding: FragmentWishlistBinding
    private val viewModel: WishlistViewModel by viewModels()

    private lateinit var wishlistData: List<Wishlist>

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    private var wishlistItemCallback = object : OnWishlistClickListener {
        override fun onWishlistClick(productId: String) {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("wishlist", "wishlist item clicked")
            }
            val bundle = Bundle().apply {
                putString("product_id", productId)
            }
            view?.findNavController()
                ?.navigate(
                    R.id.action_wishlistFragment_to_productDetailComposeFragment,
                    bundle
                )
        }

        override fun onRemoveClick(productId: String) {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("wishlist", "wishlist item remove")
            }
            try {
                viewModel.removeProductFromWishlist(productId)
                Snackbar.make(
                    view!!,
                    "Product removed from wishlist!",
                    Snackbar.LENGTH_SHORT
                )
                    .show()
            } catch (e: Exception) {
                Snackbar.make(
                    view!!,
                    e.message.toString(),
                    Snackbar.LENGTH_SHORT
                )
            }

            viewModel.getAllProductWishlist()
        }

        override fun onAddToCartClicked(product: Wishlist) {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("wishlist", "add to cart")
            }
            try {
                viewModel.addProductToCart(
                    Cart(
                        productId = product.productId,
                        productTitle = product.productTitle,
                        productImage = product.productImage,
                        productPrice = AppUtils.parseCurrencyIdr(product.productPrice.toString()),
                        productStock = product.productStock,
                        productVariant = product.productVariant,
                        quantity = 1,
                        isChecked = false
                    )
                )
                Snackbar.make(view!!, "Product added to cart", Snackbar.LENGTH_SHORT)
                    .show()
            } catch (e: SQLiteConstraintException) {
                if (e.message!!.contains("SQLITE_CONSTRAINT_PRIMARYKEY")) { // handling if the same id product added to cart then add the exist qty
                    val product = viewModel.getSingleCartProduct(product.productId)
                    viewModel.updateCartProductQuantity(product.productId, product.productVariant)
                    Snackbar.make(view!!, "Product added to cart", Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWishlistBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * Observing wishlist data
         */
        viewModel.getAllProductWishlist().observe(viewLifecycleOwner) { data ->
            wishlistData = data
            binding.wishlistTotal.text =
                requireContext().resources.getString(R.string.wishlist_total, wishlistData.size)
            setRecycleView()
        }

        /**
         * Setup list style toogle listener
         */
        binding.wishlistListStyleBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("wishlist", "list style change")
            }
            setRecycleView()
        }
    }

    private fun setRecycleView() {
        if (binding.wishlistListStyleBtn.isChecked) {
            binding.wishlistListStyleBtn.buttonDrawable =
                resources.getDrawable(R.drawable.ic_grid_view)
            if (wishlistData.isEmpty()) {
                binding.wishlistErrorView.apply {
                    storeErrorButton.visibility = View.GONE
                    storeErrorView.visibility = View.VISIBLE
                }
                binding.wishlistRecycleView.visibility = View.GONE
                binding.wishlistHeader.visibility = View.GONE
            } else {
                binding.wishlistHeader.visibility = View.VISIBLE
                binding.wishlistErrorView.storeErrorView.visibility = View.GONE
                binding.wishlistRecycleView.apply {
                    visibility = View.VISIBLE
                    layoutManager = GridLayoutManager(requireContext(), 2)
                    adapter = WishlistItemAdapter(wishlistData, true, wishlistItemCallback)
                }
            }
        } else {
            binding.wishlistListStyleBtn.buttonDrawable =
                resources.getDrawable(R.drawable.ic_format_list_bulleted)
            if (wishlistData.isEmpty()) {
                binding.wishlistErrorView.apply {
                    storeErrorView.visibility = View.VISIBLE
                    storeErrorButton.visibility = View.GONE
                }
                binding.wishlistRecycleView.visibility = View.GONE
                binding.wishlistHeader.visibility = View.GONE
            } else {
                binding.wishlistHeader.visibility = View.VISIBLE
                binding.wishlistErrorView.storeErrorView.visibility = View.GONE
                binding.wishlistRecycleView.apply {
                    visibility = View.VISIBLE
                    layoutManager = LinearLayoutManager(requireContext())
                    adapter = WishlistItemAdapter(wishlistData, false, wishlistItemCallback)
                }
            }
        }
    }
}
