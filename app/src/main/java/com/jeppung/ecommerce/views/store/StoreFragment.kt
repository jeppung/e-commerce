package com.jeppung.ecommerce.views.store

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.findNavController
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentStoreBinding
import com.jeppung.ecommerce.repository.models.ProductItem
import com.jeppung.ecommerce.repository.models.QueryModel
import com.jeppung.ecommerce.repository.prefs.UserPreference
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class StoreFragment : Fragment() {

    private lateinit var binding: FragmentStoreBinding
    private val viewModel: StoreViewModel by activityViewModels()

    private var filterData: MutableList<Chip> = mutableListOf()
    private lateinit var queryData: QueryModel
    private lateinit var pagingData: PagingData<ProductItem>
    private var searchString: String? = ""

    private lateinit var adapter: StoreViewPagingAdapter

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    @Inject
    lateinit var userPreference: UserPreference

    @Inject
    lateinit var appDatabase: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parentFragmentManager.setFragmentResultListener("searchKey", this) { _, bundle ->
            queryData = bundle.getParcelable<QueryModel>("QUERY_MODEL")!!
            searchString = queryData.search
            binding.storeSearchInput.setText(queryData.search)
            setupFilterChip()
            viewModel.searchProduct(queryData)
        }

        parentFragmentManager.setFragmentResultListener("filterKey", this) { _, bundle ->
            filterData.clear()
            queryData = bundle.getParcelable<QueryModel>("QUERY_MODEL")!!
            setupFilterChip()
            viewModel.searchProduct(queryData)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStoreBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.pagingData.collect {
                    pagingData = it
                    setRecycleViewLayout(it)
                }
            }
        }

        /**
         * To make store data still persist when changing bottomnav fragment
         */
        viewModel.query.observe(viewLifecycleOwner) { data ->
            queryData = data
            binding.storeSearchInput.setText(data.search)
            setupFilterChip()
        }

        binding.storeListStyleBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("store", "list style change")
            }
            setRecycleViewLayout(pagingData)
        }

        binding.storeSwipeRefresh.setOnRefreshListener {
            if (binding.storeSwipeRefresh.isRefreshing) {
                viewModel.searchProduct(
                    QueryModel(
                        search = queryData.search,
                        brand = queryData.brand,
                        lowest = queryData.lowest,
                        highest = queryData.highest,
                        sort = queryData.sort,
                        page = null
                    )
                )
                binding.storeSwipeRefresh.isRefreshing = false
            }
        }

        binding.storeSearchInput.setOnFocusChangeListener { v, _ ->
            if (v is TextInputEditText && v.isFocused) {
                val bundle = Bundle().apply {
                    putParcelable(
                        "QUERY_MODEL",
                        QueryModel(
                            searchString,
                            brand = queryData.brand,
                            sort = queryData.sort,
                            highest = queryData.highest,
                            lowest = queryData.lowest,
                            page = null
                        )
                    )
                }
                view.findNavController()
                    .navigate(R.id.action_storeFragment_to_storeSearchFragment, bundle)
            }
        }

        binding.storeFilterBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("store", "filter")
            }
            val bundle = Bundle()

            bundle.apply {
                putString("searchString", queryData.search)
                putSerializable("FILTER_DATA", ArrayList(filterData))
            }
            view.findNavController().navigate(R.id.storeBottomSheetFragment, bundle)
        }
    }

    private fun setupFilterChip() {
        binding.storeFilterList.removeAllViews()
        queryData.apply {
            sort?.let {
                val chip = Chip(requireContext()).apply {
                    isClickable = false
                }
                chip.text = it
                chip.tag = "sort"
                filterData.add(chip)
                binding.storeFilterList.addView(chip)
            }
            brand?.let {
                val chip = Chip(requireContext()).apply {
                    isClickable = false
                }
                chip.text = it
                chip.tag = "brand"
                filterData.add(chip)
                binding.storeFilterList.addView(chip)
            }
            lowest?.let {
                val chip = Chip(requireContext()).apply {
                    isClickable = false
                }
                chip.text = "< $it"
                chip.tag = "lowest"
                filterData.add(chip)
                binding.storeFilterList.addView(chip)
            }
            highest?.let {
                val chip = Chip(requireContext()).apply {
                    isClickable = false
                }
                chip.text = "> $it"
                chip.tag = "highest"
                filterData.add(chip)
                binding.storeFilterList.addView(chip)
            }
        }
    }

    private fun setUpLinearViewAdapter(pagingData: PagingData<ProductItem>) {
        adapter = StoreViewPagingAdapter(isGrid = false) {
            val itemProduct = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, it.productId)
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.productName)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, it.brand)
                putInt(FirebaseAnalytics.Param.PRICE, it.productPrice)
            }

            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                param(FirebaseAnalytics.Param.ITEM_LIST_ID, it.productId)
                param(FirebaseAnalytics.Param.ITEM_LIST_NAME, it.productName)
                param(FirebaseAnalytics.Param.ITEMS, arrayOf(itemProduct))
            }

            val bundle = Bundle().apply {
                putString("product_id", it.productId)
            }
            requireView().findNavController()
                .navigate(R.id.action_storeFragment_to_productDetailComposeFragment, bundle)
        }
        binding.storeRecycleView.layoutManager = LinearLayoutManager(requireContext())

        adapter.submitData(viewLifecycleOwner.lifecycle, pagingData)
        adapter.addLoadStateListener { loadStates ->
            loadStatesHandler(loadStates)
        }

        binding.storeRecycleView.adapter = adapter.withLoadStateFooter(
            footer = FooterLoadStateAdapter(requireContext())
        )
    }

    private fun setUpGridViewAdapter(pagingData: PagingData<ProductItem>) {
        adapter = StoreViewPagingAdapter(isGrid = true) {
            val itemProduct = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, it.productId)
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.productName)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, it.brand)
                putInt(FirebaseAnalytics.Param.PRICE, it.productPrice)
            }

            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                param(FirebaseAnalytics.Param.ITEM_LIST_ID, it.productId)
                param(FirebaseAnalytics.Param.ITEM_LIST_NAME, it.productName)
                param(FirebaseAnalytics.Param.ITEMS, arrayOf(itemProduct))
            }

            val bundle = Bundle().apply {
                putString("product_id", it.productId)
            }
            requireView().findNavController()
                .navigate(R.id.action_storeFragment_to_productDetailComposeFragment, bundle)
        }.apply {
            addLoadStateListener { loadStates ->
                loadStatesHandler(loadStates)
            }
            submitData(viewLifecycleOwner.lifecycle, pagingData)
        }

        val concatAdapter = adapter.withLoadStateFooter(
            footer = FooterLoadStateAdapter(requireContext())
        )

        binding.storeRecycleView.layoutManager = GridLayoutManager(requireContext(), 2).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (concatAdapter.getItemViewType(position) == 1) {
                        spanCount
                    } else {
                        1
                    }
                }
            }
        }
        binding.storeRecycleView.adapter = concatAdapter
    }

    private fun loadStatesHandler(loadStates: CombinedLoadStates) {
        val isLoading = loadStates.refresh is LoadState.Loading
        val isError = loadStates.refresh is LoadState.Error

        if (isLoading) {
            pagingDataLoading()
        } else if (isError) {
            pagingDataError(loadStates.refresh as LoadState.Error)
        } else {
            pagingDataSuccess()
        }
    }

    private fun pagingDataError(state: LoadState.Error) {
        binding.storeRecycleView.visibility = View.INVISIBLE
        if (binding.storeListStyleBtn.isChecked) {
            binding.storeShimmerGrid.storeShimmerGrid.visibility = View.GONE
            binding.storeShimmerGrid.storeShimmerGrid.stopShimmer()
        } else {
            binding.storeShimmerLinear.storeShimmerLinear.visibility = View.GONE
            binding.storeShimmerLinear.storeShimmerLinear.stopShimmer()
        }

        when (state.error) {
            is HttpException -> {
                if (state.error.message!!.contains("Not Found")) {
                    binding.storeErrorView.storeErrorView.visibility = View.VISIBLE
                    binding.storeErrorView.storeErrorTitle.text = getString(R.string.text_empty_title)
                    binding.storeErrorView.storeErrorDesc.text = getString(R.string.text_empty_description)
                    binding.storeErrorView.storeErrorButton.text = getString(R.string.button_reset)
                    binding.storeErrorView.storeErrorButton.setOnClickListener {
                        resetStore()
                    }
                } else {
                    binding.storeErrorView.storeErrorView.visibility = View.VISIBLE
                    binding.storeErrorView.storeErrorTitle.text = getString(R.string.text_error_title)
                    binding.storeErrorView.storeErrorDesc.text = getString(R.string.text_error_description)
                    binding.storeErrorView.storeErrorButton.text = getString(R.string.button_refresh)
                    binding.storeErrorView.storeErrorButton.setOnClickListener {
                        refreshStore()
                    }
                }
            }

            is IOException -> {
                binding.storeErrorView.storeErrorView.visibility = View.VISIBLE
                binding.storeErrorView.storeErrorTitle.text = getString(R.string.text_connection_title)
                binding.storeErrorView.storeErrorDesc.text = getString(R.string.text_connection_description)
                binding.storeErrorView.storeErrorButton.text = getString(R.string.button_refresh)
                binding.storeErrorView.storeErrorButton.setOnClickListener {
                    refreshStore()
                }
            }

            else -> {
                AppUtils.authTimeoutHandler(userPreference, appDatabase)
                Toast.makeText(
                    requireContext(),
                    requireContext().getString(R.string.text_kick_out),
                    Toast.LENGTH_SHORT
                ).show()
                view!!.findNavController().navigate(R.id.action_global_pre_login)
            }
        }
    }
    private fun pagingDataLoading() {
        binding.storeErrorView.storeErrorView.visibility = View.GONE
        binding.storeRecycleView.visibility = View.INVISIBLE
        binding.storeFilterContainer.visibility = View.INVISIBLE

        if (binding.storeListStyleBtn.isChecked) {
            binding.storeShimmerGrid.storeShimmerGrid.visibility = View.VISIBLE
            binding.storeShimmerGrid.storeShimmerGrid.startShimmer()
        } else {
            binding.storeShimmerLinear.storeShimmerLinear.visibility = View.VISIBLE
            binding.storeShimmerLinear.storeShimmerLinear.startShimmer()
        }
    }
    private fun pagingDataSuccess() {
        binding.storeShimmerLinear.storeShimmerLinear.visibility = View.GONE
        binding.storeRecycleView.visibility = View.VISIBLE
        binding.storeFilterContainer.visibility = View.VISIBLE

        if (binding.storeListStyleBtn.isChecked) {
            binding.storeShimmerGrid.storeShimmerGrid.visibility = View.GONE
            binding.storeShimmerGrid.storeShimmerGrid.stopShimmer()
        } else {
            binding.storeShimmerLinear.storeShimmerLinear.visibility = View.GONE
            binding.storeShimmerLinear.storeShimmerLinear.stopShimmer()
        }
    }

    private fun resetStore() {
        searchString = ""
        binding.storeSearchInput.setText("")
        binding.storeFilterList.removeAllViews()
        filterData.clear()

        viewModel.searchProduct(
            QueryModel(
                search = "",
                brand = null,
                lowest = null,
                highest = null,
                sort = null,
                page = null
            )
        )
    }

    private fun refreshStore() {
        viewModel.searchProduct(
            queryData
        )
    }

    private fun setRecycleViewLayout(pagingData: PagingData<ProductItem>) {
        if (binding.storeListStyleBtn.isChecked) {
            setUpGridViewAdapter(pagingData)
            binding.storeListStyleBtn.buttonDrawable = getDrawable(
                requireContext(),
                R.drawable.ic_grid_view
            )
        } else {
            setUpLinearViewAdapter(pagingData)
            binding.storeListStyleBtn.buttonDrawable = getDrawable(
                requireContext(),
                R.drawable.ic_format_list_bulleted
            )
        }
    }
}
