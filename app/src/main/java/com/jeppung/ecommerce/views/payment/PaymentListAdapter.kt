package com.jeppung.ecommerce.views.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jeppung.ecommerce.databinding.ItemPaymentBinding
import com.jeppung.ecommerce.repository.models.PaymentList
import com.jeppung.ecommerce.repository.models.PaymentMethod

class PaymentListAdapter(
    private val items: List<PaymentList>,
    private val onClick: (PaymentMethod) -> Unit
) :
    RecyclerView.Adapter<PaymentListAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ItemPaymentBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: PaymentList) {
            binding.paymentTitle.text = item.title
            binding.paymentMethodRecycleView.layoutManager =
                LinearLayoutManager(itemView.context)
            binding.paymentMethodRecycleView.addItemDecoration(
                DividerItemDecoration(itemView.context, RecyclerView.VERTICAL),
            )
            binding.paymentMethodRecycleView.adapter =
                PaymentMethodAdapter(item.item) {
                    onClick(it)
                }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding = ItemPaymentBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PaymentListAdapter.ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size
}
