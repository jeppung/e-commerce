package com.jeppung.ecommerce.views.store

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.core.view.forEach
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.jeppung.ecommerce.databinding.FragmentStoreBottomSheetBinding
import com.jeppung.ecommerce.repository.models.QueryModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class StoreBottomSheetFragment() : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentStoreBottomSheetBinding
    private var filterData: ArrayList<Chip> = arrayListOf()
    private var searchString: String = ""

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStoreBottomSheetBinding.inflate(inflater)

        filterData = arguments?.getSerializable("FILTER_DATA") as ArrayList<Chip>
        searchString = arguments?.getString("searchString") ?: ""

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (filterData.isNotEmpty()) {
            binding.storeBottomSheetReset.visibility = View.VISIBLE
            filterData.forEach { chip ->
                when (chip.tag) {
                    "sort" -> binding.storeFilterUrutkan.children.filterIsInstance<Chip>()
                        .first { it.text.contains(chip.text) }.isChecked = true

                    "brand" -> binding.storeFilterKategori.children.filterIsInstance<Chip>()
                        .first { it.text.contains(chip.text) }.isChecked = true

                    "lowest" -> binding.storeFilterHargaTerendahInput.setText(chip.text.filter { it.isDigit() })

                    "highest" -> binding.storeFilterHargaTertinggiInput.setText(chip.text.filter { it.isDigit() })
                }
            }
        }

        binding.storeFilterUrutkan.setOnCheckedStateChangeListener { group, _ ->
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("store_bottom_sheet", "filter urutan change")
            }
            group.forEach { view ->
                (view as Chip).setOnCheckedChangeListener { chip, isChecked ->
                    if (isChecked) {
                        group.check(chip.id)
                    }
                }
            }
        }

        binding.storeFilterKategori.setOnCheckedStateChangeListener { group, _ ->
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("store_bottom_sheet", "filter kategori change")
            }
            group.forEach { view ->
                (view as Chip).setOnCheckedChangeListener { chip, isChecked ->
                    if (isChecked) {
                        group.check(chip.id)
                    }
                }
            }
        }

        binding.storeBottomSheetReset.setOnClickListener {
            binding.storeFilterUrutkan.clearCheck()
            binding.storeFilterKategori.clearCheck()
            binding.storeFilterHargaTerendahInput.text = null
            binding.storeFilterHargaTertinggiInput.text = null

            binding.storeBottomSheetReset.visibility = View.INVISIBLE
        }

        binding.storeGetFilterProductBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("store_bottom_sheet", "get filter data")
            }
            val data = QueryModel(
                search = searchString,
                sort = if (binding.storeFilterUrutkan.checkedChipId == -1) {
                    null
                } else view.findViewById<Chip>(
                    binding.storeFilterUrutkan.checkedChipId
                ).text.toString(),
                brand = if (binding.storeFilterKategori.checkedChipId == -1) {
                    null
                } else view.findViewById<Chip>(
                    binding.storeFilterKategori.checkedChipId
                ).text.toString(),
                page = null,
                lowest = if (binding.storeFilterHargaTerendahInput.text.isNullOrEmpty()) {
                    null
                } else binding.storeFilterHargaTerendahInput.text.toString()
                    .toInt(),
                highest = if (binding.storeFilterHargaTertinggiInput.text.isNullOrEmpty()) {
                    null
                } else binding.storeFilterHargaTertinggiInput.text.toString()
                    .toInt()
            )
            val bundle = Bundle().apply {
                putParcelable("QUERY_MODEL", data)
            }
            parentFragmentManager.setFragmentResult("filterKey", bundle)
            Navigation.setViewNavController(view, findNavController())
            view.findNavController().navigateUp()
        }
    }
}
