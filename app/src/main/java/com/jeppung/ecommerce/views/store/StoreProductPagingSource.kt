package com.jeppung.ecommerce.views.store

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.jeppung.ecommerce.repository.api_service.ApiService
import com.jeppung.ecommerce.repository.models.ProductItem
import com.jeppung.ecommerce.repository.models.QueryModel
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception

class StoreProductPagingSource(
    private val apiService: ApiService,
    private val query: QueryModel
) : PagingSource<Int, ProductItem>() {
    override fun getRefreshKey(state: PagingState<Int, ProductItem>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProductItem> {
        try {
            val currentPage = params.key ?: 1
            val response = apiService.getSearchProduct(
                query.search,
                query.brand,
                query.lowest,
                query.highest,
                query.sort,
                5,
                page = currentPage
            )
            val maxPage = response.data!!.totalPages
            return LoadResult.Page(
                data = response.data.items,
                prevKey = if (currentPage == 1) null else -1,
                nextKey = if (currentPage == maxPage) null else currentPage.plus(1)
            )
        } catch (e: IOException) {
            Log.d("TAGGG", "IOException: $e")
            return LoadResult.Error(e)
        } catch (e: HttpException) {
            return LoadResult.Error(e)
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }
}
