package com.jeppung.ecommerce.views.home

import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jeppung.ecommerce.repository.prefs.UserPreference
import com.jeppung.ecommerce.repository.room.AppDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val userPreference: UserPreference,
    private val appDatabase: AppDatabase
) : ViewModel() {

    private val _locale = MutableLiveData<String>()
    val locale: LiveData<String> = _locale

    private val _darkMode = MutableLiveData<Boolean>()
    val darkMode: LiveData<Boolean> = _darkMode

    init {
        getLocale()
        _darkMode.value = userPreference.getDarkMode()
    }

    fun setLocale(locale: String) {
        val appLocale: LocaleListCompat = LocaleListCompat.forLanguageTags(locale)
        AppCompatDelegate.setApplicationLocales(appLocale)
        _locale.value = AppCompatDelegate.getApplicationLocales().toLanguageTags()
    }

    fun setUiMode(mode: Int) {
        AppCompatDelegate.setDefaultNightMode(mode)
        if (mode == AppCompatDelegate.MODE_NIGHT_YES) {
            userPreference.setDarkMode(true)
            _darkMode.value = true
        } else {
            userPreference.setDarkMode(false)
            _darkMode.value = false
        }
    }

    fun clearAllUserData() {
        userPreference.setAccessToken(null)
        userPreference.setRefreshToken(null)
        userPreference.setUserName(null)
    }

    fun destroyDatabase() {
        appDatabase.clearAllTables()
    }

    private fun getLocale() {
        _locale.value = AppCompatDelegate.getApplicationLocales().toLanguageTags()
    }
}
