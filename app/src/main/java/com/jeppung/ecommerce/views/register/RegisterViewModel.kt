package com.jeppung.ecommerce.views.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.jeppung.ecommerce.repository.api_service.ApiService
import com.jeppung.ecommerce.repository.models.LoginResponse
import com.jeppung.ecommerce.repository.models.RegisterResponse
import com.jeppung.ecommerce.repository.models.UserModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val apiService: ApiService
) : ViewModel() {

    private val _registerUiState = MutableSharedFlow<RegisterUiState>()
    val registerUiState: SharedFlow<RegisterUiState> = _registerUiState

    fun register(
        data: UserModel
    ) {
        viewModelScope.launch {
            _registerUiState.emit(RegisterUiState.Loading)
            try {
                val response = apiService.register(data)
                _registerUiState.emit(RegisterUiState.Success(response, data.firebaseToken))
            } catch (e: Exception) {
                if (e is HttpException) {
                    val errResponse = Gson().fromJson(
                        e.response()?.errorBody()?.string(),
                        LoginResponse::class.java
                    )
                    _registerUiState.emit(RegisterUiState.Error(errResponse.message))
                } else {
                    _registerUiState.emit(RegisterUiState.Error(e.message.toString()))
                }
            }
        }
    }
}

sealed class RegisterUiState() {
    object Loading : RegisterUiState()
    data class Success(val data: RegisterResponse, val firebaseToken: String) :
        RegisterUiState()
    data class Error(val message: String) : RegisterUiState()
}
