package com.jeppung.ecommerce.views.login

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.messaging.FirebaseMessaging
import com.jeppung.ecommerce.R
import com.jeppung.ecommerce.databinding.FragmentLoginBinding
import com.jeppung.ecommerce.repository.models.UserModel
import com.jeppung.ecommerce.repository.prefs.UserPreference
import com.jeppung.ecommerce.utils.AppUtils
import com.jeppung.ecommerce.views.store.StoreViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private val viewModel: LoginViewModel by viewModels()
    private val storeViewModel: StoreViewModel by activityViewModels()

    @Inject
    lateinit var userPreference: UserPreference

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    @Inject
    lateinit var firebaseMessaging: FirebaseMessaging

    private val method = "EMAIL & PASSWORD - Joshua"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * Login State Validation
         */
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.loginUiState.collect {
                    when (it) {
                        is LoginUiState.Loading -> {
                            binding.loginProgressBar.visibility = View.VISIBLE
                        }
                        is LoginUiState.Success -> {
                            binding.loginProgressBar.visibility = View.GONE
                            val response = it.data

                            userPreference.apply {
                                setAccessToken(response.data?.accessToken)
                                setRefreshToken(response.data?.refreshToken)
                                setFirebaseToken(it.firebaseToken)
                                setUserName(response.data?.userName)
                            }

                            Toast.makeText(requireContext(), response.message, Toast.LENGTH_SHORT)
                                .show()

                            if (response.data!!.userName == "") {
                                view.findNavController()
                                    .navigate(R.id.action_loginFragment_to_registerProfileFragment)
                            } else {
                                storeViewModel.resetAllStoreData() // Reseting store data caused by viewModel not cleared
                                view.findNavController().navigate(
                                    R.id.action_global_main
                                )
                            }
                        }
                        is LoginUiState.Error -> {
                            binding.loginProgressBar.visibility = View.GONE
                            Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }

        /**
         * Email input validation
         */
        binding.loginEmailInput.addTextChangedListener { input ->
            checkInput(input!!, EMAIL_TYPE)
        }

        /**
         * Password input validation
         */
        binding.loginPasswordInput.addTextChangedListener { input ->
            checkInput(input!!, PASSWORD_TYPE)
        }

        /**
         * Register button onClick function
         */
        binding.loginRegisterBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("login", "Register")
            }
            view.findNavController()
                .navigate(R.id.action_loginFragment_to_registerFragment)
        }

        /**
         * Login button onClick function
         */
        binding.loginLoginBtn.setOnClickListener {
            firebaseAnalytics.logEvent("BUTTON CLICKED") {
                param("login", "Login")
            }
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN) {
                param("Method", method)
            }
            firebaseMessaging.token.addOnCompleteListener { task ->
                try {
                    viewModel.login(
                        UserModel(
                            email = binding.loginEmailInput.text.toString().trim(),
                            password = binding.loginPasswordInput.text.toString().trim(),
                            firebaseToken = task.result
                        )
                    )
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), e.message, Toast.LENGTH_SHORT).show()
                }
            }
        }

        /**
         * Configuring spanned text
         */
        val spannedBottomInfo = AppUtils.setSpannableBottomInfo(
            requireContext(),
            R.string.terms_and_conditions_login,
            resources.getString(R.string.terms),
            resources.getString(R.string.conditions)
        )
        binding.loginBottomInfo.text = spannedBottomInfo
    }

    private fun checkInput(input: Editable, type: String) {
        when (type) {
            EMAIL_TYPE -> {
                if (input.isNotEmpty()) {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(input).matches()) {
                        EMAIL_CHECK = false
                        binding.loginEmailInputLayout.apply {
                            error = resources.getString(R.string.email_error_validation)
                            isErrorEnabled = true
                            errorIconDrawable = null
                        }
                    } else {
                        EMAIL_CHECK = true
                        binding.loginEmailInputLayout.apply {
                            error = null
                            isErrorEnabled = false
                        }
                    }

                    binding.loginLoginBtn.isEnabled = PASSWORD_CHECK && EMAIL_CHECK
                } else {
                    EMAIL_CHECK = false
                    binding.loginEmailInputLayout.apply {
                        error = null
                        isErrorEnabled = false
                    }
                    binding.loginLoginBtn.isEnabled = false
                }
            }

            PASSWORD_TYPE -> {
                if (input.isNotEmpty()) {
                    if (input.length < 8) {
                        PASSWORD_CHECK = false
                        binding.loginPasswordInputLayout.apply {
                            error = resources.getString(R.string.password_error_validation)
                            isErrorEnabled = true
                            errorIconDrawable = null
                        }
                    } else {
                        PASSWORD_CHECK = true
                        binding.loginPasswordInputLayout.apply {
                            error = null
                            isErrorEnabled = false
                        }
                    }

                    binding.loginLoginBtn.isEnabled = EMAIL_CHECK && PASSWORD_CHECK
                } else {
                    PASSWORD_CHECK = false
                    binding.loginPasswordInputLayout.apply {
                        error = null
                        isErrorEnabled = false
                    }
                    binding.loginLoginBtn.isEnabled = false
                }
            }
        }
    }

    companion object {
        private var EMAIL_CHECK = false
        private var PASSWORD_CHECK = false
        private const val EMAIL_TYPE = "EMAIL_TYPE"
        private const val PASSWORD_TYPE = "PASSWORD_TYPE"
    }
}
