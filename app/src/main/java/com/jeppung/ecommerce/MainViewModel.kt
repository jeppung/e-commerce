package com.jeppung.ecommerce

import androidx.lifecycle.ViewModel
import com.jeppung.ecommerce.repository.room.AppDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val appDatabase: AppDatabase
) : ViewModel() {

    fun getCartSize() = appDatabase.cartDao().getAllCartProduct()

    fun getNotificationSize() = appDatabase.notificationDao().getAllNotifications()

    fun getWishlistSize() = appDatabase.wishlistDao().getAllWishlistProduct()

    init {
        getCartSize()
        getNotificationSize()
        getWishlistSize()
    }
}
