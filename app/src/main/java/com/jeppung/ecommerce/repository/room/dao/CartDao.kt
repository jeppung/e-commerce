package com.jeppung.ecommerce.repository.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.jeppung.ecommerce.repository.room.entity.Cart

@Dao
interface CartDao {

    @Insert
    fun insertProductToCart(product: Cart)

    @Query(
        "UPDATE cart SET quantity = quantity + :quantity WHERE productId = :productId AND productVariant = :productVariant"
    )
    fun updateCartProductQuantity(quantity: Int, productId: String, productVariant: String)

    @Update
    fun updateCartProduct(product: Cart)

    @Query("DELETE FROM cart WHERE productId = :productId")
    fun deleteCartProductWithId(productId: String)

    @Query("UPDATE cart SET isChecked = :value")
    fun setAllCartProductCheckedValue(value: Int)

    @Query("SELECT * FROM cart WHERE isChecked = 1")
    fun getCheckedCartProduct(): List<Cart>

    @Query("SELECT * FROM cart")
    fun getAllCartProduct(): LiveData<List<Cart>>

    @Query("SELECT * FROM cart WHERE productId = :productId")
    fun getSingleCartProduct(productId: String): Cart
}
