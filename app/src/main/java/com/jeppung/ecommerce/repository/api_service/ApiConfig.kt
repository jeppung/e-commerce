package com.jeppung.ecommerce.repository.api_service

class ApiConfig {
    companion object {
        const val HOST = "10.64.250.152:8080"
        const val LOGIN_PATH = "/login"
        const val REGISTER_PATH = "/register"
        const val REFRESH_PATH = "/refresh"
        const val HEADER_API_KEY = "6f8856ed-9189-488f-9011-0ff4b6c08edc"
    }
}
