package com.jeppung.ecommerce.repository.models

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class InvoiceResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: InvoiceData,

    @field:SerializedName("message")
    val message: String
)

@Keep
@Parcelize
data class InvoiceData(

    @field:SerializedName("date")
    val date: String,

    @field:SerializedName("total")
    val total: Int,

    @field:SerializedName("invoiceId")
    val invoiceId: String,

    @field:SerializedName("payment")
    val payment: String,

    @field:SerializedName("time")
    val time: String,

    @field:SerializedName("status")
    val status: Boolean
) : Parcelable
