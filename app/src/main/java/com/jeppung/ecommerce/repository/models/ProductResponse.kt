package com.jeppung.ecommerce.repository.models

import com.google.gson.annotations.SerializedName

data class ProductResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: ProductData?,

    @field:SerializedName("message")
    val message: String
)

data class ProductData(

    @field:SerializedName("pageIndex")
    val pageIndex: Int,

    @field:SerializedName("itemsPerPage")
    val itemsPerPage: Int,

    @field:SerializedName("currentItemCount")
    val currentItemCount: Int,

    @field:SerializedName("totalPages")
    val totalPages: Int,

    @field:SerializedName("items")
    val items: List<ProductItem>
)

data class ProductItem(

    @field:SerializedName("image")
    val image: String,

    @field:SerializedName("sale")
    val sale: Int,

    @field:SerializedName("productId")
    val productId: String,

    @field:SerializedName("store")
    val store: String,

    @field:SerializedName("productRating")
    val productRating: Float,

    @field:SerializedName("brand")
    val brand: String,

    @field:SerializedName("productName")
    val productName: String,

    @field:SerializedName("productPrice")
    val productPrice: Int
)
