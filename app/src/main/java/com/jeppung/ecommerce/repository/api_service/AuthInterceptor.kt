package com.jeppung.ecommerce.repository.api_service

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.gson.Gson
import com.jeppung.ecommerce.repository.models.LoginResponse
import com.jeppung.ecommerce.repository.prefs.UserPreference
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import retrofit2.Retrofit
import retrofit2.awaitResponse
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class AuthInterceptor @Inject constructor(
    @ApplicationContext
    private val context: Context,
    private val userPreference: UserPreference
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        val getCurrentRefreshToken = userPreference.getRefreshToken()
        synchronized(this) {
            return runBlocking {
                try {
                    val newToken = refreshToken(getCurrentRefreshToken!!)
                    userPreference.setAccessToken(newToken.accessToken)
                    userPreference.setRefreshToken(newToken.refreshToken)

                    response.request
                        .newBuilder()
                        .header("Authorization", "Bearer ${newToken.accessToken}")
                        .build()
                } catch (error: Throwable) {
                    ERROR_MESSAGE_BACKEND = error.message ?: ""
                    response.close()
                    null
                }
            }
        }
    }

    private suspend fun refreshToken(tokenRequest: String): Token {
        val interceptor = Interceptor.invoke { chain ->
            val request = chain
                .request()
                .newBuilder()
                .addHeader("API_KEY", ApiConfig.HEADER_API_KEY)
                .build()
            chain.proceed(request)
        }
        val chucker = ChuckerInterceptor.Builder(context)
            .collector(ChuckerCollector(context))
            .maxContentLength(250000L)
            .redactHeaders(emptySet())
            .alwaysReadResponseBody(false)
            .build()

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(chucker)
            .build()

        val apiService = Retrofit.Builder()
            .baseUrl("http://${ApiConfig.HOST}/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApiService::class.java)

        val request = apiService.refreshToken(
            hashMapOf(
                "token" to tokenRequest
            )
        ).awaitResponse()

        if (request.isSuccessful) {
            return Token(
                accessToken = request.body()!!.data!!.accessToken,
                refreshToken = request.body()!!.data!!.refreshToken
            )
        } else {
            val errMessage = Gson().fromJson(request.errorBody()!!.string(), LoginResponse::class.java)
            throw Exception(errMessage.message)
        }
    }

    companion object {
        var ERROR_MESSAGE_BACKEND = ""
    }

    data class Token(
        val accessToken: String,
        val refreshToken: String
    )
}
