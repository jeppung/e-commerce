package com.jeppung.ecommerce.repository.prefs

import android.content.Context

class UserPreference(context: Context) {

    private val preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun setIsOnboard(value: Boolean) {
        val editor = preferences.edit()
        editor.apply {
            putBoolean(IS_ONBOARD, value)
            apply()
        }
    }

    fun getIsOnboard(): Boolean = preferences.getBoolean(IS_ONBOARD, false)

    fun setAccessToken(value: String?) {
        val editor = preferences.edit()
        editor.apply {
            putString(ACCESS_TOKEN, value)
            apply()
        }
    }

    fun setFirebaseToken(token: String) {
        val editor = preferences.edit()
        editor.apply {
            putString(FIREBASE_TOKEN, token)
            apply()
        }
    }

    fun getAccessToken(): String? = preferences.getString(ACCESS_TOKEN, null)

    fun setRefreshToken(value: String?) {
        val editor = preferences.edit()
        editor.apply {
            putString(REFRESH_TOKEN, value)
            apply()
        }
    }

    fun getRefreshToken(): String? = preferences.getString(REFRESH_TOKEN, null)

    fun setUserName(value: String?) {
        val editor = preferences.edit()
        editor.apply {
            putString(USERNAME, value)
            apply()
        }
    }

    fun getUserName(): String? = preferences.getString(USERNAME, null)

    fun setUserImage(value: String?) {
        val editor = preferences.edit()
        editor.apply {
            putString(USERIMAGE, value)
            apply()
        }
    }

    fun getUserImage(): String? = preferences.getString(USERIMAGE, null)

    fun setDarkMode(value: Boolean) {
        val editor = preferences.edit()
        editor.apply {
            putBoolean(DARK_MODE, value)
            apply()
        }
    }

    fun getDarkMode(): Boolean = preferences.getBoolean(DARK_MODE, false)

    companion object {
        private const val PREFS_NAME = "user_pref"
        private const val IS_ONBOARD = "is_onboard"
        private const val ACCESS_TOKEN = "access_token"
        private const val REFRESH_TOKEN = "refresh_token"
        private const val USERNAME = "username"
        private const val USERIMAGE = "userimage"
        private const val DARK_MODE = "darkMode"
        private const val FIREBASE_TOKEN = ""
    }
}
