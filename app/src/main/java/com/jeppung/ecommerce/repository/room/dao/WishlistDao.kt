package com.jeppung.ecommerce.repository.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.jeppung.ecommerce.repository.room.entity.Wishlist

@Dao
interface WishlistDao {

    @Query("SELECT * FROM wishlist")
    fun getAllWishlistProduct(): LiveData<List<Wishlist>>

    @Query("SELECT * FROM wishlist WHERE productId = :productId")
    fun getWishlistProduct(productId: String): Wishlist?

    @Query("DELETE FROM wishlist WHERE productId = :productId")
    fun removeProductFromWishlist(productId: String)

    @Insert
    fun addWishlistProduct(product: Wishlist)
}
