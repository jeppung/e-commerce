package com.jeppung.ecommerce.repository.models

import com.google.gson.annotations.SerializedName

data class ReviewRatingRequest(

    @field:SerializedName("review")
    val review: String? = null,

    @field:SerializedName("rating")
    val rating: Int? = null,

    @field:SerializedName("invoiceId")
    val invoiceId: String
)
