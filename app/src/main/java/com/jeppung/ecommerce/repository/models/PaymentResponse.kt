package com.jeppung.ecommerce.repository.models

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

data class PaymentResponse(
    val code: Int,
    val data: List<PaymentList>?,
    val message: String
)

data class PaymentList(
    val item: List<PaymentMethod>,
    val title: String
)

data class PaymentListFirebase(
    private val data: Array<PaymentList>
)

@Keep
@Parcelize
data class PaymentMethod(
    val image: String,
    val label: String,
    val status: Boolean
) : Parcelable
