package com.jeppung.ecommerce.repository.models

import com.google.gson.annotations.SerializedName

data class RegisterResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: RegisterData?,

    @field:SerializedName("message")
    val message: String
)

data class RegisterData(

    @field:SerializedName("accessToken")
    val accessToken: String,

    @field:SerializedName("expiresAt")
    val expiresAt: Int,

    @field:SerializedName("refreshToken")
    val refreshToken: String
)
