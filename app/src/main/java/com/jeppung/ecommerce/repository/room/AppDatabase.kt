package com.jeppung.ecommerce.repository.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jeppung.ecommerce.repository.room.dao.CartDao
import com.jeppung.ecommerce.repository.room.dao.NotificationDao
import com.jeppung.ecommerce.repository.room.dao.WishlistDao
import com.jeppung.ecommerce.repository.room.entity.Cart
import com.jeppung.ecommerce.repository.room.entity.Notification
import com.jeppung.ecommerce.repository.room.entity.Wishlist

@Database(entities = [Wishlist::class, Cart::class, Notification::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun wishlistDao(): WishlistDao
    abstract fun cartDao(): CartDao
    abstract fun notificationDao(): NotificationDao

    companion object {
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (instance == null) {
                synchronized(AppDatabase::class.java) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "jeppung-ecommerce.db"
                    ).allowMainThreadQueries().build()
                }
            }
            return instance
        }
    }
}
