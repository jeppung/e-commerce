package com.jeppung.ecommerce.repository.room.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
data class Cart(
    @PrimaryKey val productId: String,
    val productImage: String,
    val productTitle: String,
    var productVariant: String,
    val productStock: Int,
    var productPrice: Long,
    var quantity: Int,
    var isChecked: Boolean
) : Parcelable
