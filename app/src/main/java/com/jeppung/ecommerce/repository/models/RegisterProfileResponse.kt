package com.jeppung.ecommerce.repository.models

import com.google.gson.annotations.SerializedName

data class RegisterProfileResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: RegisterProfileData?,

    @field:SerializedName("message")
    val message: String
)

data class RegisterProfileData(

    @field:SerializedName("userImage")
    val userImage: String,

    @field:SerializedName("userName")
    val userName: String
)
