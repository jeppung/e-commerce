package com.jeppung.ecommerce.repository.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class ProductReviewResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: List<ProductReviewData>?,

    @field:SerializedName("message")
    val message: String
)

@Parcelize
data class ProductReviewData(

    @field:SerializedName("userImage")
    val userImage: String,

    @field:SerializedName("userName")
    val userName: String,

    @field:SerializedName("userReview")
    val userReview: String,

    @field:SerializedName("userRating")
    val userRating: Int
) : Parcelable
