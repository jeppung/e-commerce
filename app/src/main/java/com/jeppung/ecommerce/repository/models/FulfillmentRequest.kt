package com.jeppung.ecommerce.repository.models

import com.google.gson.annotations.SerializedName

data class FulfillmentRequest(

    @field:SerializedName("payment")
    val payment: String,

    @field:SerializedName("items")
    val items: List<FullfillmentItem>
)

data class FullfillmentItem(

    @field:SerializedName("quantity")
    val quantity: Int,

    @field:SerializedName("productId")
    val productId: String,

    @field:SerializedName("variantName")
    val variantName: String
)
