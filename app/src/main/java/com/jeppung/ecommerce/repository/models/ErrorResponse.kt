package com.jeppung.ecommerce.repository.models

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @field:SerializedName("code")
    var code: Int,

    @field:SerializedName("message")
    var message: String
)
