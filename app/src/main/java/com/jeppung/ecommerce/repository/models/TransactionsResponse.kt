package com.jeppung.ecommerce.repository.models

import com.google.gson.annotations.SerializedName

data class TransactionsResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: List<Transaction>,

    @field:SerializedName("message")
    val message: String
)

data class Transaction(

    @field:SerializedName("date")
    val date: String,

    @field:SerializedName("total")
    val total: Int,

    @field:SerializedName("items")
    val items: List<TransactionItem>,

    @field:SerializedName("review")
    val review: String? = null,

    @field:SerializedName("rating")
    val rating: Int? = null,

    @field:SerializedName("invoiceId")
    val invoiceId: String,

    @field:SerializedName("payment")
    val payment: String,

    @field:SerializedName("time")
    val time: String,

    @field:SerializedName("status")
    val status: Boolean,

    @field:SerializedName("image")
    val image: String,

    @field:SerializedName("name")
    val name: String,
)

data class TransactionItem(

    @field:SerializedName("quantity")
    val quantity: Int,

    @field:SerializedName("productId")
    val productId: String,

    @field:SerializedName("variantName")
    val variantName: String
)
