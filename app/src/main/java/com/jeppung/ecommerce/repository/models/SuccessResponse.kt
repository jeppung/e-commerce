package com.jeppung.ecommerce.repository.models

import com.google.gson.JsonElement

data class SuccessResponse(
    val code: Int,
    var message: String,
    var data: JsonElement?
)
