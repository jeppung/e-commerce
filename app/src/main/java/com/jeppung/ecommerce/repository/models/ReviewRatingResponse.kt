package com.jeppung.ecommerce.repository.models

import com.google.gson.annotations.SerializedName

data class ReviewRatingResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("message")
    val message: String
)
