package com.jeppung.ecommerce.repository.api_service

import android.content.Context
import com.jeppung.ecommerce.repository.prefs.UserPreference
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class RequestInterceptor @Inject constructor(
    @ApplicationContext
    private val context: Context
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val modifiedRequest = when (originalRequest.url.encodedPath) {
            ApiConfig.LOGIN_PATH, ApiConfig.REGISTER_PATH, ApiConfig.REFRESH_PATH -> {
                originalRequest.newBuilder().addHeader(
                    "API_KEY",
                    ApiConfig.HEADER_API_KEY
                ).build()
            }
            else -> {
                val accessToken = UserPreference(context).getAccessToken()
                originalRequest.newBuilder().addHeader(
                    "Authorization",
                    "Bearer $accessToken"
                ).build()
            }
        }

        return chain.proceed(modifiedRequest)
    }
}
