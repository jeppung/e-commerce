package com.jeppung.ecommerce.repository.models

import com.google.firebase.database.IgnoreExtraProperties
import com.google.gson.annotations.SerializedName
import java.sql.Timestamp

@IgnoreExtraProperties
data class UserMessage(
    val name: String? = "",
    val message: String? = "",
    val timestamp: String? = ""
)
