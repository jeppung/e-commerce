package com.jeppung.ecommerce.repository.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Wishlist(
    @PrimaryKey val productId: String,
    val productImage: String,
    val productTitle: String,
    val productPrice: Long,
    val productStore: String,
    val productRating: Float,
    val productSale: Int,
    val productStock: Int,
    val productVariant: String
)
