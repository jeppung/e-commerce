package com.jeppung.ecommerce.repository.api_service

import com.jeppung.ecommerce.repository.models.FulfillmentRequest
import com.jeppung.ecommerce.repository.models.InvoiceResponse
import com.jeppung.ecommerce.repository.models.LoginResponse
import com.jeppung.ecommerce.repository.models.ProductDetailResponse
import com.jeppung.ecommerce.repository.models.ProductResponse
import com.jeppung.ecommerce.repository.models.ProductReviewResponse
import com.jeppung.ecommerce.repository.models.RegisterProfileResponse
import com.jeppung.ecommerce.repository.models.RegisterResponse
import com.jeppung.ecommerce.repository.models.ReviewRatingRequest
import com.jeppung.ecommerce.repository.models.ReviewRatingResponse
import com.jeppung.ecommerce.repository.models.SearchResponse
import com.jeppung.ecommerce.repository.models.TransactionsResponse
import com.jeppung.ecommerce.repository.models.UserModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    companion object {
        private const val HEADER_CONTENT_TYPE = "Content-Type: application/json"
    }

    @Headers(HEADER_CONTENT_TYPE)
    @POST("register")
    suspend fun register(
        @Body body: UserModel
    ): RegisterResponse

    @Multipart
    @POST("profile")
    suspend fun registerProfile(
        @Part("userName") userName: RequestBody,
        @Part userImage: MultipartBody.Part?
    ): RegisterProfileResponse

    @Headers(HEADER_CONTENT_TYPE)
    @POST("login")
    suspend fun login(
        @Body body: UserModel
    ): LoginResponse

    @Headers(HEADER_CONTENT_TYPE)
    @POST("search")
    suspend fun search(@Query("query") query: String?): SearchResponse

    @Headers(HEADER_CONTENT_TYPE)
    @POST("products")
    suspend fun getSearchProduct(
        @Query("search") search: String?,
        @Query("brand") brand: String?,
        @Query("lowest") lowest: Int?,
        @Query("highest") highest: Int?,
        @Query("sort") sort: String?,
        @Query("limit") limit: Int?,
        @Query("page") page: Int?
    ): ProductResponse

    @Headers(HEADER_CONTENT_TYPE)
    @POST("refresh")
    fun refreshToken(
        @Body body: HashMap<String, String?>
    ): Call<LoginResponse>

    @Headers(HEADER_CONTENT_TYPE)
    @GET("products/{id}")
    suspend fun productDetail(@Path("id") productId: String): ProductDetailResponse

    @Headers(HEADER_CONTENT_TYPE)
    @GET("review/{id}")
    suspend fun productReview(@Path("id") productId: String): ProductReviewResponse

    @Headers(HEADER_CONTENT_TYPE)
    @POST("fulfillment")
    suspend fun processPayment(
        @Body body: FulfillmentRequest
    ): InvoiceResponse

    @Headers(HEADER_CONTENT_TYPE)
    @POST("rating")
    suspend fun submitReviewRating(
        @Body body: ReviewRatingRequest
    ): ReviewRatingResponse

    @Headers(HEADER_CONTENT_TYPE)
    @GET("transaction")
    suspend fun getAllTransaction(): TransactionsResponse
}
