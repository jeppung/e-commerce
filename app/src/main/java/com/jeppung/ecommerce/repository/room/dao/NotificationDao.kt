package com.jeppung.ecommerce.repository.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.jeppung.ecommerce.repository.room.entity.Notification

@Dao
interface NotificationDao {
    @Insert
    fun addNotification(data: Notification)

    @Update
    fun updateIsReadStatus(data: Notification)

    @Query("SELECT * FROM notification")
    fun getAllNotifications(): LiveData<List<Notification>>
}
