package com.jeppung.ecommerce.repository.models

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class QueryModel(
    var search: String?,
    var brand: String?,
    var lowest: Int?,
    var highest: Int?,
    var sort: String?,
    var limit: Int? = 10,
    var page: Int?
) : Parcelable
