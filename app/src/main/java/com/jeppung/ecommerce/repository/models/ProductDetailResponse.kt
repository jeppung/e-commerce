package com.jeppung.ecommerce.repository.models

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Keep
@Parcelize
data class ProductDetailResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: ProductDetailData?,

    @field:SerializedName("message")
    val message: String
) : Parcelable

@Parcelize
data class ProductVariantItem(

    @field:SerializedName("variantPrice")
    val variantPrice: Int,

    @field:SerializedName("variantName")
    val variantName: String
) : Parcelable

@Parcelize
data class ProductDetailData(
    @field:SerializedName("image")
    val image: List<String>,

    @field:SerializedName("productId")
    val productId: String,

    @field:SerializedName("description")
    val description: String,

    @field:SerializedName("totalRating")
    val totalRating: Int,

    @field:SerializedName("store")
    val store: String,

    @field:SerializedName("productName")
    val productName: String,

    @field:SerializedName("totalSatisfaction")
    val totalSatisfaction: Int,

    @field:SerializedName("sale")
    val sale: Int,

    @field:SerializedName("productVariant")
    val productVariant: List<ProductVariantItem>,

    @field:SerializedName("stock")
    val stock: Int,

    @field:SerializedName("productRating")
    val productRating: Float,

    @field:SerializedName("brand")
    val brand: String,

    @field:SerializedName("productPrice")
    val productPrice: Int,

    @field:SerializedName("totalReview")
    val totalReview: Int
) : Parcelable, Serializable

@Parcelize
data class ProductDetailDataTest(
    val image: List<String>,

    val productId: String,

    val description: String,

    val totalRating: Int,

    val store: String,

    val productName: String,

    val totalSatisfaction: Int,

    val sale: Int,

    val productVariant: List<ProductVariantItem>,

    val stock: Int,

    val productRating: Float,

    val brand: String,

    val productPrice: Int,

    val totalReview: Int
) : Parcelable, Serializable
