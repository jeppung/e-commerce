package com.jeppung.ecommerce.repository.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Notification(
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
    val type: String,
    val date: String,
    val time: String,
    val title: String,
    val body: String,
    val image: String,
    var isRead: Boolean
)
