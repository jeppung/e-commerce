package com.jeppung.ecommerce.repository.models

data class NotificationModel(
    val title: String,
    val body: String,
    val image: String
)
