package com.jeppung.ecommerce.repository.models

data class UserModel(
    val email: String,
    val password: String,
    var firebaseToken: String
)
