package com.jeppung.ecommerce.room.viewmodel

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jeppung.ecommerce.MainViewModel
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.dao.CartDao
import com.jeppung.ecommerce.repository.room.dao.NotificationDao
import com.jeppung.ecommerce.repository.room.dao.WishlistDao
import com.jeppung.ecommerce.repository.room.entity.Wishlist
import com.jeppung.ecommerce.room.RoomBaseTest
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock

@RunWith(AndroidJUnit4::class)
class MainnViewModelTest : RoomBaseTest() {

    private lateinit var notificationDao: NotificationDao
    private lateinit var cartDao: CartDao
    private lateinit var wishlistDao: WishlistDao

    @Mock
    private lateinit var mockViewModel: MainViewModel

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()

        notificationDao = db.notificationDao()
        cartDao = db.cartDao()
        wishlistDao = db.wishlistDao()
        mockViewModel = MainViewModel(db)
    }

    @Test
    fun getCartSize() {
        dummyCartList.forEach {
            cartDao.insertProductToCart(it)
        }
        TestCase.assertEquals(
            2, mockViewModel.getCartSize().getOrAwaitValue().size
        )
    }

    @Test
    fun getNotificationSize() {
        dummyNotificationList.forEach {
            notificationDao.addNotification(it)
        }
        TestCase.assertEquals(
            2, mockViewModel.getNotificationSize().getOrAwaitValue().size
        )
    }

    @Test
    fun getWishlistSize() {
        dummyWishlistList.forEach {
            wishlistDao.addWishlistProduct(it)
        }
        TestCase.assertEquals(
            2, mockViewModel.getWishlistSize().getOrAwaitValue().size
        )
    }
}

val dummyWishlistList: List<Wishlist> = listOf(
    Wishlist(
        "mnoq-rstu-vwyz-ps1r",
        "",
        "Product2",
        8000000,
        "ProductStore2",
        5.00.toFloat(),
        5,
        productStock = 10,
        productVariant = "RAM 32GB"
    ),
    Wishlist(
        "abcd-efgh-ijkl-mnop",
        "",
        "Product1",
        8000000,
        "ProductStore1",
        4.00.toFloat(),
        10,
        productStock = 20,
        productVariant = "RAM 16GB"
    )
)