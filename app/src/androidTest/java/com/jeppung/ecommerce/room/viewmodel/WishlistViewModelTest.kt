package com.jeppung.ecommerce.room.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.dao.CartDao
import com.jeppung.ecommerce.repository.room.dao.WishlistDao
import com.jeppung.ecommerce.room.RoomBaseTest
import com.jeppung.ecommerce.views.wishlist.WishlistViewModel
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

class WishlistViewModelTest : RoomBaseTest() {

    private lateinit var wishlistDao: WishlistDao
    private lateinit var cartDao: CartDao

    @Mock
    private lateinit var mockViewModel: WishlistViewModel

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()

        wishlistDao = db.wishlistDao()
        cartDao = db.cartDao()
        mockViewModel = WishlistViewModel(db)
    }

    @Test
    fun getAllWishlist() {
        dummyWishlistList.forEach {
            wishlistDao.addWishlistProduct(it)
        }

        TestCase.assertEquals(2, mockViewModel.getAllProductWishlist().getOrAwaitValue().size)
    }

    @Test
    fun removeProductFromWishlist() {
        dummyWishlistList.forEach {
            wishlistDao.addWishlistProduct(it)
        }

        mockViewModel.removeProductFromWishlist("mnoq-rstu-vwyz-ps1r")

        TestCase.assertEquals(1, mockViewModel.getAllProductWishlist().getOrAwaitValue().size)
    }

    @Test
    fun getSingleCartProduct() {
        dummyCartList.forEach {
            cartDao.insertProductToCart(it)
        }

        TestCase.assertEquals(
            "mnoq-rstu-vwyz-ps1r",
            mockViewModel.getSingleCartProduct("mnoq-rstu-vwyz-ps1r").productId
        )
    }

    @Test
    fun addWishlistToCart() {
        dummyCartList.forEach {
            mockViewModel.addProductToCart(it)
        }

        TestCase.assertEquals(2, cartDao.getAllCartProduct().getOrAwaitValue().size)
    }

    @Test
    fun updateCartProductQty() {
        dummyCartList.forEach {
            mockViewModel.addProductToCart(it)
        }

        mockViewModel.updateCartProductQuantity("mnoq-rstu-vwyz-ps1r", "RAM 32 GB")

        TestCase.assertEquals(2, mockViewModel.getSingleCartProduct("mnoq-rstu-vwyz-ps1r").quantity)
    }

}