package com.jeppung.ecommerce.room

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.dao.CartDao
import com.jeppung.ecommerce.repository.room.dao.WishlistDao
import com.jeppung.ecommerce.repository.room.entity.Wishlist
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

@RunWith(AndroidJUnit4::class)
class WishlistRoomTest {
    private lateinit var db: AppDatabase
    private lateinit var wishlistDao: WishlistDao

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        wishlistDao = db.wishlistDao()
    }


    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun addAndGetAllWishlistProduct() {
        val wishlist1 = Wishlist(
            "mnoq-rstu-vwyz-ps1r",
            "",
            "Product2",
            8000000,
            "ProductStore2",
            5.00.toFloat(),
            5,
            productStock = 10,
            productVariant = "RAM 32GB"
        )

        val wishlist2 = Wishlist(
            "abcd-efgh-ijkl-mnop",
            "",
            "Product1",
            8000000,
            "ProductStore1",
            4.00.toFloat(),
            10,
            productStock = 20,
            productVariant = "RAM 16GB"
        )


        wishlistDao.addWishlistProduct(wishlist1)
        wishlistDao.addWishlistProduct(wishlist2)

        Assert.assertEquals(2, wishlistDao.getAllWishlistProduct().getOrAwaitValue().size)
    }

    @Test
    @Throws(Exception::class)
    fun addAndRemoveWishlist() {
        val wishlist1 = Wishlist(
            "mnoq-rstu-vwyz-ps1r",
            "",
            "Product2",
            8000000,
            "ProductStore2",
            5.00.toFloat(),
            5,
            productStock = 10,
            productVariant = "RAM 32GB"
        )

        wishlistDao.addWishlistProduct(wishlist1)
        Assert.assertNotNull(wishlistDao.removeProductFromWishlist("mnoq-rstu-vwyz-ps1r"))
    }

    @Test
    @Throws(Exception::class)
    fun getSingleWishlistProduct() {
        val wishlist1 = Wishlist(
            "mnoq-rstu-vwyz-ps1r",
            "",
            "Product2",
            8000000,
            "ProductStore2",
            5.00.toFloat(),
            5,
            productStock = 10,
            productVariant = "RAM 32GB"
        )

        val wishlist2 = Wishlist(
            "abcd-efgh-ijkl-mnop",
            "",
            "Product1",
            8000000,
            "ProductStore1",
            4.00.toFloat(),
            10,
            productStock = 20,
            productVariant = "RAM 16GB"
        )


        wishlistDao.addWishlistProduct(wishlist1)
        wishlistDao.addWishlistProduct(wishlist2)

        Assert.assertEquals(
            "Product1",
            wishlistDao.getWishlistProduct("abcd-efgh-ijkl-mnop")?.productTitle
        )
    }

    fun <T> LiveData<T>.getOrAwaitValue(
        time: Long = 2,
        timeUnit: TimeUnit = TimeUnit.SECONDS
    ): T {
        var data: T? = null
        val latch = CountDownLatch(1)
        val observer = object : Observer<T> {
            override fun onChanged(value: T) {
                data = value
                latch.countDown()
                this@getOrAwaitValue.removeObserver(this)
            }
        }

        this.observeForever(observer)

        // Don't wait indefinitely if the LiveData is not set.
        if (!latch.await(time, timeUnit)) {
            throw TimeoutException("LiveData value was never set.")
        }

        @Suppress("UNCHECKED_CAST")
        return data as T
    }
}