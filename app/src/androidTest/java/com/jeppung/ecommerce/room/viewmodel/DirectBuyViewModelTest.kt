package com.jeppung.ecommerce.room.viewmodel

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jeppung.ecommerce.repository.api_service.ApiService
import com.jeppung.ecommerce.repository.models.FulfillmentRequest
import com.jeppung.ecommerce.repository.models.FullfillmentItem
import com.jeppung.ecommerce.repository.models.InvoiceData
import com.jeppung.ecommerce.repository.models.InvoiceResponse
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.dao.CartDao
import com.jeppung.ecommerce.repository.room.entity.Cart
import com.jeppung.ecommerce.room.RoomBaseTest
import com.jeppung.ecommerce.views.direct_buy.DirectBuyViewModel
import com.jeppung.ecommerce.views.direct_buy.PaymentProcessListener
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(AndroidJUnit4::class)
class DirectBuyViewModelTest : RoomBaseTest() {

    private lateinit var cartDao: CartDao

    @Mock
    private lateinit var mockViewModel: DirectBuyViewModel

    @Mock
    private lateinit var apiService: ApiService

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        apiService = mock()

        cartDao = db.cartDao()
        mockViewModel = DirectBuyViewModel(db, apiService)
    }

    @Test
    fun deleteRelatedProductFromCart() {
        dummyCartList.forEach {
            cartDao.insertProductToCart(it)
        }

        mockViewModel.deleteRelatedProductFromCart(dummyCartList[0].productId)

        TestCase.assertEquals(1, cartDao.getAllCartProduct().getOrAwaitValue().size)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun processPayment() = runTest {
          whenever(apiService.processPayment(
            FulfillmentRequest(
                "Bank BCA",
                fulfillmentList,
            ))).thenReturn(InvoiceResponse(
                200,
                InvoiceData(
                    "22 Jun 2023",
                    49000000,
                    "aaaa-bbbb-cccc-dddd",
                    "Bank BCA",
                    "09:05",
                    true,
                ),
                "OK"
            ))
        mockViewModel.processPayment(
            FulfillmentRequest(
                "Bank BCA",
                fulfillmentList,
            ),
            object : PaymentProcessListener {
                override fun onFailure(err: Throwable) {}
            }
        ) { invoiceData, fullfillmentItems ->
            TestCase.assertEquals("aaaa-bbbb-cccc-dddd", invoiceData.invoiceId)
            TestCase.assertEquals(2, fullfillmentItems.size)
        }
    }

}

val fulfillmentList: List<FullfillmentItem> = listOf(
    FullfillmentItem(
        1,
        "abcd-efgh-ijkl-mnop",
        "RAM 16 GB"
    ),
    FullfillmentItem(
        1,
        "mnoq-rstu-vwyz-ps1r",
        "RAM 32 GB"
    ),
)

val dummyCartList: List<Cart> = listOf(
    Cart(
        "abcd-efgh-ijkl-mnop",
        "",
        "Product1",
        "RAM 16 GB",
        5,
        8000000,
        1,
        true
    ),

    Cart(
        "mnoq-rstu-vwyz-ps1r",
        "",
        "Product2",
        "RAM 32 GB",
        2,
        12000000,
        1,
        false
    )
)