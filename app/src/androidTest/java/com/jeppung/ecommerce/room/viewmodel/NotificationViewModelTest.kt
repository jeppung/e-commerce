package com.jeppung.ecommerce.room.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.dao.NotificationDao
import com.jeppung.ecommerce.repository.room.entity.Notification
import com.jeppung.ecommerce.room.RoomBaseTest
import com.jeppung.ecommerce.views.notification.NotificationViewModel
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

@RunWith(AndroidJUnit4::class)
class NotificationViewModelTest : RoomBaseTest() {
    private lateinit var notificationDao: NotificationDao

    @Mock
    private lateinit var mockViewModel: NotificationViewModel

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()

        notificationDao = db.notificationDao()
        mockViewModel = NotificationViewModel(db)
    }

    @Test
    @Throws(Exception::class)
    fun getAllNotification() {

        dummyNotificationList.forEach {
            notificationDao.addNotification(it)
        }

        Assert.assertEquals(2, mockViewModel.getNotifications().getOrAwaitValue().size)

    }

    @Test
    @Throws(Exception::class)
    fun updateNotification() {
        notificationDao.addNotification(dummyNotificationList[0])
        mockViewModel.updateIsReadStatus(
            dummyNotificationList[0].copy(
            id = 1,
            isRead = true
        ))
        Assert.assertEquals(true, mockViewModel.getNotifications().getOrAwaitValue().first().isRead)
    }

}

val dummyNotificationList: List<Notification> = listOf(
    Notification(
        type = "info",
        date = "19 Jun 2023",
        time = "11:30",
        title = "Transaksi Berhasil",
        body = "Lorem Ipsum",
        image = "",
        isRead = false
    ), Notification(
        type = "promo",
        date = "18 Jun 2023",
        time = "15:30",
        title = "Promo Terbaru",
        body = "Lorem Ipsum",
        image = "",
        isRead = false
    )
)