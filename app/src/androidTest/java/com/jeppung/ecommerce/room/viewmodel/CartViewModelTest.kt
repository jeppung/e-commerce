package com.jeppung.ecommerce.room.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.dao.CartDao
import com.jeppung.ecommerce.repository.room.dao.WishlistDao
import com.jeppung.ecommerce.room.RoomBaseTest
import com.jeppung.ecommerce.views.cart.CartViewModel
import com.jeppung.ecommerce.views.wishlist.WishlistViewModel
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

@RunWith(AndroidJUnit4::class)
class CartViewModelTest: RoomBaseTest() {

    private lateinit var cartDao: CartDao

    @Mock
    private lateinit var mockViewModel: CartViewModel

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()

        cartDao = db.cartDao()
        mockViewModel = CartViewModel(db)
    }

    @Test
    fun getAllCart() {
        dummyCartList.forEach {
            cartDao.insertProductToCart(it)
        }

        Assert.assertEquals(2, mockViewModel.getAllCartProduct().getOrAwaitValue().size)
    }

    @Test
    fun updateAndGetCheckedCartProduct() {
        dummyCartList.forEach {
            cartDao.insertProductToCart(it)
        }

        dummyCartList[0].isChecked = true
        mockViewModel.updateCartProduct(dummyCartList[0])

        Assert.assertEquals(1, mockViewModel.getCheckedProduct().size)
    }

    @Test
    fun deleteCartProduct() {
        dummyCartList.forEach {
            cartDao.insertProductToCart(it)
        }

        mockViewModel.deleteCartProduct(dummyCartList[0])

        Assert.assertEquals(1, mockViewModel.getAllCartProduct().getOrAwaitValue().size)
    }


    @Test
    fun setAllCartProductCheckedValue() {
        dummyCartList.forEach {
            cartDao.insertProductToCart(it)
        }

        mockViewModel.setAllCartProductCheckedValue(1)

        Assert.assertEquals(2, mockViewModel.getCheckedProduct().size)
    }
}