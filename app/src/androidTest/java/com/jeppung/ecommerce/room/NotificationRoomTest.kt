package com.jeppung.ecommerce.room

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.dao.NotificationDao
import com.jeppung.ecommerce.repository.room.entity.Notification
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

@RunWith(AndroidJUnit4::class)
class NotificationRoomTest: RoomBaseTest() {
    private lateinit var notificationDao: NotificationDao
    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        notificationDao = db.notificationDao()
    }

    @Test
    @Throws(Exception::class)
    fun addAndGetAllNotification() {
        val notification1 = Notification(
            type = "info",
            date = "19 Jun 2023",
            time = "11:30",
            title = "Transaksi Berhasil",
            body = "Lorem Ipsum",
            image = "",
            isRead = false
        )

        val notification2 = Notification(
            type = "promo",
            date = "18 Jun 2023",
            time = "15:30",
            title = "Promo Terbaru",
            body = "Lorem Ipsum",
            image = "",
            isRead = false
        )

        notificationDao.addNotification(notification1)
        notificationDao.addNotification(notification2)

        Assert.assertEquals(2, notificationDao.getAllNotifications().getOrAwaitValue().size)
    }

    @Test
    @Throws(Exception::class)
    fun addAndUpdateNotificationIsRead() {
        val notification1 = Notification(
            type = "info",
            date = "19 Jun 2023",
            time = "11:30",
            title = "Transaksi Berhasil",
            body = "Lorem Ipsum",
            image = "",
            isRead = false
        )
        notificationDao.addNotification(notification1)
        val updated = notification1.copy(
            id = 1,
            isRead = true
        )
        notificationDao.updateIsReadStatus(updated)

        Assert.assertEquals(
            updated,
            notificationDao.getAllNotifications().getOrAwaitValue().first()
        )
    }
}