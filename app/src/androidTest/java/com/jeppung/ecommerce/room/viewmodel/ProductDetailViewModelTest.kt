//package com.jeppung.ecommerce.room.viewmodel
//
//import android.content.Context
//import androidx.room.Room
//import androidx.test.core.app.ApplicationProvider
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import com.jeppung.ecommerce.repository.api_service.ApiService
//import com.jeppung.ecommerce.repository.models.ProductDetailData
//import com.jeppung.ecommerce.repository.models.ProductDetailResponse
//import com.jeppung.ecommerce.repository.models.ProductVariantItem
//import com.jeppung.ecommerce.repository.room.AppDatabase
//import com.jeppung.ecommerce.repository.room.dao.CartDao
//import com.jeppung.ecommerce.repository.room.dao.WishlistDao
//import com.jeppung.ecommerce.room.RoomBaseTest
//import com.jeppung.ecommerce.views.product_detail.ProductDetailCallback
//import com.jeppung.ecommerce.views.product_detail.ProductDetailViewModel
//import junit.framework.TestCase
//import kotlinx.coroutines.test.runTest
//import org.junit.Before
//import org.junit.Test
//import org.junit.runner.RunWith
//import org.mockito.Mock
//import org.mockito.kotlin.mock
//import org.mockito.kotlin.whenever
//
//@RunWith(AndroidJUnit4::class)
//class ProductDetailViewModelTest : RoomBaseTest() {
//    private lateinit var cartDao: CartDao
//    private lateinit var wishlistDao: WishlistDao
//
//    @Mock
//    private lateinit var mockViewModel: ProductDetailViewModel
//
//    @Mock
//    private lateinit var apiService: ApiService
//
//    @Before
//    fun setup() {
//        val context = ApplicationProvider.getApplicationContext<Context>()
//        db = Room.inMemoryDatabaseBuilder(
//            context, AppDatabase::class.java
//        ).build()
//        apiService = mock()
//
//        cartDao = db.cartDao()
//        wishlistDao = db.wishlistDao()
//        mockViewModel = ProductDetailViewModel(apiService, db)
//    }
//
//    @Test
//    fun checkProductWishlist() {
//        dummyWishlistList.forEach {
//            wishlistDao.addWishlistProduct(it)
//        }
//
//        mockViewModel.checkProductWishlist(dummyWishlistList[0].productId)
//
//        TestCase.assertEquals(true, mockViewModel.isWishlist.getOrAwaitValue())
//    }
//
//    @Test
//    fun addProductToWishlist() {
//        dummyWishlistList.forEach {
//            mockViewModel.addProductToWishlist(it)
//        }
//
//        TestCase.assertEquals(2, wishlistDao.getAllWishlistProduct().getOrAwaitValue().size)
//    }
//
//    @Test
//    fun removeProductFromWishlist() {
//        dummyWishlistList.forEach {
//            mockViewModel.addProductToWishlist(it)
//        }
//
//        mockViewModel.removeProductFromWishlist(dummyWishlistList[0].productId)
//
//        TestCase.assertEquals(1, wishlistDao.getAllWishlistProduct().getOrAwaitValue().size)
//    }
//
//    @Test
//    fun addProductToCart() {
//        dummyCartList.forEach {
//            mockViewModel.addProductToCart(it)
//        }
//
//        TestCase.assertEquals(2, cartDao.getAllCartProduct().getOrAwaitValue().size)
//    }
//
//    @Test
//    fun updateCartProductQuantityAndGetSingleCartProduct() {
//        dummyCartList.forEach {
//            mockViewModel.addProductToCart(it)
//        }
//
//        mockViewModel.updateCartProductQuantity(
//            dummyCartList[0].productId,
//            dummyCartList[0].productVariant
//        )
//
//        TestCase.assertEquals(
//            2,
//            mockViewModel.getSingleCartProduct(dummyCartList[0].productId).quantity
//        )
//    }
//
//    @Test
//    fun getProductDetail() = runTest {
//        whenever(apiService.productDetail("17b4714d-527a-4be2-84e2-e4c37c2b3292"))
//            .thenReturn(ProductDetailResponse(
//                200,
//                dummyProductResponse,
//                message = "OK"
//            ))
//
//        mockViewModel.getProductDetail("17b4714d-527a-4be2-84e2-e4c37c2b3292", object:ProductDetailCallback {
//            override fun onFailure(err: Throwable) {}
//        })
//
//        TestCase.assertEquals(dummyProductResponse.productId, mockViewModel.productDetail.getOrAwaitValue().productId)
//    }
//}
//
//val dummyProductResponse = ProductDetailData(
//    productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
//    productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
//    productPrice = 24499000,
//    image = listOf(
//        "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
//        "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
//        "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
//    ),
//    brand = "Asus",
//    description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\\n\\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\\nGraphics Memory : 6GB GDDR6\\nDiscrete/Optimus : MUX Switch + Optimus\\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
//    store = "AsusStore",
//    sale = 12,
//    stock = 2,
//    totalRating = 7,
//    totalReview = 5,
//    totalSatisfaction = 100,
//    productRating = 5f,
//    productVariant = listOf(
//        ProductVariantItem(
//            variantName = "RAM 16GB",
//            variantPrice = 0
//        ),
//        ProductVariantItem(
//            variantName = "RAM 32GB",
//            variantPrice = 1000000
//        )
//    ),
//)