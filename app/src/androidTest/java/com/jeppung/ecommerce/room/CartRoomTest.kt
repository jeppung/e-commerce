package com.jeppung.ecommerce.room

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jeppung.ecommerce.repository.room.AppDatabase
import com.jeppung.ecommerce.repository.room.dao.CartDao
import com.jeppung.ecommerce.repository.room.entity.Cart
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

@RunWith(AndroidJUnit4::class)
class CartRoomTest: RoomBaseTest() {
    private lateinit var cartDao: CartDao

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        cartDao = db.cartDao()
    }

    @Test
    @Throws(Exception::class)
    fun getAllCartItem() {
        val cart1 = Cart(
            "abcd-efgh-ijkl-mnop",
            "",
            "Product1",
            "RAM 16 GB",
            5,
            8000000,
            1,
            false
        )
        val cart2 = Cart(
            "mnoq-rstu-vwyz-ps1r",
            "",
            "Product2",
            "RAM 32 GB",
            2,
            12000000,
            1,
            false
        )

        cartDao.insertProductToCart(cart1)
        cartDao.insertProductToCart(cart2)

        Assert.assertEquals(2, cartDao.getAllCartProduct().getOrAwaitValue().size)

    }

    @Test
    @Throws(Exception::class)
    fun getSingleCartProduct() {
        val cart1 = Cart(
            "abcd-efgh-ijkl-mnop",
            "",
            "Product1",
            "RAM 16 GB",
            5,
            8000000,
            1,
            true
        )

        val cart2 = Cart(
            "mnoq-rstu-vwyz-ps1r",
            "",
            "Product2",
            "RAM 32 GB",
            2,
            12000000,
            1,
            false
        )
        cartDao.insertProductToCart(cart1)
        cartDao.insertProductToCart(cart2)

        Assert.assertEquals(
            "mnoq-rstu-vwyz-ps1r",
            cartDao.getSingleCartProduct("mnoq-rstu-vwyz-ps1r").productId
        )
    }


    @Test
    @Throws(Exception::class)
    fun insertProductToCart() {
        val cart1 = Cart(
            "abcd-efgh-ijkl-mnop",
            "",
            "Product1",
            "RAM 16 GB",
            5,
            8000000,
            1,
            false
        )

        cartDao.insertProductToCart(cart1)
        Assert.assertNotNull(cartDao.getSingleCartProduct("abcd-efgh-ijkl-mnop"))
    }

    @Test
    @Throws(Exception::class)
    fun deleteProductFromCart() {
        val cart1 = Cart(
            "abcd-efgh-ijkl-mnop",
            "",
            "Product1",
            "RAM 16 GB",
            5,
            8000000,
            1,
            false
        )

        cartDao.insertProductToCart(cart1)
        cartDao.deleteCartProductWithId(cart1.productId)
        Assert.assertNull(cartDao.getSingleCartProduct("abcd-efgh-ijkl-mnop"))
    }

    @Test
    @Throws(Exception::class)
    fun updateCartProductQuantity() {
        val cart1 = Cart(
            "abcd-efgh-ijkl-mnop",
            "",
            "Product1",
            "RAM 16 GB",
            5,
            8000000,
            1,
            false
        )
        cartDao.insertProductToCart(cart1)
        cartDao.updateCartProductQuantity(1, "abcd-efgh-ijkl-mnop", "RAM 16 GB")
        Assert.assertEquals(2, cartDao.getSingleCartProduct("abcd-efgh-ijkl-mnop").quantity)
    }

    @Test
    @Throws(Exception::class)
    fun updateCartCheckedStatus() {
        val cart1 = Cart(
            "abcd-efgh-ijkl-mnop",
            "",
            "Product1",
            "RAM 16 GB",
            5,
            8000000,
            1,
            false
        )
        cartDao.insertProductToCart(cart1)
        cart1.isChecked = true
        cartDao.updateCartProduct(cart1)
        Assert.assertEquals(true, cartDao.getSingleCartProduct("abcd-efgh-ijkl-mnop").isChecked)
    }

    @Test
    @Throws(Exception::class)
    fun setAllCartProductCheckedValue() {
        val cart1 = Cart(
            "abcd-efgh-ijkl-mnop",
            "",
            "Product1",
            "RAM 16 GB",
            5,
            8000000,
            1,
            false
        )

        val cart2 = Cart(
            "mnoq-rstu-vwyz-ps1r",
            "",
            "Product2",
            "RAM 32 GB",
            2,
            12000000,
            1,
            false
        )
        cartDao.insertProductToCart(cart1)
        cartDao.insertProductToCart(cart2)

        cartDao.setAllCartProductCheckedValue(1)

        Assert.assertEquals(
            2,
            cartDao.getAllCartProduct().getOrAwaitValue().filter { it.isChecked }.size
        )
    }

    @Test
    @Throws(Exception::class)
    fun getCheckedCartProduct() {
        val cart1 = Cart(
            "abcd-efgh-ijkl-mnop",
            "",
            "Product1",
            "RAM 16 GB",
            5,
            8000000,
            1,
            true
        )

        val cart2 = Cart(
            "mnoq-rstu-vwyz-ps1r",
            "",
            "Product2",
            "RAM 32 GB",
            2,
            12000000,
            1,
            false
        )
        cartDao.insertProductToCart(cart1)
        cartDao.insertProductToCart(cart2)

        Assert.assertEquals(1, cartDao.getCheckedCartProduct().size)
    }
}