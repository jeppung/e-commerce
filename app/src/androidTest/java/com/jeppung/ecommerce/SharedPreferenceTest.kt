package com.jeppung.ecommerce

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jeppung.ecommerce.repository.prefs.UserPreference
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SharedPreferenceTest {
    private lateinit var sharedPreferences: UserPreference

    @Before
    fun setup() {
        val context: Context = ApplicationProvider.getApplicationContext()
        sharedPreferences = UserPreference(context)
    }


    @Test
    @Throws(Exception::class)
    fun setAndGetIsOnboard() {
        sharedPreferences.setIsOnboard(true)
        TestCase.assertEquals(true, sharedPreferences.getIsOnboard())
    }

    @Test
    @Throws(Exception::class)
    fun setAndGetAccessToken() {
        sharedPreferences.setAccessToken(
            "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMzAuMTI5OjgwODAvIiwidXNlcklkIjoiMzczNTNkMzAtMWIzZC00ZGJlLThmODQtYWZjMjdjNGU5MWJhIiwidHlwZVRva2VuIjoiYWNjZXNzVG9rZW4iLCJleHAiOjE2ODUzNDE1MjB9.ldL_6Qoo-MfMmwHrhxXUv670Uz6j0CCF9t9I8uOmW_LuAUTzCWhjMcQelP8MjfnVDqKSZj2LaqHv3TY08AB7TQ"
        )

        TestCase.assertEquals(
            "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMzAuMTI5OjgwODAvIiwidXNlcklkIjoiMzczNTNkMzAtMWIzZC00ZGJlLThmODQtYWZjMjdjNGU5MWJhIiwidHlwZVRva2VuIjoiYWNjZXNzVG9rZW4iLCJleHAiOjE2ODUzNDE1MjB9.ldL_6Qoo-MfMmwHrhxXUv670Uz6j0CCF9t9I8uOmW_LuAUTzCWhjMcQelP8MjfnVDqKSZj2LaqHv3TY08AB7TQ",
            sharedPreferences.getAccessToken()
        )
    }

    @Test
    @Throws(Exception::class)
    fun setAndGetFirebaseToken() {
        TestCase.assertNotNull(
            sharedPreferences.setFirebaseToken(
                "ehnGH9NWSnmzOUV6dx-5dR:APA91bFXVwzUudRDlU8bnJRQfyZ0qcgK7oKJn5-oAM8IhOs9h5eFVWVoW_ZN08fJBXsVeXups7Pf_sHkby4aLhVk55ORS3F5ElDvMGEs8iZhQP0NxtS0Hwqi_gu7PtBbugDLQ0nSCl3Q"
            )
        )
    }

    @Test
    @Throws(Exception::class)
    fun setAndGetRefreshToken() {
        sharedPreferences.setRefreshToken("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4zOC4xMjk6ODA4MC8iLCJ1c2VySWQiOiJhZTA0ZWU4ZS0zNWZhLTRkMzktODY0ZC0xMjQ4M2QzMDczNGEiLCJ0eXBlVG9rZW4iOiJhY2Nlc3NUb2tlbiIsImV4cCI6MTY4NDIxMTE1OH0.XGpTt2QuY2GA2XbGl_WyCNO5bxnW0iUqGs58gZuHJ1ASzYwnTT4pMd0S9cgSt1PkbiohMu-mVbb1oMJS7kyNeA")
        TestCase.assertEquals(
            "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4zOC4xMjk6ODA4MC8iLCJ1c2VySWQiOiJhZTA0ZWU4ZS0zNWZhLTRkMzktODY0ZC0xMjQ4M2QzMDczNGEiLCJ0eXBlVG9rZW4iOiJhY2Nlc3NUb2tlbiIsImV4cCI6MTY4NDIxMTE1OH0.XGpTt2QuY2GA2XbGl_WyCNO5bxnW0iUqGs58gZuHJ1ASzYwnTT4pMd0S9cgSt1PkbiohMu-mVbb1oMJS7kyNeA",
            sharedPreferences.getRefreshToken()
        )
    }

    @Test
    @Throws(Exception::class)
    fun setAndGetUserName() {
        sharedPreferences.setUserName("test")

        TestCase.assertEquals("test", sharedPreferences.getUserName())
    }

}